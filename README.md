# Globers Android Application

## Setup

* Download `Android Studio`
* Load this project
* Run app with the emulator or a connected mobile


## App Architecture

### General principles

The app follows the "[Jetpack](https://developer.android.com/topic/libraries/architecture)" architecture guidelines

* A small number of [Activities](https://developer.android.com/guide/components/activities/intro-activities)
* Each one consists of several [fragments](https://developer.android.com/guide/components/fragments)
* Navigation inside each activity is managed by its own [Navigation graph](https://developer.android.com/guide/navigation)
* Each fragment follows the `MVVM` model:
    * The fragment communicate GUI events to a [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)
    * The fragment subscribe to events from to the ViewModel with [LiveData](https://developer.android.com/topic/libraries/architecture/livedata)
    * The ViewModel manage state necessary to the GUI and communicate with the model

There is no explicit backend service, the app relies on [Firebase](https://firebase.google.com/) for data, monitoring, authentication... The database is [Firestore](https://firebase.google.com/products/firestore/).

Inside the model, asynchronous tasks (mainly db requests and subscriptions) are managed in two ways:

* Kotline [Coroutines](https://kotlinlang.org/docs/reference/coroutines-overview.html) for classical async tasks (e.g. [firestore one-time requests](https://firebase.google.com/docs/firestore/query-data/get-data))
* [RxJava](https://github.com/ReactiveX/RxJava) observables for real-time evetns (e.g. [firestore subscriptions](https://firebase.google.com/docs/firestore/query-data/listen))


### Folders Architecture

`res` folder architecture is contrained by Android framework

* `res/layout` contains all views
    * Fragments are prefixed by the activity name to which they belong and are suffixed by `_fragment`
    * Activities are prefixed by `activity_`
    * Reusable view components are suffixed by `_view`
* `res/drawable` contains graphical resources (jpg...)
* `res/navigation` contains navigation graphs of activities
* `res/values` contains colors, strings, styles... used by views

`java.co.globers.globersapp` namespace contains the source code of the app. it contains:

* `GlobersApp` class: Singleton containing global scope data of the app (should be kept minimal) 
* `firestore` folder: contains all db entities and utils for firestore
* `machinelearning` folder: contains logic for machine learning algorithms (for now, just to compute user level)
* `services` folder: contains services used by ui as model
* `ui` folder: contains all view related code
    * `commoncomponents`contains views and abstract fragments reused in several activities
    * the other folders (`main`, `newuser`, `taboo`) contains the three activities, and fragments / viewModel associated to them
* `utils` contains technical utils


### Technical debt

* Move some logic from ViewModel to model
* Move some logic from Fragments to ViewModel
* Replace custom callbacks in model API by RxJava observables
* Add unit & functional tests
* Move hardcoded string to String resources to prepare for multi-languages
* Execute build, lint, tests and deployement on gitlab-ci
* Extract files in `tokeepsync` folder in specific repo to have one source of truth (files copy-pasted to taboo-cards-generator repository)


## Game phases

### Before UserCreation

The firestore database already contains data used for the game:
* `DbTabooProgram` which contains a set of levels -> lessons -> cards.
* `DbDictionary` which contains all the words used in cards with info about them (log-frequency of usage in the language, etc)

### UserCreation

When a new user connects to the app, he does a test to assess his level. The test consists of groups of words (loaded from `DbDictionary`) for which he has to say if he knowns them or not. At the first iteration we give him very easy and hard words. Depending on his replies, we iterate towards  more or less difficult words.

At each iteration, we use his replies to make a  [logistic regression](https://en.wikipedia.org/wiki/Logistic_regression) to guess the probability if him knowing a word depending the log-frequency of the word in the language.

When the logistic regression is calibrated, we can estimate the number of words he knows, and decide of a starting level in the learning program. We create a `DbTabooProgramUserState` to register this information. This state will change as the user plays games (to register his progress in the learning program).

We then ask him to create its account to register his test result and display his customized learning program.

### PeerMatching

Peer Matching is the process of matching people by pair to play the game. This is done by using the firestore real-time notification mecanism.

The logic is the following. When a user is looking for peer, the app is doing several things:
    * It creates a `DbPeerMatch` entity with all the info about the game it want to create. This DbPeerMatch is associated the `WAITING_FOR_PEER` status.
    * It subscribes to events from the created DbPeerMatch to be notified if a user joined it
    * It subscribes to other DbPeerMatches with caracteristics adequat to its game (language, level...) which are in `WAITING_FOR_PEER` status


Then there is to possibilities:

* It finds another DbPeerMatch to join, add its own data to it and set the status to `JOINED`. Inside the same [transaction](https://firebase.google.com/docs/firestore/manage-data/transactions), it set the status of its own create DbPeerMatch to `CANCELED`
* Its own DbPeerMatch is joined by another user, it cancel its subscription to other DbPeerMatches and start the game


### TabooGame

Once a DbPeerMatch is completed, the two users are called `Creator` and `Joiner` (of the peerMatch). The creator will create a `DbTabooGame` entity (using `TabooGameCreation` service).

DbTabooGame contains all the data usefull for the game (cards...). When the game is `READY`, we start phone call between users (using `VoiceCall` service), when the call is started, the game start.

Each user update subscribe to DbTabooGame entity events and update it when user interact with game (pass cards, guess word...) 

In addition to UI, the service `TabooStateUpdater` subscribes to the game events, and update `DbTabooProgramUserState` entity when the user guess a card. This ensure guessed cards are taken into account event if there is a issue (peer disconnects...) later in the game.
