package co.globers.globersapp.machinelearning

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*

internal class LogisticRegression1DKtTest {


    @Test
    fun logisticRegression1D_isOk() {

        val y = booleanArrayOf(true, true, true, true, false, true, true, false, false, true, false, false, false, false)
        val x = DoubleArray(y.size){ i -> i.toDouble()}

        val coeffs= logisticRegression1D(y,x).result!!

        val probaStart = computeProba(coeffs, x.first())
        val probaEnd = computeProba(coeffs, x.last())

        val probaMiddle = computeProba(coeffs, (x.last() + x.first())/2)

        assertTrue( probaStart > probaMiddle)
        assertTrue( probaMiddle > probaEnd)
        assertTrue( probaStart > 0.8)
        assertTrue( probaEnd < 0.2)


    }
}