package co.globers.globersapp

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun testRegexPhone(){

        val base = "+1 (300)-200-2000"
        val transformed = base.replace(Regex("[^0-9+]"), "")
        assertEquals("+13002002000", transformed)

    }

}
