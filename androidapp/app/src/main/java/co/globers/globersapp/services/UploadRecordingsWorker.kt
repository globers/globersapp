package co.globers.globersapp.services

import android.content.Context
import android.util.Log
import androidx.work.*
import com.google.firebase.auth.FirebaseAuth
import java.io.File
import java.util.concurrent.TimeUnit


class UploadRecordingsWorker(appContext: Context, workerParams: WorkerParameters)
    : CoroutineWorker(appContext, workerParams) {

    companion object {
        const val LOG_TAG = "UploadRecordingsWK"

        fun start(applicationContext: Context){

            Log.d(LOG_TAG, "start")

            val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()

            val saveRequest =
                PeriodicWorkRequestBuilder<UploadRecordingsWorker>(1, TimeUnit.HOURS)
                    .setConstraints(constraints)
                    .build()

            WorkManager.getInstance(applicationContext).enqueueUniquePeriodicWork(
                "UPLOAD_RECORDINGS", ExistingPeriodicWorkPolicy.REPLACE , saveRequest)

        }
    }

    override suspend fun doWork(): Result {
        Log.d(LOG_TAG, "start doWork")

        val recordingsDir = applicationContext.cacheDir.listFiles {
                file: File ->  file.isDirectory && file.name == "recordings" }?.firstOrNull()

        val userUid =FirebaseAuth.getInstance().uid

        if (recordingsDir != null && userUid != null){

            recordingsDir.listFiles()?.forEach {file ->

                Log.d(LOG_TAG, "upload recording: ${file.name}")

                try {
                    FirebaseStorageUploader.upload(file, "recordings/$userUid/${file.name}")
                    file.delete()
                } catch (e: Exception){
                    Log.e(LOG_TAG, "could not upload ${file.name}", e)
                }
            }

            return Result.success()

        } else {
            return Result.success()
        }

    }




}
