package co.globers.globersapp.firestore.entities

// uid of the entity is userUid
data class DbMessageToken(
    val token: String
){

    companion object {
        const val COLLECTION_NAME = "messageTokens"
    }

}