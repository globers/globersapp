package co.globers.globersapp.ui.main


import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.globers.globersapp.firestore.entities.DbUser
import co.globers.globersapp.firestore.executeGetOrNull
import co.globers.globersapp.services.UserDataObservables
import co.globers.globersapp.services.initMessageToken
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.Observables
import kotlinx.coroutines.launch

class MainSplashScreenViewModel : ViewModel() {


    enum class Status {
        SplashScreen,
        UserNotLoggedIn,
        UserLoaded,
        AccountNotFound,
    }


    companion object {
        const val LOG_TAG = "MainVM"
    }

    val status = MutableLiveData(Status.SplashScreen)
    private val firebaseAuth = FirebaseAuth.getInstance()
    private val authStateListener = FirebaseAuth.AuthStateListener{onAuthStateChanged(it)}

    private val compositeDisposable = CompositeDisposable()

    init{

        firebaseAuth.addAuthStateListener(authStateListener)

        val userDataObservables = UserDataObservables()

        //this code is useful to be sure that user data can be loaded before redirecting to other fragment
        // and to manage the error (clean, logout) is the opposite case
        val userLoadedSub = Observables.combineLatest(
            userDataObservables.getUserObservable(),
            userDataObservables.getProfileObservable(),
            userDataObservables.getProgramObservable(),
            userDataObservables.getProgramUserStateObservable()){
                user, _, _, _ ->
            user.uid
        }.take(1).doOnError { e ->

            Log.e(LOG_TAG, "Error on user data observables initial subscriptions", e)
            val firebaseAuth = FirebaseAuth.getInstance()
            val firestoreInstance = FirebaseFirestore.getInstance()
            firestoreInstance.terminate()
            firestoreInstance.clearPersistence()
            firebaseAuth.signOut()

        }.subscribe {userUid ->
            initMessageToken(userUid)
            status.postValue(Status.UserLoaded)
        }

        compositeDisposable.add(userLoadedSub)

    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
        firebaseAuth.removeAuthStateListener(authStateListener)
    }

    fun onAuthStateChanged(auth: FirebaseAuth) {
        val userUid = auth.uid
        if (userUid != null){
            val db = FirebaseFirestore.getInstance()
            viewModelScope.launch {
                val user = executeGetOrNull<DbUser>(db.collection(DbUser.COLLECTION_NAME).document(userUid))
                if (user == null){
                    status.value = Status.AccountNotFound
                }
            }
        } else {
            firebaseAuth.signInAnonymously().addOnCompleteListener {task ->

                if (task.isSuccessful){
                    Log.d(LOG_TAG, "signInAnonymously")
                } else {
                    Log.e(LOG_TAG, "signInAnonymously failure:", task.exception)
                }

                status.value = Status.UserNotLoggedIn
            }
        }

    }


}

