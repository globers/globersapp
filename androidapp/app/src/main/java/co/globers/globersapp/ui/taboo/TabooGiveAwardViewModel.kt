package co.globers.globersapp.ui.taboo

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.globers.globersapp.firestore.entities.DbPeerMatch
import co.globers.globersapp.firestore.entities.DbUserAward
import co.globers.globersapp.firestore.entities.DbUserAwardType
import co.globers.globersapp.firestore.executeSet
import co.globers.globersapp.utils.NonNullLiveData
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.launch
import java.util.*

class TabooGiveAwardViewModel : ViewModel() {


    enum class Status {
        NOT_SAVED,
        SAVE_IN_PROGRESS,
        FINISHED
    }

    data class State (val status: Status, val userAwardType: DbUserAwardType?)

    val status: NonNullLiveData<State> = NonNullLiveData(State(Status.NOT_SAVED, null))

    private lateinit var peerMatch: DbPeerMatch
    private var isReporterCreator: Boolean? = null

    fun init(peerMatch: DbPeerMatch, isReporterCreator: Boolean){
        this.peerMatch = peerMatch
        this.isReporterCreator = isReporterCreator
    }

    fun setAwardType(type: DbUserAwardType?){
        if ( status.value.status == Status.NOT_SAVED){
            status.value = State(Status.NOT_SAVED, type)
        }
    }


    fun save() {

        val state = status.value
        if (state.status == Status.NOT_SAVED && state.userAwardType != null){

            status.value = state.copy(status = Status.SAVE_IN_PROGRESS)

            val db = FirebaseFirestore.getInstance()
            val giverGameInfo = if (isReporterCreator!!) peerMatch.creatorGameInfo else peerMatch.joinerGameInfo!!
            val receiverGameInfo = if (isReporterCreator!!) peerMatch.joinerGameInfo!! else peerMatch.creatorGameInfo

            viewModelScope.launch {

                val uid = UUID.randomUUID().toString()
                val docRef = db.collection(DbUserAward.COLLECTION_NAME).document(uid)
                val report = DbUserAward(uid, giverGameInfo.userUid, receiverGameInfo.userUid, peerMatch.uid, state.userAwardType)

                executeSet(docRef, report)
                status.value = state.copy(status = Status.FINISHED)
            }

        }

    }


}
