package co.globers.globersapp.firestore.entities.tokeepinsync

import co.globers.globersapp.firestore.entities.DbLanguageIsoCode
import com.google.firebase.Timestamp
import com.google.firebase.firestore.ServerTimestamp

data class DbDictionary(
    val language: DbLanguageIsoCode,
    val words: Map<String, DbWordProperties>,
    val insertionBatchTag: String,
    @ServerTimestamp
    val creationDate: Timestamp = Timestamp.now()
){
    companion object {
        const val COLLECTION_NAME = "dictionaries"
    }

    constructor(): this(DbLanguageIsoCode.ENG, mapOf(), "")
}

data class DbWordProperties(
    val lnFrequencyInverse: Double,
    val wordRank: Int
){
    constructor(): this(Double.NaN, -1)
}
