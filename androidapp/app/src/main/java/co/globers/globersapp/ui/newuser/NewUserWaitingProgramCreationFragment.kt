package co.globers.globersapp.ui.newuser

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import co.globers.globersapp.ui.commoncomponents.ProgressFragment
import co.globers.globersapp.utils.Event
import co.globers.globersapp.utils.Screen
import co.globers.globersapp.utils.Tracking
import com.google.firebase.auth.FirebaseAuth


class NewUserWaitingProgramCreationFragment : ProgressFragment(
    "A few seconds before your personalized training program is ready"
) {

    private lateinit var tracking: Tracking

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tracking = Tracking(this, Screen.NewUserWaitingProgramCreation)

        val activityViewModel = ViewModelProvider(requireActivity()).get(NewUserViewModel::class.java)

        activityViewModel.currentUserCreationStep.observe(viewLifecycleOwner, Observer {
            if (activityViewModel.currentUserCreationStep.value == NewUserViewModel.UserCreationStep.Finished){

                FirebaseAuth.getInstance().currentUser?.let {
                    val loginMethod = (it.getIdToken(false).result?.signInProvider).orEmpty()
                    tracking.event(Event.SignUp(loginMethod))
                }

                startMainActivity(this)
            } else if (activityViewModel.currentUserCreationStep.value ==
                NewUserViewModel.UserCreationStep.UserAlreadyExists){

                AlertDialog.Builder(requireActivity())
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("User already exists")
                    .setCancelable(false)
                    .setMessage("Account already exists, do you want to load it instead?")
                    .setPositiveButton("Load existing user") { _, _ ->

                        FirebaseAuth.getInstance().currentUser?.let {
                            val loginMethod = (it.getIdToken(false).result?.signInProvider).orEmpty()
                            tracking.event(Event.Login(loginMethod))
                        }

                        startMainActivity(this)
                    } // will close NewUser and load main
                    .setNegativeButton("Erase existing user") { _, _ ->
                        val firebaseUser = FirebaseAuth.getInstance().currentUser!!
                        activityViewModel.createUser(firebaseUser.uid, firebaseUser.displayName!!, firebaseUser.photoUrl.toString(), true)
                    }.show()


            }
        })
    }


}
