package co.globers.globersapp.ui.newuser

import android.util.Log
import androidx.lifecycle.ViewModel
import co.globers.globersapp.firestore.entities.DbLogitParams
import co.globers.globersapp.firestore.entities.tokeepinsync.DbDictionary
import co.globers.globersapp.machinelearning.*
import co.globers.globersapp.utils.NonNullLiveData
import java.util.*
import kotlin.math.max
import kotlin.math.min


class NewUserLevelTestViewModel : ViewModel() {

    enum class Step {
        STARTING,
        TEST,
        FINISHED
    }

    data class State (
        val step: Step,
        val guessedWordsByTestStep: List<Map<String,Boolean>>,
        val currentProbaRangePhaseIndex: Int,
        val currentTestStepIndex: Int,
        val percentTestCompletion: Int,
        val finalLevelLogitParams: DbLogitParams?= null,
        val finalEstimatedNbWordsKnown: Int? = null
    )

    private data class ProbaRange(val minProba: Double, val maxProba: Double)

    companion object {
        private const val nbWordsToSelect = 6

        // TODO NICO: this should be configurable without app update ?
        private val probaRangePhases = arrayOf(
            ProbaRange(0.01, 0.99),
            ProbaRange(0.05, 0.95),
            ProbaRange(0.05, 0.95),
            ProbaRange(0.15, 0.85),
            ProbaRange(0.3, 0.7),
            ProbaRange(0.4, 0.6)
        )
        private const val LOG_TAG = "NewUserLevelTestVM"
    }

    private lateinit var dictionary: DbDictionary
    private lateinit var initLogit: Logit1DCoeffs
    private lateinit var wordsByLnFreqInv: SortedMap<Double, String>
    val state = NonNullLiveData(State(Step.STARTING, listOf(),-1, -1, 0))


    fun init(dictionary: DbDictionary){

        this.dictionary = dictionary
        initLogit = computeInitLogit()
        wordsByLnFreqInv = dictionary.words.map { it.value.lnFrequencyInverse to it.key }.toMap().toSortedMap()

        val words = selectWordsToTests(initLogit, probaRangePhases[0].maxProba, probaRangePhases[0].minProba, setOf())
        val guessedWords = words.map { it to false }.toMap()
        state.value = State(Step.TEST, listOf(guessedWords), 0, 0, 0)
    }

    private fun computeInitLogit(): Logit1DCoeffs {

        val rankToResult = listOf(
            500 to true,
            800 to false,
            1000 to true,
            2000 to true,
            2200 to true,
            3000 to false,
            5000 to false,
            10000 to true,
            11000 to false
        )

        val rankToLnFreq = dictionary.words.values.asSequence().map { it.wordRank to it.lnFrequencyInverse }.toMap().toSortedMap()

        val regressionParams = rankToResult.map {
            val rankInMap = rankToLnFreq.tailMap(it.first).firstKey()
            val freq = rankToLnFreq.getValue(rankInMap)
            freq to it.second
        }

        val result = regressionParams.map { it.second }.toBooleanArray()
        val input: DoubleArray = regressionParams.map { it.first }.toDoubleArray()

        val regressionResult = logisticRegression1D(result, input)

        return regressionResult.result!!
    }


    fun onSwitch(word: String){

        val currentState = state.value
        val currentWords = currentState.guessedWordsByTestStep[currentState.currentTestStepIndex]
        val newWordsStateThisElt = currentWords.map { it.key to if (it.key == word) !it.value else it.value}.toMap()
        val currentWordsState = currentState.guessedWordsByTestStep
        val newState = currentWordsState.mapIndexed { index, elt -> if (index == currentState.currentTestStepIndex) newWordsStateThisElt else elt }

        this.state.value = currentState.copy(guessedWordsByTestStep = newState)
    }

    fun next(){

        val currentState = this.state.value

        if (currentState.step == Step.FINISHED){
            // If we hit "next" very fast after test is finished and before event is dispatched so we switch screen, nothing to do we just wait
            return
        }

        val newGuessedWords = currentState.guessedWordsByTestStep

        val regressionParams = newGuessedWords
            .map { thisStepMap ->
                thisStepMap.map { dictionary.words.getValue(it.key).lnFrequencyInverse to it.value }
            }
            .flatten()
        val result = regressionParams.map { it.second }.toBooleanArray()
        val input: DoubleArray = regressionParams.map { it.first }.toDoubleArray()

        val regressionResult = logisticRegression1D(result, input)

        val regressionOk = regressionResult.state == LogisticRegressionResultState.OK && regressionResult.result!!.coeff < 0

        // If regression has result (separable), the result is valid (coeff < 0) and it was the last index => Test is finished
        if (currentState.currentProbaRangePhaseIndex == probaRangePhases.lastIndex && regressionOk) {
            val logit1DCoeffs = regressionResult.result!!
            val nbKnownWords = computeNbWordsKnown(logit1DCoeffs)
            Log.i(LOG_TAG, "NbWordsKnown estimated FINAL: $nbKnownWords")

            state.value = State(Step.FINISHED, listOf(), -1, -1, 100, DbLogitParams(logit1DCoeffs.intercept, logit1DCoeffs.coeff), nbKnownWords)
        }
        else {
            // next step

            val logit1DCoeffs: Logit1DCoeffs
            val nextProbaRangePhaseIndex: Int
            if (regressionOk)
            {
                logit1DCoeffs = regressionResult.result!! // If result is ok, result != null
                nextProbaRangePhaseIndex = currentState.currentProbaRangePhaseIndex + 1
            }
            else {
                logit1DCoeffs = initLogit
                nextProbaRangePhaseIndex = currentState.currentProbaRangePhaseIndex // we keep the same phase to add words till problem is
            }

            val nextProbaRange = probaRangePhases[nextProbaRangePhaseIndex]
            val forbiddenWords = currentState.guessedWordsByTestStep.flatMap { it.keys }.toSet()
            val newWords = selectWordsToTests(logit1DCoeffs, nextProbaRange.maxProba, nextProbaRange.minProba, forbiddenWords).map { it to false }.toMap()
            state.value = currentState.copy(
                guessedWordsByTestStep = currentState.guessedWordsByTestStep + newWords,
                currentProbaRangePhaseIndex = nextProbaRangePhaseIndex,
                currentTestStepIndex = currentState.currentTestStepIndex + 1,
                percentTestCompletion = max (100 * nextProbaRangePhaseIndex / probaRangePhases.size,  currentState.percentTestCompletion + 5 /* to display some movement even if regression failed...*/)
            )

            val nbKnownWords = computeNbWordsKnown(logit1DCoeffs)
            Log.i(LOG_TAG, "NbWordsKnown estimated: $nbKnownWords")
        }
    }

    private fun computeNbWordsKnown(logit1DCoeffs: Logit1DCoeffs): Int {

        var previousWordRank = 0
        var totalKnownWords = 0.0
        for(word in wordsByLnFreqInv){

            val rank = dictionary.words.getValue(word.value).wordRank
            val currentStepNbKnownWords = (rank - previousWordRank)* computeProba(logit1DCoeffs, word.key)
            totalKnownWords += currentStepNbKnownWords
            previousWordRank = rank
        }
        return totalKnownWords.toInt()
    }


    private fun selectWordsToTests(level: Logit1DCoeffs, maxProbaOfKnowing: Double, minProbaOfKnowing: Double, forbiddenWords: Set<String>): List<String>{

        var wordsInProbaRange: List<Pair<Double, String>>
        var minProbaCurrent = minProbaOfKnowing
        var maxProbaCurrent = maxProbaOfKnowing
        do {

            val maxLnFreqInverse = computeX(level, minProbaCurrent)
            val minLnFreqInverse = computeX(level, maxProbaCurrent)

            wordsInProbaRange = wordsByLnFreqInv
                .subMap(minLnFreqInverse, maxLnFreqInverse).toList()
                .filter { !forbiddenWords.contains(it.second) }

            minProbaCurrent = max( 0.9 * minProbaCurrent, 0.001)
            maxProbaCurrent = min ( 1.1 * maxProbaCurrent, 0.999)

        } while (wordsInProbaRange.size < nbWordsToSelect)


        val stepInNbWords = wordsInProbaRange.size.toDouble()/(nbWordsToSelect -1)
        val selectedWordsIndexes = (0 until nbWordsToSelect).map { min(wordsInProbaRange.size-1,(stepInNbWords * it).toInt()) }
        val selectedWords = selectedWordsIndexes.map { wordsInProbaRange[it].second }
        return selectedWords
    }
}
