package co.globers.globersapp.utils

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.google.firebase.analytics.FirebaseAnalytics


enum class Screen {
    MainSplashScreen,
    MainGetStarted,
    MainProgram,
    MainFriends,
    MainProfile,
    MainCallRequest,
    MainFindFriend,

    NewUserWaitingForTestLoading,
    NewUserLevelTest,
    NewUserLevelTestResult,
    NewUserWaitingProgramCreation,

    TabooPeerMatching,
    TabooStarting,
    TabooTalkerSide,
    TabooListenerSide,
    TabooListenerSideWithChoice,
    TabooFinished,
    TabooTeammateFeedback,
    TabooGiveAward,
    TabooReportUser
}



sealed class Event {

    abstract fun bundle(): Bundle
    abstract fun name(): String


    data class Login(val loginMethod: String): Event(){

        override fun name() = FirebaseAnalytics.Event.LOGIN
        override fun bundle() = Bundle().apply { putString(FirebaseAnalytics.Param.METHOD, loginMethod) }
    }

    data class SignUp(val loginMethod: String): Event(){

        override fun name() = FirebaseAnalytics.Event.SIGN_UP
        override fun bundle() = Bundle().apply { putString(FirebaseAnalytics.Param.METHOD, loginMethod) }
    }

    data class NewUserLevelTestStepCompleted(val stepIndex: Int): Event(){

        override fun name() = "new_user_level_test_step_completed"
        override fun bundle() = Bundle().apply { putLong(FirebaseAnalytics.Param.VALUE, stepIndex.toLong()) }
    }

}

// events interessants:
// select content
// search
// share

// tutorial begin, tutorial complete
// view item


class Tracking(fragment: Fragment, screen: Screen)  {

    private val firebaseAnalytics = FirebaseAnalytics.getInstance(fragment.requireContext())

    init {
        firebaseAnalytics.setCurrentScreen(fragment.requireActivity(), screen.name, null)
    }

    fun event(event: Event){
        firebaseAnalytics.logEvent(event.name(), event.bundle())
    }

}

