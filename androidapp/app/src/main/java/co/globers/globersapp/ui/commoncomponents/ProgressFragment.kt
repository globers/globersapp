package co.globers.globersapp.ui.commoncomponents

// https://antonioleiva.com/kotlin-android-extensions/

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import androidx.fragment.app.Fragment
import co.globers.globersapp.R
import kotlinx.android.synthetic.main.progress_fragment.*


abstract class ProgressFragment(private val message: String, private val cancelButtonMessage: String? = null) : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.progress_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        textView_progressFragment_message.text = message

        if (cancelButtonMessage == null){
            button_progressFragment_cancel.visibility = View.GONE
        } else {
            button_progressFragment_cancel.text = cancelButtonMessage
        }

        button_progressFragment_cancel.setOnClickListener { onCancelButtonClick() }

        val rotate = rotateAnimation()
        imageView_progressFragment_loading.startAnimation(rotate)

    }


    open fun onCancelButtonClick(){}

}

fun rotateAnimation(): RotateAnimation {
    val rotate = RotateAnimation(
        0f,
        360f,
        Animation.RELATIVE_TO_SELF,
        0.5f,
        Animation.RELATIVE_TO_SELF,
        0.5f
    )
    rotate.duration = 1000
    rotate.interpolator = AccelerateDecelerateInterpolator()
    rotate.repeatCount = Animation.INFINITE
    return rotate
}
