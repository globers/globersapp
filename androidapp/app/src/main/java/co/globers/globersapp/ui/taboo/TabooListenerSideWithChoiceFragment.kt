package co.globers.globersapp.ui.taboo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import co.globers.globersapp.R
import co.globers.globersapp.utils.Screen
import co.globers.globersapp.utils.Tracking
import com.google.android.flexbox.*
import kotlinx.android.synthetic.main.taboo_listener_side_with_choice_fragment.*


class TabooListenerSideWithChoiceFragment : Fragment() {

    companion object {
        fun newInstance() = TabooListenerSideWithChoiceFragment()
        const val LOG_TAG = "TabooListenerSideFr"

    }

    private lateinit var viewModel: TabooViewModel
    private var wordToGuess: String? = null
    private lateinit var tracking: Tracking


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.taboo_listener_side_with_choice_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tracking = Tracking(this, Screen.TabooListenerSideWithChoice)

        viewModel = ViewModelProvider(requireActivity()).get(TabooViewModel::class.java)

        val recyclerView = recyclerView_taboo_listenerSideWithChoice
        val flexboxManager = FlexboxLayoutManager(context)
        flexboxManager.flexWrap = FlexWrap.WRAP
        flexboxManager.justifyContent = JustifyContent.CENTER
        flexboxManager.alignItems = AlignItems.CENTER

        recyclerView.layoutManager = flexboxManager

        floatingActionButton_listenerSideWithChoice_help.setOnClickListener {
            buildListenerRulesSnackbar(requireView()).show()
        }

        val lifeImage = imageView_taboo_listenerSideWithChoice_life

        viewModel.state.observe( viewLifecycleOwner, Observer {

            if (it.status == TabooViewModel.Status.RunningGuesser){

                val activeCard = it.gameState!!.currentCard!!
                val wordToGuess = activeCard.wordToGuess

                if (wordToGuess != this.wordToGuess) { // wordToGuess has changed

                    this.wordToGuess = wordToGuess
                    recyclerView.adapter = TabooListenerSideWithChoiceRecyclerViewAdapter(activeCard.proposedWords)
                    { word: String ->
                        if (word == wordToGuess) {
                            viewModel.guessWord()
                        } else if (lifeImage.visibility == View.VISIBLE){
                            lifeImage.visibility = View.INVISIBLE
                        } else {
                            viewModel.buzzWordGuesser()
                        }
                    }


                }

            }

        })

    }

}



