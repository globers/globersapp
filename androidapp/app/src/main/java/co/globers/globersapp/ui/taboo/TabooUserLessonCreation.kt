package co.globers.globersapp.ui.taboo

import co.globers.globersapp.firestore.entities.DbTabooCardUserState
import co.globers.globersapp.firestore.entities.DbTabooCardUserStatus
import co.globers.globersapp.firestore.entities.DbTabooLessonUserState
import co.globers.globersapp.firestore.entities.DbTabooProgramUserState
import co.globers.globersapp.firestore.entities.tokeepinsync.DbTabooLesson
import co.globers.globersapp.firestore.entities.tokeepinsync.DbTabooProgram
import co.globers.globersapp.firestore.executeGet
import co.globers.globersapp.firestore.executeGetOrNull
import co.globers.globersapp.firestore.executeSet
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore



suspend fun getOrCreateUserLesson(
    userUid: String, userProgramUid: String, userLessonUid: String,
    programUid: String, lessonUid: String,
    db: FirebaseFirestore
): DbTabooLessonUserState {

    val userLessonDocRef = getUserLessonDocRef(
        userUid,
        userProgramUid,
        userLessonUid,
        db
    )
    return executeGetOrNull(userLessonDocRef)
        ?: createUserLesson(
            userUid, userProgramUid, userLessonUid,
            programUid, lessonUid,
            db
        )

}

private fun getUserLessonDocRef(userUid: String, userProgramUid: String, userLessonUid: String, db: FirebaseFirestore): DocumentReference {
    val userProgramDocRef =
        DbTabooProgramUserState.buildDocRef(
            db,
            userUid,
            userProgramUid
        )
    val userLessonsCol = userProgramDocRef.collection(DbTabooLessonUserState.COLLECTION_NAME)
    return userLessonsCol.document(userLessonUid)
}

private suspend fun createUserLesson(userUid: String, userProgramUid: String, userLessonUid: String, programUid: String, lessonUid: String, db: FirebaseFirestore): DbTabooLessonUserState {

    val lesson = getLesson(programUid, lessonUid, db)
    val cards = lesson.cards.map { it.uid to DbTabooCardUserState(
        DbTabooCardUserStatus.NotPlayed
    )
    }.toMap()
    val userLesson = DbTabooLessonUserState(
        userLessonUid,
        lesson.uid,
        cards
    )
    val userLessonDocRef = getUserLessonDocRef(
        userUid,
        userProgramUid,
        userLessonUid,
        db
    )
    executeSet(userLessonDocRef, userLesson)
    return userLesson
}

private suspend fun getLesson(programUid: String, lessonUid: String, db: FirebaseFirestore): DbTabooLesson {
    val programsCol = db.collection(DbTabooProgram.COLLECTION_NAME)
    val programDocRef = programsCol.document(programUid)
    val lessonsCol = programDocRef.collection(DbTabooLesson.COLLECTION_NAME)
    val lessonDocRef = lessonsCol.document(lessonUid)
    return executeGet(lessonDocRef)
}