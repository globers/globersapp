package co.globers.globersapp.ui.taboo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.taboo_report_user_fragment.*
import android.content.Context
import android.view.inputmethod.InputMethodManager
import co.globers.globersapp.utils.Screen
import co.globers.globersapp.utils.Tracking


class TabooReportUserFragment : Fragment() {

    companion object {
        fun newInstance() = TabooReportUserFragment()
    }

    private lateinit var tracking: Tracking

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(co.globers.globersapp.R.layout.taboo_report_user_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        tracking = Tracking(this, Screen.TabooReportUser)
        val viewModel = ViewModelProvider(this).get(TabooReportUserViewModel::class.java)
        val activityViewModel = ViewModelProvider(requireActivity()).get(TabooViewModel::class.java)
        val peerMatchResult = activityViewModel.peerMatchResult!!
        val peerMatch = peerMatchResult.peerMatch
        val isUserCreator = peerMatchResult.isCreator
        viewModel.init(peerMatch, isUserCreator)


        val adapter = TabooReportUserReasonTypeRecyclerViewAdapter()
        recyclerView_tabooReportUser_reportType.adapter = adapter
        recyclerView_tabooReportUser_reportType.layoutManager = LinearLayoutManager(context)

        adapter.selectedInappropriateBehaviorType.observe(viewLifecycleOwner, Observer {
            viewModel.setBehaviorType(it)
        })

        activityViewModel.state.observe(viewLifecycleOwner, Observer {
            if (it.status == TabooViewModel.Status.GameCompletedFeedbackFinished ||
                it.status == TabooViewModel.Status.GameAbortedFeedbackFinished){

                AlertDialog.Builder(requireActivity())
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle("Report sent")
                    .setCancelable(false)
                    .setMessage("Thanks for your help, we will take it into account shortly")
                    .setNeutralButton("Ok") {  _, _ ->

                        if (it.status == TabooViewModel.Status.GameCompletedFeedbackFinished) {
                            val action =
                                TabooReportUserFragmentDirections.actionTabooReportUserFragmentToTabooFinishedFragment()
                            findNavController().navigate(action)
                        }
                        else {
                            requireActivity().finish()
                        }
                    }
                    .show()
            }
        })

        viewModel.status.observe(viewLifecycleOwner, Observer {
            when(it.status){
                TabooReportUserViewModel.Status.NOT_SAVED -> {
                    button_tabooReportUser_confirmReport.isEnabled = (it.behaviorType != null)
                }
                TabooReportUserViewModel.Status.SAVE_IN_PROGRESS -> {
                    freezeScreen()
                }
                TabooReportUserViewModel.Status.FINISHED -> {
                    activityViewModel.setFeedbackGivenOrDiscarded()
                }
            }
        })

        button_tabooReportUser_cancel.setOnClickListener {
            requireActivity().onBackPressed()
        }

        button_tabooReportUser_confirmReport.setOnClickListener {
            viewModel.save(editText_tabooReportUser_details.text.toString())
        }


        editText_tabooReportUser_details.setOnFocusChangeListener { _, focus ->
            if (focus) {
                val imm =
                    requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.showSoftInput(editText_tabooReportUser_details, InputMethodManager.SHOW_IMPLICIT)
            }
            else {
                hideKeyboard()
            }
        }

        editText_tabooReportUser_details.setOnClickListener {
            val imm =
                requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editText_tabooReportUser_details, InputMethodManager.SHOW_IMPLICIT) }
    }


    private fun freezeScreen(){
        button_tabooReportUser_confirmReport.isEnabled = false
        button_tabooReportUser_cancel.isEnabled = false
    }


    override fun onPause() {
        super.onPause()
        hideKeyboard()
    }


    fun hideKeyboard() {
        val inputManager = requireActivity()
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        // check if no view has focus:
        val currentFocusedView = requireActivity().currentFocus
        if (currentFocusedView != null) {
            inputManager.hideSoftInputFromWindow(
                currentFocusedView.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
        }
    }


}
