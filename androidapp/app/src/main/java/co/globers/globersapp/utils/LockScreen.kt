package co.globers.globersapp.utils

import android.app.Activity
import android.app.KeyguardManager
import android.content.Context
import android.os.Build
import android.view.WindowManager


// https://stackoverflow.com/questions/48277302/android-o-flag-show-when-locked-is-deprecated
fun allowOnLockedScreen(activity: Activity){

    /*
val pm: PowerManager = getSystemService(Context.POWER_SERVICE) as PowerManager

wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "Globers:wakeupcall")
wl!!.acquire(30000)

*/

    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O_MR1) {
        @Suppress("DEPRECATION")
        activity.window.addFlags(
            WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON)
    } else {
        activity.setShowWhenLocked(true)
        activity.setTurnScreenOn(true)
        val keyguardManager = activity.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
        keyguardManager.requestDismissKeyguard(activity, null)
    }
}