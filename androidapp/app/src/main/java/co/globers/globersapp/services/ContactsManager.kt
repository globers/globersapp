package co.globers.globersapp.services


import co.globers.globersapp.firestore.*
import co.globers.globersapp.firestore.entities.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldPath
import com.google.firebase.firestore.FirebaseFirestore
import io.reactivex.Observable
import java.util.*


data class SentFriendRequest(
    val friendRequest: DbFriendRequest,
    val receiverPublicProfile: DbUserPublicProfile
)

data class ReceivedFriendRequest(
    val friendRequest: DbFriendRequest,
    val senderPublicProfile: DbUserPublicProfile
)


class ContactsManager {

    private val db = FirebaseFirestore.getInstance()
    private val auth = FirebaseAuth.getInstance()
    private val userDataObservables = UserDataObservables()


    suspend fun sendFriendRequest(friendUserUid: String){

        val userUid = auth.currentUser?.uid

        if (userUid != null) {

            val dbFriendRequest = DbFriendRequest(
                UUID.randomUUID().toString(),
                userUid,
                friendUserUid
            )

            executeSet(
                db.collection(DbFriendRequest.COLLECTION_NAME).document(dbFriendRequest.uid),
                dbFriendRequest
            )
        }
    }

    suspend fun replyToFriendRequest(friendRequestUid: String, reply: DbFriendRequestReply){

        val friendRequest = executeGet<DbFriendRequest>(db.collection(DbFriendRequest.COLLECTION_NAME).document(friendRequestUid))
        val senderProfile = executeGet<DbUserPublicProfile>(DbUserPublicProfile.buildDocRef(db, friendRequest.senderUserUid))

        executeBatch { batch ->

            batch.update(
                db.collection(DbFriendRequest.COLLECTION_NAME).document(friendRequest.uid),
                mapOf(
                    DbFriendRequest.FIELD_STATUS to DbFriendRequestStatus.REPLIED,
                    DbFriendRequest.FIELD_REPLY to reply
                )
            )

            batch.set(
                DbContact.getDocReference(db, friendRequest.receiverUserUid, friendRequest.senderUserUid),
                DbContact(
                    senderProfile,
                    if (reply == DbFriendRequestReply.ACCEPT) DbContactStatus.FRIEND else DbContactStatus.BLOCKED,
                    friendRequest.uid
                )
            )
        }
    }


    fun contacts(): Observable<List<DbContact>> = userDataObservables.getAuthenticatedUserUidObservable().flatMap { userUid ->
            listenQuery<DbContact>(DbContact.getCollectionReference(db, userUid))
        }

    fun sentFriendRequests(): Observable<List<SentFriendRequest>> {

        return userDataObservables.getAuthenticatedUserUidObservable()

            .flatMap { userUid ->

                val requestsObs = listenQuery<DbFriendRequest>(
                    db.collection(DbFriendRequest.COLLECTION_NAME)
                        .whereEqualTo(
                            FieldPath.of(
                                DbFriendRequest.FIELD_SENDER_USER_UID
                            ), userUid
                        )
                        .whereEqualTo(DbFriendRequest.FIELD_STATUS, DbFriendRequestStatus.PENDING)
                )

                requestsObs

            }

            .flatMap { requestList ->

                if (requestList.isEmpty()){
                    Observable.just(listOf())
                } else {

                    val profileObsList = requestList.map { request ->
                        listenOnce<DbUserPublicProfile>(
                            DbUserPublicProfile.buildDocRef(
                                db,
                                request.receiverUserUid
                            )
                        ).map { SentFriendRequest(request, it) }
                    }


                    val result =
                        Observable.combineLatest<SentFriendRequest, List<SentFriendRequest>>(
                            profileObsList
                        ) { sentRequests: Array<Any> ->
                            sentRequests.map { it as SentFriendRequest }.toList()
                        }
                    result
                }

            }

    }


    fun receivedFriendRequests(): Observable<List<ReceivedFriendRequest>> = userDataObservables.getAuthenticatedUserUidObservable()

        .flatMap { userUid ->
        listenQuery<DbFriendRequest>(
            db.collection(DbFriendRequest.COLLECTION_NAME)
                .whereEqualTo(
                    FieldPath.of(
                        DbFriendRequest.FIELD_RECEIVER_USER_UID
                    ), userUid
                )
                .whereEqualTo(DbFriendRequest.FIELD_STATUS, DbFriendRequestStatus.PENDING)
        )
    }
        .flatMap { requestList ->

            if (requestList.isEmpty()){
                Observable.just(listOf()) // combineLatest with an empty list of observable never return
            } else {

                val profileObsList = requestList.map { request ->
                    listenOnce<DbUserPublicProfile>(
                        DbUserPublicProfile.buildDocRef(
                            db,
                            request.senderUserUid
                        )
                    ).map { ReceivedFriendRequest(request, it) }
                }

                val result =
                    Observable.combineLatest<ReceivedFriendRequest, List<ReceivedFriendRequest>>(
                        profileObsList
                    ) { sentRequests: Array<Any> ->
                        sentRequests.map { it as ReceivedFriendRequest }.toList()
                    }
                result
            }

        }




    suspend fun changeContactStatus(contact: DbContact, status: DbContactStatus) {

        val userUid = auth.currentUser?.uid
        if (userUid != null) {
            executeUpdate(
                DbContact.getDocReference(db, userUid, contact.publicProfile.userUid),
                FieldPath.of(DbContact.FIELD_STATUS),
                status
            )
        }
    }

    suspend fun cancelFriendRequest(request: SentFriendRequest) {

        executeUpdate(
            db.collection(DbFriendRequest.COLLECTION_NAME).document(request.friendRequest.uid),
            FieldPath.of(DbFriendRequest.FIELD_STATUS),
            DbFriendRequestStatus.CANCELED_BY_SENDER
        )

    }


}