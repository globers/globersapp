package co.globers.globersapp

import android.util.Log
import co.globers.globersapp.utils.BugException
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


sealed class AppParams<T>(protected val key: String, protected val defaultValue: T) {

    companion object {
        private const val LOG_TAG = "AppParams"
        private val remoteConfig = FirebaseRemoteConfig.getInstance()

        suspend fun init(){

            // Default interval is 12H, should use this code ONLY in test

            /*
            val configSettings = FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(30)
                .build()


            remoteConfig.setConfigSettingsAsync(configSettings)
            */





            val config = mapOf<String, Any>(
                UxCamActivated.key to UxCamActivated.defaultValue
            )


            return suspendCoroutine { cont: Continuation<Unit> ->

                remoteConfig.setDefaultsAsync(config).addOnCompleteListener {
                    remoteConfig.fetchAndActivate().addOnCompleteListener {

                        if (!it.isSuccessful){
                            Log.e(LOG_TAG, "error loading remoteConfig", it.exception)
                        } else {
                            Log.d(LOG_TAG, "loading remoteConfig success")
                        }
                        cont.resume(Unit)
                    }
                }

            }
        }
    }

    fun get(): T {
        return when (defaultValue){
            is Boolean -> remoteConfig.getBoolean(key) as T
            is Double -> remoteConfig.getDouble(key) as T
            is Long -> remoteConfig.getLong(key) as T
            is String -> remoteConfig.getString(key) as T
            else -> throw BugException("AppParam invalid type")
        }
    }

    object UxCamActivated: AppParams<Boolean>("UX_CAM_ACTIVATED", false)
}

object AppConfig {

    const val COEFF_PEER_MATCHING_NB_WORD_KNOWN_SPREAD = 10
    const val TABOO_GAME_NB_FORBIDDEN_WORDS = 3
    const val TABOO_GAME_DURATION_IN_SEC = 5*60
    const val TABOO_GAME_MIN_CARD_TO_PLAY = 20
    const val WORDNIK_API_KEY = "f2nnhuslui6rfe0o1lhxdc1c5vcxnsh1g2utlwufrsyoij7p2"
    const val UXCAM_API_KEY = "jgvto2njq7d2qjf"

}