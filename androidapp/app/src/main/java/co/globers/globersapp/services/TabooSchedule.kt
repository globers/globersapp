package co.globers.globersapp.services

import android.os.Handler
import co.globers.globersapp.firestore.entities.tokeepinsync.DbTabooSchedule
import java.util.*


class TabooSchedule(val dbTabooSchedule: DbTabooSchedule, val onSessionStarted: (endSession: Date) -> Unit, val onSessionEnded: (nextStartSession: Date) -> Unit) {

    private data class Session(val start: Date, val end: Date){

        fun isCurrent(): Boolean {
            val now = Date()
            return start <= now && end > now
        }

        fun msToStart(): Long {
            val now = Date()
            return start.time - now.time
        }

        fun msToEnd(): Long {
            val now = Date()
            return end.time - now.time
        }

    }

    private val startSessionEventHandler = Handler()
    private val endSessionEventHandler = Handler()

    init {
        val currentOrNextSession = getCurrentOrNextSession(false)
        if (currentOrNextSession.isCurrent()){
            onSessionStarted(currentOrNextSession.end)
            subscribeToEndSession(currentOrNextSession)
        } else {
            onSessionEnded(currentOrNextSession.start)
            subscribeToNextStartSession(currentOrNextSession)
        }
    }

    private fun getCurrentOrNextSession(onlyNext: Boolean): Session {

        val now = Date()
        val nowInMsSinceEpoch = now.time

        val midnightInMsSinceEpoch = getMidnightInMsSinceEpoch()

        val nowInMsSinceMidnightUtc = nowInMsSinceEpoch - midnightInMsSinceEpoch

        // the first for which the end is after now
        val endNextOrCurrentSessionInMsSinceMidnightUtc = if (onlyNext) {
            dbTabooSchedule.matchingTimesInMsSinceMidnightUtc.first { it > nowInMsSinceMidnightUtc }
        } else {
            dbTabooSchedule.matchingTimesInMsSinceMidnightUtc.first { it + dbTabooSchedule.matchingDurationInMs >= nowInMsSinceMidnightUtc }
        }

        val startNextOrCurrentSessionInMsSinceMidnightUtc = endNextOrCurrentSessionInMsSinceMidnightUtc - dbTabooSchedule.matchingDurationInMs

        val startDateCurrentOrNextSession = Date(midnightInMsSinceEpoch + startNextOrCurrentSessionInMsSinceMidnightUtc)
        val endDateCurrentOrNextSession = Date(midnightInMsSinceEpoch + endNextOrCurrentSessionInMsSinceMidnightUtc)

        return Session(
            startDateCurrentOrNextSession, endDateCurrentOrNextSession
        )
    }



    private fun subscribeToNextStartSession(nextSession: Session) {

        startSessionEventHandler.postDelayed(
            {
                onSessionStarted(nextSession.end)
                subscribeToEndSession(nextSession)
            },
            nextSession.msToStart())
    }

    private fun subscribeToEndSession(currentSession: Session) {

        val nextSession = getCurrentOrNextSession(true)
        endSessionEventHandler.postDelayed(
            {
                onSessionEnded(nextSession.start)
                subscribeToNextStartSession(nextSession)
            },
            currentSession.msToEnd()
        )
    }

    fun unsubscribe(){
        startSessionEventHandler.removeCallbacksAndMessages(null)
        endSessionEventHandler.removeCallbacksAndMessages(null)
    }


}

private fun getMidnightInMsSinceEpoch(): Long {
    val calendarUtcMidnight = Calendar.getInstance(TimeZone.getTimeZone("GMT"))
        .apply {
            set(Calendar.HOUR_OF_DAY, 0)
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
        }

    return calendarUtcMidnight.timeInMillis
}