package co.globers.globersapp.firestore.entities

import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ServerTimestamp
import java.util.*


data class DbTabooProgramUserState(
    val uid: String,
    val programUid: String,
    val startLevelUid: String,
    val levelUidToState: Map<String,DbTabooLevelUserState>,
    @ServerTimestamp
    val creationDate: Date = Date()
)
{
    constructor(): this( "","", "", mapOf())

    companion object {
        const val COLLECTION_NAME = "tabooProgramUserStates"
        const val FIELD_LEVEL_UID_TO_STATE = "levelUidToState"

        fun buildDocRef(db: FirebaseFirestore, userUid: String, uid: String) =
            db.collection(DbUser.COLLECTION_NAME).document(userUid).collection(COLLECTION_NAME).document(uid)
    }
}

data class DbTabooLevelUserState(
    val lessonUidToStateSummary: Map<String, DbTabooLessonUserStateSummary>
){
    constructor(): this(mapOf())

    companion object {
        const val FIELD_LESSON_UID_TO_STATE_SUMMARY = "lessonUidToStateSummary"
    }
}

data class DbTabooLessonUserStateSummary(
    val userLessonUid: String,
    val nbCardsCompleted: Int
)
{
    constructor(): this("",-1)

    companion object {
        const val FIELD_NB_CARDS_COMPLETED = "nbCardsCompleted"
    }

}

data class DbTabooLessonUserState(
    val uid: String,
    val lessonUid:String,
    val cardUidToState: Map<String, DbTabooCardUserState>
)
{
    constructor(): this("",  "", mapOf())

    companion object {
        const val COLLECTION_NAME = "tabooUserLessons"
        const val FIELD_CARD_UID_TO_STATE = "cardUidToState"


        fun buildDocRefFromProgramUserState(programUserStateDocRef: DocumentReference, userLessonUid: String) =
            programUserStateDocRef.collection(COLLECTION_NAME).document(userLessonUid)

        fun buildDocRef(db: FirebaseFirestore, userUid: String, userProgramUid: String, userLessonUid: String) =
            buildDocRefFromProgramUserState(DbTabooProgramUserState.buildDocRef(
                db,
                userUid,
                userProgramUid
            ),userLessonUid)
    }
}

enum class DbTabooCardUserStatus{
    NotPlayed,
    Found,
    NotFound
}

data class DbTabooCardUserState(
    val status: DbTabooCardUserStatus
)
{
    companion object {
        const val FIELD_CARD_STATUS = "status"
    }

    constructor(): this(DbTabooCardUserStatus.NotPlayed)
}




