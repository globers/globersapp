package co.globers.globersapp.services

import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import co.globers.globersapp.firestore.entities.DbTabooGameCallRequest
import co.globers.globersapp.firestore.entities.DbTabooGameCallRequestStatus
import co.globers.globersapp.firestore.executeUpdate
import co.globers.globersapp.firestore.listen
import co.globers.globersapp.firestore.listenNullable
import co.globers.globersapp.ui.taboo.TabooActivity
import co.globers.globersapp.ui.taboo.TabooActivityArgs
import co.globers.globersapp.ui.taboo.TabooActivityTeammateArgs
import com.google.firebase.firestore.FieldPath
import com.google.firebase.firestore.FirebaseFirestore
import io.reactivex.Observable
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class ReceivedPhoneCallManager(
    private val context: Context,
    private val notificationId: Int,
    private val callRequestUid: String,
    private val peerMatchUid: String,
    private val callerUid: String){


    private val docRef = FirebaseFirestore.getInstance().collection(DbTabooGameCallRequest.COLLECTION_NAME).document(callRequestUid)

    val callRequestClosed: Observable<Unit> = listenNullable<DbTabooGameCallRequest>(docRef)
        .filter { it.value != null }.map { it.value } // if we listen to soon, it might be null (eventually consistent), because we start this subscription from receiving server notif
        .filter {it.status !=  DbTabooGameCallRequestStatus.SENT && it.status !=  DbTabooGameCallRequestStatus.RECEIVED }
        .map { Unit }
        .doOnEach { closeNotification() }
        .take(1)

    fun declinePhoneCall(){
        closeNotification()
        setReplyStatusOnDbCallRequest(
            callRequestUid,
            DbTabooGameCallRequestStatus.DECLINED_BY_CALLED
        )
    }

    fun notifyPhoneCallReceived(){
        MainScope().launch {
            executeUpdate(
                docRef,
                FieldPath.of(DbTabooGameCallRequest.FIELD_STATUS),
                DbTabooGameCallRequestStatus.RECEIVED
            )
        }
    }

    fun acceptPhoneCall(){
        closeNotification()

        setReplyStatusOnDbCallRequest(
            callRequestUid,
            DbTabooGameCallRequestStatus.ACCEPTED_BY_CALLED
        )
        startTabooActivity()
    }

    private fun closeNotification() {
        // to close notification when button is clicked
        val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.cancel(notificationId)
    }

    private fun startTabooActivity() {

        val intent = Intent(context, TabooActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK // must be added to start activity from outside activity
        val tabooArgs = TabooActivityArgs(
            null,
            TabooActivityTeammateArgs(
                callerUid,
                peerMatchUid,
                false
            ),
            false
        )
        intent.putExtra(TabooActivity.EXTRA_TABOO_ACTIVITY_ARGS, tabooArgs)
        context.startActivity(intent)
    }


}

private fun setReplyStatusOnDbCallRequest(callRequestUid: String, status: DbTabooGameCallRequestStatus){
    MainScope().launch {
        executeUpdate(
            DbTabooGameCallRequest.buildDocRef(
                FirebaseFirestore.getInstance(),
                callRequestUid
            ),
            FieldPath.of(DbTabooGameCallRequest.FIELD_STATUS),
            status
        )
    }
}



