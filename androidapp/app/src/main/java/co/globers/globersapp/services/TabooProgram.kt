package co.globers.globersapp.services

import co.globers.globersapp.firestore.entities.DbLearnedLanguage
import co.globers.globersapp.firestore.entities.DbTabooLessonUserStateSummary
import co.globers.globersapp.firestore.entities.tokeepinsync.DbTabooLessonSummary
import io.reactivex.Observable
import io.reactivex.rxkotlin.Observables
import kotlin.math.max
import kotlin.math.min


class TabooProgram(
    val programUid: String,
    val userProgramUid: String,
    val label: String,
    val learnedLanguage: DbLearnedLanguage,
    val levels: List<TabooLevel>
)

enum class LevelStatus {
    SKIPPED,
    COMPLETED,
    CURRENT,
    NOT_STARTED
}

enum class LessonStatus {
    SKIPPED,
    COMPLETED,
    CURRENT,
    NOT_STARTED
}

class TabooLevel(
    val levelUid: String,
    val label: String,
    val levelStatus: LevelStatus,
    val lessons: List<TabooLesson>
)


class TabooLesson(
    val lessonUid: String,
    val userLessonUid: String,
    val label: String,
    val status: LessonStatus,
    val nbCardsFound: Int,
    val nbCardsObjective: Int,
    val imageUrl: String
)

class TabooProgramService{

    fun getTabooProgramObservable(): Observable<TabooProgram> {

        val userDataObservables = UserDataObservables()

        val dbUserObs = userDataObservables.getUserObservable()
        val dbProgramObs = userDataObservables.getProgramObservable()
        val dbProgUserState = userDataObservables.getProgramUserStateObservable()

        val programObs = Observables.combineLatest(
            dbUserObs,
            dbProgramObs,
            dbProgUserState
        )
        { user, program, programUserState ->


            val dbLevels = program.levels

            val startLevelIndex =
                program.levels.indexOfFirst { it.uid == programUserState.startLevelUid }

            val lastCompletedLevel = max(startLevelIndex - 1, dbLevels.indexOfLast { level ->
                val lastLesson = level.lessons.last()
                val userLevel = programUserState.levelUidToState.getValue(level.uid)
                isLessonCompleted(
                    level.lessons.last(),
                    userLevel.lessonUidToStateSummary.getValue(lastLesson.lessonUid)
                )
            })

            val currentLevelIndex = min(
                lastCompletedLevel + 1,
                dbLevels.lastIndex
            ) // min to manage case where all levels are completed
            val currentLevel = dbLevels[currentLevelIndex]

            val currentLevelLessons = dbLevels[currentLevelIndex].lessons
            val currentUserLevel = programUserState.levelUidToState.getValue(currentLevel.uid)

            val currentLessonInCurrentLevel = currentLevelLessons
                .indexOfLast { lesson ->
                    val userLesson =
                        currentUserLevel.lessonUidToStateSummary.getValue(lesson.lessonUid)
                    isLessonCompleted(lesson, userLesson)
                } + 1


            val tabooLevels = dbLevels.mapIndexed { indexLevel, level ->

                val userLevel = programUserState.levelUidToState.getValue(level.uid)

                val levelStatus = when {
                    indexLevel < startLevelIndex -> LevelStatus.SKIPPED
                    indexLevel <= lastCompletedLevel -> LevelStatus.COMPLETED
                    indexLevel == lastCompletedLevel + 1 -> LevelStatus.CURRENT
                    else -> LevelStatus.NOT_STARTED
                }

                TabooLevel(
                    level.uid,
                    level.label,
                    levelStatus,
                    level.lessons.mapIndexed { indexLesson, lesson ->

                        val userLesson =
                            userLevel.lessonUidToStateSummary.getValue(lesson.lessonUid)

                        val lessonStatus = when (levelStatus) {
                            LevelStatus.SKIPPED -> LessonStatus.SKIPPED
                            LevelStatus.COMPLETED -> LessonStatus.COMPLETED
                            LevelStatus.NOT_STARTED -> LessonStatus.NOT_STARTED
                            LevelStatus.CURRENT -> {

                                when {
                                    indexLesson < currentLessonInCurrentLevel -> LessonStatus.COMPLETED
                                    indexLesson == currentLessonInCurrentLevel -> LessonStatus.CURRENT
                                    else -> LessonStatus.NOT_STARTED
                                    // Skipped don't exists if level is "current"
                                }
                            }
                        }

                        TabooLesson(
                            lesson.lessonUid,
                            userLesson.userLessonUid,
                            lesson.label,
                            lessonStatus,
                            userLesson.nbCardsCompleted,
                            nbCardsObjective(lesson),
                            lesson.imageUrl
                        )
                    })
            }

            TabooProgram(
                program.uid,
                programUserState.uid,
                program.label,
                user.learnedLanguages.first { it.languageIsoCode == program.languageIsoCode },
                tabooLevels
            )
        }
        return programObs
    }

}

private fun nbCardsObjective(lesson: DbTabooLessonSummary) =  min ( (0.999 * lesson.nbCards).toInt(), lesson.nbCards - 0) //prod: for example 0.8, -5

private fun isLessonCompleted(lesson: DbTabooLessonSummary, lessonUserState: DbTabooLessonUserStateSummary): Boolean {
    return lessonUserState.nbCardsCompleted >= nbCardsObjective(lesson)
}