package co.globers.globersapp.ui.main

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import co.globers.globersapp.R
import co.globers.globersapp.utils.Screen
import co.globers.globersapp.utils.Tracking
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.main_friends_fragment.*


class MainFriendsFragment : Fragment() {

    companion object {
        fun newInstance() = MainFriendsFragment()
    }

    private lateinit var viewModel: MainFriendsViewModel
    private lateinit var tracking: Tracking


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_friends_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tracking = Tracking(this, Screen.MainFriends)
        val bottomNavView = requireActivity().main_bottom_navigation_view
        bottomNavView.visibility = View.VISIBLE

        // owner is ACTIVITY instead of THIS to keep state inside MainFriendsViewModel
        viewModel = ViewModelProvider(requireActivity()).get(MainFriendsViewModel::class.java)


        viewModel.contactsToDisplay.observe(viewLifecycleOwner, Observer { contacts ->
            recyclerView_mainFriends_friendsList.adapter = MainFriendsRecyclerViewAdapter(viewModel, contacts)
            recyclerView_mainFriends_friendsList.layoutManager = LinearLayoutManager(requireActivity())
        })

        viewModel.calledContact.observe(viewLifecycleOwner, Observer { contact ->
            Log.d("MainFriendsFrag", "calledContactEvent, contact:${contact}")
            if (contact != null){
                val action = MainFriendsFragmentDirections.actionMainFriendsFragmentToMainCallRequestFragment(
                    contact.uid, contact.publicProfile.displayName)
                findNavController().navigate(action)
            }
        })


        viewModel.status.observe(viewLifecycleOwner, Observer { status: MainFriendsViewModel.Status ->

            when(status){
                MainFriendsViewModel.Status.USERS_NOT_LOADED -> {
                    button_mainFriends_findFriends.visibility = View.VISIBLE
                    button_mainFriends_findFriends.isEnabled = true
                }
                MainFriendsViewModel.Status.LOADING_USERS_FROM_CONTACT -> {
                    button_mainFriends_findFriends.visibility = View.VISIBLE
                    button_mainFriends_findFriends.isEnabled = false
                }
                MainFriendsViewModel.Status.LOADED -> {
                    button_mainFriends_findFriends.visibility = View.GONE
                }
            }

        })

        button_mainFriends_findFriends.setOnClickListener {

            val action = MainFriendsFragmentDirections.actionMainFriendsFragmentToMainFindFriendFragment()
            findNavController().navigate(action)
        }


        button_mainFriends_invitFriend.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_TEXT, "Let's play together to practice English! Download Globers at https://play.google.com/store/apps/details?id=co.globers.globersapp")

            startActivity(Intent.createChooser(intent, null))
        }

    }

    override fun onResume() {
        super.onResume()
        // next call has been cancelled, we go back to friendsFragment => we can now call someone else
        viewModel.calledContact.value = null
    }

}


// Afficher:
// - 1) la liste des friends: userName, contactName si chargé depuis la contactList, un boutton "Invit to play" -> create un PeerMatch special et attend qu'il soit joined
// - 3) un boutton "find friends already on Globers"
// - 2) un boutton "invite friends": just send a sms

//3) ce qui se passe en background
// 3.1) on charge tous le repertoire
// 3.2) pour chaque numéro on regarde s'il est dans la base
// 3.3) si oui on envoie une friendRequest

// 3.4) quand on reçois un friendRequest, on a le numéro du sender
// 3.5) Si il est dans le repertoire => on l'ajoute au friends avec son propre contact
// 3.6) et on envoie en retour une autre friendRequest. Le user 1 va la recevoir et donc aussi créer le friend



// Inviter
// Le user arrive sur l'app
// Il clique sur une lesson
// Il voit 2 bouttons: Play with random teammate / Play with friend
// Si random teammate => looking for teammate: immediatement
// si friend => on part sur l'onglet "friend" en ayant noté la lesson choisie
// Puis click sur friend lance le call et affiche un screen "calling TOTO"
// Sur random teammate: si personne au bout de X secondes: Message avec "Retry" / "Play with friend" / "Close"

// Process friend
// On créé un "PlayRequest" ou un "PeerMatching" spécial dans la base
// on déclenche une function sur firebase, celle-ci check si le user n'est pas bloqué, pui envoie la notif


// comment savoir qu'on a été friendé ?
// quand A friend B, on ajoute une "FriendRequest"
// dans l'écran d'affichage des amis, on affiche les requests
// emitter => pending, receiver: 2 options Accept / refuse
// "Accept" => friend the other, "Refuse" => Ask confirmation & block the user


// KEEP IT SIMPLE - STUPID

// RULES: on doit pas pouvoir etre contacté sans avoir donné au préalable son authorisation
// Corrolaire, on doit pas pouvoir etre contacté sans avoir ajouté en friends


// Si A friend B, on envoie une requette "A added you as a friend on Globers"
// 2 solutions : "Accept or do nothing" => add as a friend
// "Block user" =>



// comment bloquer user ?
