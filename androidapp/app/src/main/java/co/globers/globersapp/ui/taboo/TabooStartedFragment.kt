package co.globers.globersapp.ui.taboo

import android.content.Context
import android.media.MediaPlayer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.whenCreated
import androidx.navigation.fragment.findNavController
import co.globers.globersapp.R
import co.globers.globersapp.firestore.entities.DbTabooCardAction
import co.globers.globersapp.services.TabooGame
import kotlinx.android.synthetic.main.taboo_started_fragment.*

class TabooStartedFragment : Fragment() {

    companion object {
        fun newInstance() = TabooStartedFragment()
    }

    private lateinit var activityViewModel: TabooViewModel
    private lateinit var passedCardMediaPlayer: MediaPlayer
    private lateinit var foundCardMediaPlayer: MediaPlayer
    private lateinit var buzzedCardMediaPlayer: MediaPlayer
    private lateinit var endGameMediaPlayer: MediaPlayer

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.taboo_started_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activityViewModel = ViewModelProvider(requireActivity()).get(TabooViewModel::class.java)

        val peerMatch = activityViewModel.peerMatchResult!!
        val otherUserInfo = if (peerMatch.isCreator)
            peerMatch.peerMatch.joinerGameInfo!!
        else
            peerMatch.peerMatch.creatorGameInfo

        val otherName = otherUserInfo.displayName
        val otherLesson = otherUserInfo.lessonLabel

        textView_tabooStartedFragment_teammate.text = "$otherName / $otherLesson"

        val timerProgressBar = progressBar_tabooStartedFragment_timer

        manageGameInterrupted(activityViewModel, this)

        activityViewModel.state.observe( viewLifecycleOwner, Observer {

            val status = it.status

            if (status == TabooViewModel.Status.RunningTalker || status == TabooViewModel.Status.RunningGuesser){

                val gameState = it.gameState!!
                val nbGuessed = gameState.cardsPlayed.count { it.cardPlayed.action == DbTabooCardAction.FOUND }

                val lastCardPlayed = gameState.cardsPlayed.lastOrNull()
                if (lastCardPlayed != null){
                    when(lastCardPlayed.cardPlayed.action){
                        DbTabooCardAction.PASSED -> {passedCardMediaPlayer.start()}
                        DbTabooCardAction.FOUND -> {foundCardMediaPlayer.start()}
                        DbTabooCardAction.BUZZED_GUESSER -> {buzzedCardMediaPlayer.start()}
                    }
                }

                textView_tabooStartedFragment_nbGuessedCards.text = nbGuessed.toString()

                val fragment = if (gameState.status == TabooGame.Status.RunningTalker){
                    TabooTalkerSideFragment.newInstance()
                }
                else {
                    TabooListenerSideWithChoiceFragment.newInstance()
                }
                val transaction: FragmentTransaction = childFragmentManager.beginTransaction()
                transaction.add(R.id.frameLayout_tabooStartedFragment_talkerListener, fragment)
                transaction.commit()

            } else if (status == TabooViewModel.Status.Finished){
                endGameMediaPlayer.setOnCompletionListener {
                    val action = TabooStartedFragmentDirections.actionTabooStartedFragmentToTabooFinishedFragment()
                    findNavController().navigate(action)
                }
                endGameMediaPlayer.start()
            }
        })


        button_tabooStarted_quitGame.setOnClickListener {

            AlertDialog.Builder(requireActivity())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Quit game?")
                .setMessage("Are you sure you want to quit the game?")
                .setPositiveButton("Quit game") { _, _ ->
                    activityViewModel.quitGame() }
                .setNegativeButton("No", null)
                .show()
        }

        activityViewModel.remainingPercentTime.observe(viewLifecycleOwner, Observer {
            timerProgressBar.progress = it
        })


    }

    override fun onStart() {
        super.onStart()
        passedCardMediaPlayer = MediaPlayer.create(context, R.raw.sound_pass)
        foundCardMediaPlayer = MediaPlayer.create(context, R.raw.sound_correct)
        buzzedCardMediaPlayer = MediaPlayer.create(context, R.raw.sound_error)
        endGameMediaPlayer = MediaPlayer.create(context, R.raw.sound_endgame)

    }

    override fun onStop() {
        super.onStop()
        passedCardMediaPlayer.release()
        foundCardMediaPlayer.release()
        buzzedCardMediaPlayer.release()
        endGameMediaPlayer.release()
    }

}
