package co.globers.globersapp.firestore.entities.tokeepinsync

import co.globers.globersapp.firestore.entities.DbLanguageIsoCode
import com.google.firebase.Timestamp
import com.google.firebase.firestore.ServerTimestamp


enum class DbProgramStatus {
    Beta,
    Active,
    Deprecated
}

data class DbTabooProgram(
    val uid: String,
    val label: String,
    val languageIsoCode: DbLanguageIsoCode,
    val status: DbProgramStatus,
    val insertionBatchTag: String,
    val levels: List<DbTabooLevel>,
    @ServerTimestamp
    val creationDate: Timestamp = Timestamp.now())
{
    companion object {
        const val COLLECTION_NAME = "tabooPrograms"
        const val FIELD_LANGUAGE_ISO_CODE = "languageIsoCode"
        const val FIELD_STATUS = "status"
    }

    constructor(): this("", "",
        DbLanguageIsoCode.ENG,
        DbProgramStatus.Active, "", listOf())
}

data class DbTabooLevel(
    val uid: String,
    val label: String,
    val averageLnFrequencyInverse: Double,
    val lessons: List<DbTabooLessonSummary>
){
    constructor(): this("", "", -1.0, listOf())
}

data class DbTabooLessonSummary(
    val lessonUid:String,
    val nbCards: Int,
    val label: String, // Ex: "Verbs 2",
    val imageUrl: String
)
{
    constructor(): this("", -1, "", "")
}


data class DbTabooLesson(
    val uid: String,
    val cards: List<DbTabooCard>
)
{

    constructor(): this("", listOf())

    companion object {
        const val COLLECTION_NAME = "TabooLessons"
    }
}


data class DbTabooCard(
    val uid: String,
    val wordToGuess: String,
    val forbiddenWords: List<String>)
{
    constructor(): this("", "", listOf())
}