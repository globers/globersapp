package co.globers.globersapp.notifservice

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Parcelable
import android.util.Log
import co.globers.globersapp.services.ReceivedPhoneCallManager
import kotlinx.android.parcel.Parcelize


// https://stackoverflow.com/questions/38073139/how-to-close-the-notification-dialog-after-click-the-dismiss-button-without-cal
class CallReplyAcceptBroadcastReceiver : BroadcastReceiver() {

    companion object{
        const val EXTRA_ARGS = "CALL_REPLY_ACCEPT_BR_EXTRA_ARGS"
        const val LOG_TAG = "CallReplyAcceptBR"
    }

    @Parcelize
    data class Args(
        val notificationId: Int,
        val callRequestUid: String,
        val callerUid: String,
        val peerMatchUid: String
    ) : Parcelable


    override fun onReceive(context: Context, intent: Intent) {

        val args = intent.getParcelableExtra<Args>(EXTRA_ARGS)!!

        Log.d(LOG_TAG, "CallReplyAcceptBroadcastReceiver:onReceive, args:$args")

        ReceivedPhoneCallManager(context, args.notificationId, args.callRequestUid, args.peerMatchUid, args.callerUid)
            .acceptPhoneCall()
    }

}


class CallReplyDeclineBroadcastReceiver : BroadcastReceiver() {

    companion object{
        const val EXTRA_ARGS = "EXTRA_ARGS"
        const val LOG_TAG = "CallReplyDeclineBR"
    }

    @Parcelize
    data class Args(
        val notificationId: Int,
        val callRequestUid: String,
        val peerMatchUid: String,
        val callerUid: String
    ) : Parcelable


    override fun onReceive(context: Context, intent: Intent) {

        val args = intent.getParcelableExtra<Args>(EXTRA_ARGS)!!

        Log.d(LOG_TAG, "CallReplyDeclineBroadcastReceiver:onReceive, args:$args")
        ReceivedPhoneCallManager(context, args.notificationId, args.callRequestUid, args.peerMatchUid, args.callerUid)
            .declinePhoneCall()
    }


}
