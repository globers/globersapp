package co.globers.globersapp.firestore.entities


data class DbUserGameInfo(
    val userUid: String,
    val displayName: String,
    val userPhotoUrl: String?,
    val userCountryIsoCode: String,
    val userNativeLanguageLabel: String,
    val learnedLanguageIsoCode: DbLanguageIsoCode,
    val nbWordsKnownInGameLanguage: Int,
    val userProgramUid: String,
    val levelUid: String,
    val programUid: String,
    val programLabel: String,
    val levelLabel: String,
    val lessonLabel: String,
    val tabooLessonUserState: DbTabooLessonUserState
)
{

    companion object {
        const val FIELD_LEARNED_LANGUAGE_ISO_CODE = "learnedLanguageIsoCode"
    }

    constructor(): this("", "", "", "", "", DbLanguageIsoCode.ENG, -1,"", "", "", "",
        "", "", DbTabooLessonUserState())
}