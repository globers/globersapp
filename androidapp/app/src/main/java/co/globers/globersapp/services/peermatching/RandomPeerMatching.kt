package co.globers.globersapp.services.peermatching

import android.os.CountDownTimer
import android.util.Log
import co.globers.globersapp.AppConfig
import co.globers.globersapp.firestore.entities.*
import co.globers.globersapp.firestore.executeBatch
import co.globers.globersapp.firestore.executeGetOrNull
import co.globers.globersapp.firestore.executeUpdate
import co.globers.globersapp.firestore.listen
import co.globers.globersapp.services.peermatching.IPeerMatching.Companion.isFinished
import com.google.firebase.firestore.*
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import java.util.*


private const val LOG_TAG = "PeerMatching"


class RandomPeerMatching(
    private val userGameInfo: DbUserGameInfo
) : IPeerMatching {

    // db service
    private val db = FirebaseFirestore.getInstance()
    private val peerMatchUid = UUID.randomUUID().toString()
    private val createdMatchDocRef: DocumentReference = db.collection(DbPeerMatch.COLLECTION_NAME_RANDOM).document(peerMatchUid)

    // listeners
    private var waitingMatchesListener : ListenerRegistration? = null
    private var createdMatchListener : ListenerRegistration? = null
    private val peerMatchStateSubscription : Disposable

    private var state: IPeerMatching.State =
        IPeerMatching.State(
            IPeerMatching.Status.NotStarted,
            null
        )

    override val peerMatchingState = BehaviorSubject.createDefault<IPeerMatching.State>(
        state
    )

    init {
        peerMatchStateSubscription = peerMatchingState.subscribe {
            state = it
        }
    }


    private val waitingTimeUpdateTimerMaxNbSec = 10*60L
    private val waitingTimeUpdateTimerIntervalNbSec = 20L
    private var waitingTimeUpdateTimer: CountDownTimer = object : CountDownTimer(
        waitingTimeUpdateTimerMaxNbSec*1000,
        waitingTimeUpdateTimerIntervalNbSec*1000
    ) {

        private var nbSecElapsed: Long = 0

        override fun onFinish() {
        }

        override fun onTick(millisUntilFinished: Long) {

            nbSecElapsed = waitingTimeUpdateTimerMaxNbSec - millisUntilFinished / 1000
            // update
            MainScope().launch {
                executeUpdate(
                    createdMatchDocRef,
                    FieldPath.of(DbPeerMatch.FIELD_WAITING_TIME_IN_SEC),
                    nbSecElapsed
                )
            }
        }
    }


    override fun start(){

        Log.i(LOG_TAG, "start PeerMatching")
        if (state.status != IPeerMatching.Status.NotStarted){
            throw IPeerMatching.PeerMatchingException("Cannot start PeerMatching, status should be NotStarted but is ${state.status}")
        }

        createdMatchListener = createdMatchDocRef.addSnapshotListener { snapshot, exception ->

            if (exception != null) {
                setError(exception)
            } else if (snapshot != null){

                val peerMatch = snapshot.toObject(DbPeerMatch::class.java)
                if (peerMatch != null) {
                    onCreatedMatchUpdate(peerMatch)
                } else {
                    Log.d(LOG_TAG, "CreatedPeerMatch update but peerMatch is null")
                }
            }
            else {
                Log.d(LOG_TAG, "CreatedPeerMatch update but snapshot is null")
            }
        }

        val waitingMatchesQuery =
            buildFindWaitingMatchesQuery(
                db,
                userGameInfo.learnedLanguageIsoCode,
                userGameInfo.nbWordsKnownInGameLanguage
            )
        waitingMatchesListener = waitingMatchesQuery.addSnapshotListener { snapshot, exception ->

            if (exception != null) {
                setError(exception)
            } else if (snapshot != null && !snapshot.metadata.isFromCache && snapshot.documents.isNotEmpty()) { // not interested in snapshot from cache
                val peerMatches = snapshot.documents
                    .map { it.reference to it.toObject(DbPeerMatch::class.java)!! }
                    .toMap()
                onWaitingMatchesUpdate(peerMatches)
            }
        }

        createPeerMatch()

        updateState(
            IPeerMatching.State(
                IPeerMatching.Status.Started,
                null
            )
        )
    }

    private fun createPeerMatch(){

        db.runTransaction { transaction ->
            val snapshot = transaction.get(createdMatchDocRef)
            val currentPeerMatch = snapshot.toObject(DbPeerMatch::class.java)
            when {
                currentPeerMatch == null -> { // normal case

                    val peerMatchToCreate = DbPeerMatch(
                        peerMatchUid,
                        null,
                        userGameInfo,
                        null,
                        DbPeerMatchStatus.WAITING_FOR_PEER,
                        UUID.randomUUID().toString(),
                        null,
                        0
                    )

                    transaction.set(createdMatchDocRef, peerMatchToCreate)
                    peerMatchToCreate
                }
                currentPeerMatch.status == DbPeerMatchStatus.CANCELED -> { // when it has been cancelled on onWaitingMatchesUpdate callback even before it is properly created
                    transaction.update(createdMatchDocRef, mapOf())// Dummy update because we of firestore bug
                    null
                }
                else -> // BUG, should not happens
                    throw IPeerMatching.PeerMatchingException(
                        "PeerMatch already exists, should not happens"
                    )
            }
        }
            .addOnSuccessListener { peerMatch: DbPeerMatch? ->
                if (peerMatch != null) {
                    Log.i(LOG_TAG, "PeerMatch created")
                    waitingTimeUpdateTimer.start()
                }
                else {
                    Log.i(LOG_TAG, "PeerMatch not created, has already been created with status canceled")
                }
            }
            .addOnFailureListener {
                Log.e(LOG_TAG, "Error in createPeerMatch transaction", it)
                setError(it)
            }
    }

    private fun onCreatedMatchUpdate(peerMatch: DbPeerMatch){

        Log.d(LOG_TAG, "CreatedPeerMatch update, new status: ${peerMatch.status}")
        when(peerMatch.status){
            DbPeerMatchStatus.WAITING_FOR_PEER -> { /*Nothing new here...*/}
            DbPeerMatchStatus.JOINED -> {
                Log.i(LOG_TAG, "created peerMatch has been joined by another player")
                waitingTimeUpdateTimer.cancel()

                if (peerMatch.gameUid == null){
                    Log.d(LOG_TAG, "created peerMatch has been joined, we associate game to peerMatch")
                    MainScope().launch {
                        associateGameToPeerMatching(
                            peerMatch,
                            createdMatchDocRef,
                            db
                        )
                    }
                } else {
                    Log.d(LOG_TAG, "created peerMatch has been associated to game, we complete peerMatch")
                    MainScope().launch {

                        val tabooGame = getTabooGame(
                            db,
                            peerMatch,
                            true,
                            userGameInfo.userUid
                        )

                        val teammateContact = executeGetOrNull<DbContact>(
                            DbContact.getDocReference(db, userGameInfo.userUid, peerMatch.joinerGameInfo!!.userUid)
                        )

                        updateState(
                            IPeerMatching.State(
                                IPeerMatching.Status.Completed,
                                IPeerMatching.Result(
                                    peerMatch,
                                    true,
                                    tabooGame,
                                    teammateContact
                                )
                            )
                        )
                    }
                }

            }
            DbPeerMatchStatus.CANCELED -> { /* Has been canceled because joined another waiting */}
            DbPeerMatchStatus.FORCE_CANCELED -> { Log.d(LOG_TAG, "peerMatch ${peerMatch.uid} FORCE_CANCELED")}
        }

    }


    private fun onWaitingMatchesUpdate(waitingMatches: Map<DocumentReference, DbPeerMatch>){

        val zombiePeerMatches = waitingMatches.filter { isZombiePeerMatch(it.value) }
        val alivePeerMatches = waitingMatches.filter { !zombiePeerMatches.containsKey(it.key) }
        forceCancelPeerMatches(zombiePeerMatches)

        val peerMatchToJoinDocRef = alivePeerMatches
            .filterKeys { it.id != createdMatchDocRef.id }
            .keys.firstOrNull()

        if (peerMatchToJoinDocRef != null){
            db.runTransaction { transaction ->

                val snapshotPeerMatchToJoin = transaction.get(peerMatchToJoinDocRef)
                val peerMatchWaitingToJoin = snapshotPeerMatchToJoin.toObject(DbPeerMatch::class.java)!!

                val snapshotPeerMatchCreated = transaction.get(createdMatchDocRef)
                val peerMatchCreatedToCancel = snapshotPeerMatchCreated.toObject(DbPeerMatch::class.java) // can be null if still not created

                Log.i(LOG_TAG, "Status peerMatchWaitingToJoin: ${peerMatchWaitingToJoin.status}") // TODO NICO HERE: pourquoi le status est souvent "CANCELED" alors qu'on a pris les "WAITING" dans la query
                Log.i(LOG_TAG, "Status peerMatchCreatedToCancel: ${peerMatchCreatedToCancel?.status}")

                Log.d(LOG_TAG, "peerMatchWaitingToJoin details: $peerMatchWaitingToJoin")


                if(peerMatchWaitingToJoin.status == DbPeerMatchStatus.WAITING_FOR_PEER && (peerMatchCreatedToCancel == null || peerMatchCreatedToCancel.status == DbPeerMatchStatus.WAITING_FOR_PEER))
                {
                    //The one we one to join is still available, the created is not yet created or is still waiting => it still time to cancel it
                    cancelCreatedPeerMatchInsideTransaction(transaction, peerMatchCreatedToCancel)

                    transaction.update(
                        peerMatchToJoinDocRef,
                        DbPeerMatch.FIELD_JOINER,
                        userGameInfo,
                        DbPeerMatch.FIELD_STATUS,
                        DbPeerMatchStatus.JOINED
                    )
                    val result: DbPeerMatch? = peerMatchWaitingToJoin.copy(joinerGameInfo = userGameInfo, status = DbPeerMatchStatus.JOINED)

                    Log.d(LOG_TAG, "Try to join peerMatchWaitingToJoin")

                    result
                }
                else {
                    // https://groups.google.com/forum/#!topic/google-cloud-firestore-discuss/LfD_YEnGVu4
                    // workaround for error: "FirebaseFirestoreException: Every document read in a transaction must also be written."
                    transaction.update(peerMatchToJoinDocRef, mapOf())
                    if (peerMatchCreatedToCancel != null) {
                        transaction.update(createdMatchDocRef, mapOf())
                    }

                    val result: DbPeerMatch? = null
                    result
                }
                // else: race condition, either
                //    - created has been joined => will be managed in onCreatedMatchUpdate
                //    - waiting has been joined or canceled => we will get another one on next call to onWaitingMatchesUpdate
            }
                .addOnSuccessListener { joinedPeerMatch: DbPeerMatch? ->
                    if (joinedPeerMatch != null) {
                        Log.i(LOG_TAG, "joined another waiting peerMatch")
                        // we know wait for gameId to be set

                        listen<DbPeerMatch>(peerMatchToJoinDocRef).filter { it.gameUid != null }.take(1).subscribe { peerMatch ->

                            MainScope().launch {
                                val tabooGame =
                                    getTabooGame(
                                        db,
                                        peerMatch,
                                        false,
                                        userGameInfo.userUid
                                    )

                                val teammateContact = executeGetOrNull<DbContact>(
                                    DbContact.getDocReference(db, userGameInfo.userUid, peerMatch.creatorGameInfo.userUid)
                                )

                                updateState(
                                    IPeerMatching.State(
                                        IPeerMatching.Status.Completed,
                                        IPeerMatching.Result(
                                            peerMatch,
                                            false,
                                            tabooGame,
                                            teammateContact
                                        )
                                    )
                                )
                            }
                        }

                    } // else, we got a race condition, we just wait for the next potential peer to join
                    else {
                        Log.i(LOG_TAG, "waiting peerMatch could not be joined (race condition)")
                    }

                }
                .addOnFailureListener { exception ->
                    Log.e(LOG_TAG, "Error in joinPeerMatch transaction", exception)
                    setError(exception) }
        }
        else{
            Log.i(LOG_TAG, "No waiting peerMatch found")
        }
    }

    private fun forceCancelPeerMatches(peerMatches: Map<DocumentReference, DbPeerMatch>) {

        MainScope().launch {
            executeBatch { batch ->
                peerMatches.forEach { pair ->
                    batch.update(
                        pair.key,
                        DbPeerMatch.FIELD_STATUS,
                        DbPeerMatchStatus.FORCE_CANCELED
                    )
                }
            }
        }
    }

    private fun isZombiePeerMatch(peerMatch: DbPeerMatch): Boolean {

        val now = Date()

        val creationDate = peerMatch.creationDate
        val waitingTimeInSec = peerMatch.waitingTimeInSec

        val lastUpdateDateMs = creationDate.time + waitingTimeInSec * 1000
        val nowMinusBufferUpdateInterval = now.time - waitingTimeUpdateTimerIntervalNbSec*3*1000

        return lastUpdateDateMs < nowMinusBufferUpdateInterval
    }


    override fun abort(){
        Log.i(LOG_TAG, "Abort PeerMatching")

        if (!isFinished(state.status)){
            if (state.status != IPeerMatching.Status.NotStarted){
                cancelCreatedPeerMatch()
            }
            updateState(
                IPeerMatching.State(
                    IPeerMatching.Status.Aborted,
                    null
                )
            )
        }
    }


    private fun updateState(newState: IPeerMatching.State){
        peerMatchingState.onNext(newState)
        if (isFinished(newState.status)) // isFinished checked against new Status
        {
            waitingTimeUpdateTimer.cancel()
            stopSubscriptions()
            peerMatchingState.onComplete()
        }

    }

    private fun setError(e: Exception){
        Log.e(LOG_TAG, "Error in PeerMatching", e)
        cancelCreatedPeerMatch()
        updateState(
            IPeerMatching.State(
                IPeerMatching.Status.Error,
                null
            )
        )
    }


    private fun stopSubscriptions(){
        // should ot be called if start has not been called
        if (state.status != IPeerMatching.Status.NotStarted){
            waitingMatchesListener!!.remove()
            createdMatchListener!!.remove()
        }
        peerMatchStateSubscription.dispose()

        Log.i(LOG_TAG, "Subscriptions inside PeerMatching has been stopped")
    }

    private fun cancelCreatedPeerMatch(){

        Log.d(LOG_TAG, "Cancel Created PeerMatch")

        waitingTimeUpdateTimer.cancel()

        db.runTransaction { transaction ->
            val snapshotPeerMatchCreated = transaction.get(createdMatchDocRef)
            val peerMatchCreatedToCancel = snapshotPeerMatchCreated.toObject(DbPeerMatch::class.java) // can be null if still not created
            cancelCreatedPeerMatchInsideTransaction(transaction, peerMatchCreatedToCancel)
        }
            .addOnSuccessListener {
                Log.i(LOG_TAG, "Created PeerMatch has been canceled")



            }
            .addOnFailureListener { e -> Log.e(LOG_TAG, "Error in cancelling PeerMatch", e) }
    }


    private fun cancelCreatedPeerMatchInsideTransaction(transaction: Transaction, peerMatchCreatedToCancel: DbPeerMatch?){

        Log.d(LOG_TAG, "Cancel Created PeerMatch inside transaction")

        if(peerMatchCreatedToCancel != null){
            transaction.update(createdMatchDocRef, DbPeerMatch.FIELD_STATUS, DbPeerMatchStatus.CANCELED)
        }
        else {

            val peerMatchToCreate = DbPeerMatch(
                peerMatchUid,
                null,
                userGameInfo,
                null,
                DbPeerMatchStatus.CANCELED,
                "",
                null,
                -1
            )

            transaction.set(createdMatchDocRef, peerMatchToCreate)
        }
    }

}




private fun buildFindWaitingMatchesQuery(
    db: FirebaseFirestore,
    learnedLanguageIsoCode: DbLanguageIsoCode,
    estimatedNbWordsKnown: Int
): Query {


    val minNbWords = estimatedNbWordsKnown / AppConfig.COEFF_PEER_MATCHING_NB_WORD_KNOWN_SPREAD // TODO change this for more precise level matching
    val maxNbWords = estimatedNbWordsKnown * AppConfig.COEFF_PEER_MATCHING_NB_WORD_KNOWN_SPREAD



    Log.i(LOG_TAG, "Query Waiting PeerMatchs in $learnedLanguageIsoCode in [$minNbWords, $maxNbWords] known words")

    val query = db.collection(DbPeerMatch.COLLECTION_NAME_RANDOM)
        .whereEqualTo(FieldPath.of(DbPeerMatch.FIELD_CREATOR, DbUserGameInfo.FIELD_LEARNED_LANGUAGE_ISO_CODE), learnedLanguageIsoCode)
        .whereEqualTo(DbPeerMatch.FIELD_STATUS, DbPeerMatchStatus.WAITING_FOR_PEER)
        .limit(2) /* Need two because it might contains the one created by user (we cannot do whereNotEqualsTo)*/
    return query
}





