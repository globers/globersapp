package co.globers.globersapp.firestore.entities

import com.google.firebase.firestore.ServerTimestamp
import java.util.*

data class DbUserAward(
    val uid: String,
    val giverUserUid: String,
    val receiverUserUid: String,
    val peerMatchUid: String,
    val userAwardType: DbUserAwardType,
    @ServerTimestamp
    val creationDate: Date = Date()
){
    companion object {
        const val COLLECTION_NAME = "userAwards"
    }

    constructor(): this("", "", "", "", DbUserAwardType.AwesomeAccent)

}

enum class DbUserAwardType {
    AwesomeAccent,
    AwesomeVocabulary,
    AwesomeGrammar,
    SuperFriendly,
    SuperNice,
    SuperCreative,
    SuperMotivated,
    SuperFunny,
    SuperTalkative,
    SuperWeirdInAGoodWay,
    SuperHelpful,
    SuperSmart,
    SuperPatient,
    SuperEfficient,
    AwesomeSinger,
    SuperRelaxed,
    SuperEnergetic,
    FightingSpirit,
    BestTeammateEver
}

fun toDisplayMessage(userAwardType: DbUserAwardType) = when (userAwardType){
    DbUserAwardType.AwesomeAccent -> "Awesome accent"
    DbUserAwardType.AwesomeVocabulary -> "Awesome vocabulary"
    DbUserAwardType.AwesomeGrammar -> "Awesome grammar"
    DbUserAwardType.SuperFriendly -> "Super friendly"
    DbUserAwardType.SuperNice -> "Super nice"
    DbUserAwardType.SuperCreative -> "Super creative"
    DbUserAwardType.SuperMotivated -> "Super motivated"
    DbUserAwardType.SuperFunny -> "Super funny"
    DbUserAwardType.SuperWeirdInAGoodWay -> "Super weird (in a good way!)"
    DbUserAwardType.SuperHelpful -> "Super helpful"
    DbUserAwardType.SuperSmart -> "Super smart"
    DbUserAwardType.SuperPatient -> "Super patient"
    DbUserAwardType.SuperEfficient -> "Super efficient"
    DbUserAwardType.AwesomeSinger -> "Awesome singer"
    DbUserAwardType.SuperRelaxed -> "Super relaxed"
    DbUserAwardType.SuperEnergetic -> "Super energetic"
    DbUserAwardType.BestTeammateEver -> "Best teammate ever"
    DbUserAwardType.SuperTalkative -> "Super talkative"
    DbUserAwardType.FightingSpirit -> "Fighting spirit"
}