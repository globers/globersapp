package co.globers.globersapp.ui.phonecall

import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import co.globers.globersapp.R
import co.globers.globersapp.utils.allowOnLockedScreen
import kotlinx.android.parcel.Parcelize


class PhoneCallActivity : AppCompatActivity() {

    companion object{
        const val EXTRA_ARGS = "PhoneCallActivityExtraArgs"
        const val LOG_TAG = "PhoneCallAct"

    }

    @Parcelize
    data class Args(
        val notificationId: Int,
        val callerUserUid: String,
        val callRequestsUid: String,
        val peerMatchUid: String,
        val callerUserName: String
    ): Parcelable


    //private var wl: PowerManager.WakeLock? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.d(LOG_TAG, "onCreate")
        allowOnLockedScreen(this)

        setContentView(R.layout.activity_phone_call)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, PhoneCallFragment.newInstance())
                .commitNow()
        }

        val args = intent.getParcelableExtra<Args>(EXTRA_ARGS)!!

        val viewModel = ViewModelProvider(this).get(PhoneCallViewModel::class.java)
        viewModel.init(args)
    }


}

