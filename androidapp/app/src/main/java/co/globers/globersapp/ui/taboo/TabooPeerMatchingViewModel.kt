package co.globers.globersapp.ui.taboo

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.globers.globersapp.firestore.entities.DbUserGameInfo
import co.globers.globersapp.firestore.entities.DbUserPublicProfile
import co.globers.globersapp.firestore.executeGet
import co.globers.globersapp.services.*
import co.globers.globersapp.services.peermatching.FixedPeerMatching
import co.globers.globersapp.services.peermatching.IPeerMatching
import co.globers.globersapp.services.peermatching.RandomPeerMatching
import co.globers.globersapp.utils.NonNullLiveData
import com.google.firebase.firestore.FirebaseFirestore
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.launch
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class TabooPeerMatchingViewModel : ViewModel() {

    companion object {
        private const val LOG_TAG = "TabooPeerMatchingVM"
    }

    private var peerMatchingStateSubscription: Disposable? = null
    private var peerMatching: IPeerMatching? = null
    private lateinit var userUid: String
    private val db: FirebaseFirestore = FirebaseFirestore.getInstance()

    enum class Status {
        NotStarted,
        Running,
        Joined,
        Completed,
        Failed
    }

    data class State(val status: Status, val peerMatchingResult: IPeerMatching.Result?)
    val state = NonNullLiveData(State(Status.NotStarted, null))

    val userGameInfoLiveData = MutableLiveData<DbUserGameInfo?>(null)

    fun init(tabooActivityArgs: TabooActivityArgs, userUid: String, userCountryIsoCode: String?){

        this.userUid = userUid

        viewModelScope.launch {

            val userGameInfo = buildUserGameInfo(userUid, tabooActivityArgs, userCountryIsoCode)
            userGameInfoLiveData.value = userGameInfo

            val peerMatching = if (tabooActivityArgs.teammateArgs == null) {
                RandomPeerMatching(userGameInfo)
            } else {
                FixedPeerMatching(
                    userGameInfo,
                    tabooActivityArgs.teammateArgs
                )
            }

            peerMatchingStateSubscription = peerMatching.peerMatchingState.subscribe {
                onPeerMatchingStateChanged(it)
            }

            peerMatching.start()

            this@TabooPeerMatchingViewModel.peerMatching = peerMatching
        }

    }


    private suspend fun buildUserGameInfo(
        userUid: String,
        tabooActivityArgs: TabooActivityArgs,
        userCountryIsoCode: String?): DbUserGameInfo {

        val user = executeGet<DbUserPublicProfile>(DbUserPublicProfile.buildDocRef(db, userUid))

        val programService = TabooProgramService()
        val tabooProgram = getTabooProgram(programService)

        val lessonData = if (tabooActivityArgs.lessonArgs != null)
            getLessonData(tabooProgram, tabooActivityArgs.lessonArgs) else
            getDefaultLessonData(tabooProgram)

        val lessonUserState = getOrCreateUserLesson(
            userUid,
            lessonData.program.userProgramUid,
            lessonData.lesson.userLessonUid,
            lessonData.program.programUid,
            lessonData.lesson.lessonUid,
            db
        )

        return DbUserGameInfo(
            user.userUid,
            user.displayName,
            user.userPhotoUrl,
            userCountryIsoCode ?: user.nativeLanguageIsoCode,
            user.nativeLanguageIsoCode,
            lessonData.program.learnedLanguage.languageIsoCode,
            lessonData.program.learnedLanguage.level.estimatedNbWordsKnown,
            lessonData.program.userProgramUid,
            lessonData.level.levelUid,
            lessonData.program.programUid,
            lessonData.program.label,
            lessonData.level.label,
            lessonData.lesson.label,
            lessonUserState
        )

    }

    data class LessonData(
        val program: TabooProgram,
        val level: TabooLevel,
        val lesson: TabooLesson
    )

    private suspend fun getTabooProgram(programService: TabooProgramService): TabooProgram{

        return suspendCoroutine { cont: Continuation<TabooProgram> ->
            programService.getTabooProgramObservable().take(1).subscribe { program ->
                cont.resume(program)
            }
        }
    }

    private fun getDefaultLessonData(program: TabooProgram): LessonData {

        val level = program.levels.first { level -> level.levelStatus == LevelStatus.CURRENT }
        val lesson = level.lessons.first { lesson -> lesson.status == LessonStatus.CURRENT }

        return LessonData(program, level, lesson)
    }

    private fun getLessonData(program: TabooProgram, lessonArgs: TabooActivityLessonArgs): LessonData{

        val level = program.levels.first { it.levelUid == lessonArgs.levelUid }
        val lesson = level.lessons.first { it.userLessonUid == lessonArgs.userLessonUid }
        return LessonData(program, level, lesson)
    }



    private fun onPeerMatchingStateChanged(peerMatchingState: IPeerMatching.State){


        Log.d(LOG_TAG, "onPeerMatchingStateChanged: $peerMatchingState")

        when(peerMatchingState.status){
            IPeerMatching.Status.NotStarted -> { } // nothing to do
            IPeerMatching.Status.Started -> { state.value = State(Status.Running, null)} // nothing to do
            IPeerMatching.Status.Completed -> {
                state.value = State(Status.Completed, peerMatchingState.result!!)
            }
            IPeerMatching.Status.Error -> state.value = State(Status.Failed, null)
            IPeerMatching.Status.Aborted -> state.value = State(Status.Failed, null)
        }

    }

    override fun onCleared() {
        super.onCleared()
        peerMatchingStateSubscription?.dispose()
    }

    fun abortPeerMatch(){
        peerMatching?.abort()
    }


}


