package co.globers.globersapp.ui.main

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import co.globers.globersapp.R
import co.globers.globersapp.ui.commoncomponents.ProgressFragment
import co.globers.globersapp.ui.taboo.TabooActivity
import co.globers.globersapp.ui.taboo.TabooActivityArgs
import co.globers.globersapp.ui.taboo.TabooActivityTeammateArgs
import co.globers.globersapp.utils.Screen
import co.globers.globersapp.utils.Tracking
import kotlinx.android.synthetic.main.activity_main.*

class MainCallRequestFragment : ProgressFragment("Call in process...", "Cancel Call") {

    companion object {
        fun newInstance() = MainCallRequestFragment()
    }

    private lateinit var viewModel: MainCallRequestViewModel
    private lateinit var soundPhoneMediaPlayer: MediaPlayer
    private lateinit var tracking: Tracking


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tracking = Tracking(this, Screen.MainCallRequest)
        viewModel = ViewModelProvider(this).get(MainCallRequestViewModel::class.java)

        val args = MainCallRequestFragmentArgs.fromBundle(requireArguments())
        val teammateUserUid = args.contactUid
        val teammateName = args.contactName

        viewModel.startCall(teammateUserUid)


        val bottomNavView = requireActivity().main_bottom_navigation_view
        bottomNavView.visibility = View.GONE

        viewModel.callStatus.observe(viewLifecycleOwner, Observer {status: MainCallRequestViewModel.CallStatus ->

            when (status) {
                MainCallRequestViewModel.CallStatus.NotStarted -> {}
                MainCallRequestViewModel.CallStatus.Ongoing -> {}
                MainCallRequestViewModel.CallStatus.Failed -> displayMessageAndQuit()
                MainCallRequestViewModel.CallStatus.CanceledByCaller -> quit()
                MainCallRequestViewModel.CallStatus.Accepted -> startTabooActivity(teammateUserUid, viewModel.peerMatchUid)
            }
        })

        soundPhoneMediaPlayer = MediaPlayer.create(context, R.raw.sound_ringtone)

    }

    private fun startTabooActivity(teammateUserUid: String, peerMatchUid: String) {

        quit()

        val intent = Intent(context, TabooActivity::class.java)
        val tabooArgs = TabooActivityArgs(
            null,
            TabooActivityTeammateArgs(teammateUserUid, peerMatchUid, true),
            false
        )
        intent.putExtra(TabooActivity.EXTRA_TABOO_ACTIVITY_ARGS, tabooArgs)
        startActivity(intent)

    }

    private fun quit() {
        requireActivity().onBackPressed()
    }

    override fun onCancelButtonClick() {
        viewModel.cancelCall()
    }

    private fun displayMessageAndQuit(){

        soundPhoneMediaPlayer.stop()
        AlertDialog.Builder(requireActivity())
            .setTitle("Call failed")
            .setNeutralButton("Ok") {  _, _ ->
                quit()
            }
            .show()
    }

    override fun onResume() {
        super.onResume()
        soundPhoneMediaPlayer.start()
    }

    override fun onPause() {
        soundPhoneMediaPlayer.stop()
        if (viewModel.callStatus.value == MainCallRequestViewModel.CallStatus.Ongoing){
            viewModel.cancelCall()
        }
        super.onPause()
    }

    override fun onStop() {
        super.onStop()
        soundPhoneMediaPlayer.release()
    }

}
