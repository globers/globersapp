package co.globers.globersapp.ui.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import co.globers.globersapp.AppConfig
import co.globers.globersapp.AppParams
import co.globers.globersapp.R
import co.globers.globersapp.utils.Screen
import co.globers.globersapp.utils.Tracking
import com.uxcam.UXCam
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.launch

class MainSplashScreenFragment : Fragment() {

    private lateinit var viewModel: MainSplashScreenViewModel
    private lateinit var tracking: Tracking

    companion object {
        fun newInstance() = MainSplashScreenFragment()
        private const val LOG_TAG = "MainSplashScreenFrag"
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_splash_screen_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tracking = Tracking(this, Screen.MainSplashScreen)
        viewModel = ViewModelProvider(this).get(MainSplashScreenViewModel::class.java)

        val bottomNavView = requireActivity().main_bottom_navigation_view
        bottomNavView.visibility = View.GONE

        lifecycleScope.launch {

            AppParams.init()

            if (AppParams.UxCamActivated.get()){
                // key in https://app.uxcam.com
                Log.i(LOG_TAG, "activate uxcam")
                UXCam.startWithKey(AppConfig.UXCAM_API_KEY)
            }


            viewModel.status.observe(
                viewLifecycleOwner,
                Observer { status: MainSplashScreenViewModel.Status ->

                    when (status) {
                        MainSplashScreenViewModel.Status.AccountNotFound -> {

                            AlertDialog.Builder(requireActivity())
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle("User not found")
                                .setCancelable(false)
                                .setMessage("Please login or create an account")
                                .setNeutralButton("Ok") { _, _ ->
                                    // userId loggedIn but user is not found..., for now we do as if it didn't exists
                                    val action =
                                        MainSplashScreenFragmentDirections.actionMainSplashScreenFragmentToMainGetStartedFragment()
                                    findNavController().navigate(action)
                                }
                                .show()
                        }
                        MainSplashScreenViewModel.Status.UserLoaded -> {
                            val action =
                                MainSplashScreenFragmentDirections.actionMainSplashScreenFragmentToMainProgramFragment()
                            findNavController().navigate(action)
                        }
                        MainSplashScreenViewModel.Status.SplashScreen -> {
                        }
                        MainSplashScreenViewModel.Status.UserNotLoggedIn -> {
                            val action =
                                MainSplashScreenFragmentDirections.actionMainSplashScreenFragmentToMainGetStartedFragment()
                            findNavController().navigate(action)
                        }
                    }
                }
            )



        }


    }

}

