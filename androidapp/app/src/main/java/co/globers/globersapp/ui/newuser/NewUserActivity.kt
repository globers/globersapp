package co.globers.globersapp.ui.newuser

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import co.globers.globersapp.R
import co.globers.globersapp.utils.InternetConnectionManager


class NewUserActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_user)

        InternetConnectionManager(this, false)

        if(savedInstanceState !=null){
            // it means it has been "killed" by android (waited too long or memory too low), we kill to reload program
            finish()
        }
    }



    override fun onBackPressed() {

        val activityViewModel = ViewModelProvider(this).get(NewUserViewModel::class.java)

        if (activityViewModel.currentUserCreationStep.value in (NewUserViewModel.UserCreationStep.LanguageTest..NewUserViewModel.UserCreationStep.Finished)) {

            AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Quit test?")
                .setMessage("Are you sure you want to quit? It will cancel the whole test")
                .setPositiveButton("Yes") { _, _ -> finish() }
                .setNegativeButton("No", null)
                .show()
        }
        else {
            super.onBackPressed()
        }
    }
}
