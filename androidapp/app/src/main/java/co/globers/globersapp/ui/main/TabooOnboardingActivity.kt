package co.globers.globersapp.ui.main

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.core.content.ContextCompat
import com.github.paolorotolo.appintro.AppIntro
import com.github.paolorotolo.appintro.AppIntroFragment
import androidx.fragment.app.Fragment
import co.globers.globersapp.R
import com.github.paolorotolo.appintro.model.SliderPage



private fun makeSlide(title: String, description: String, image: Int, context: Context): AppIntroFragment {

    val sliderPage = SliderPage()
    sliderPage.title = title
    sliderPage.description = description
    sliderPage.imageDrawable = image
    sliderPage.bgColor = Color.WHITE
    sliderPage.titleColor = ContextCompat.getColor(context, R.color.myColorPrimary)
    sliderPage.descColor = Color.BLACK
    return AppIntroFragment.newInstance(sliderPage)

}


class TabooOnboardingActivity : AppIntro() {

    companion object {
        const val RESULT_OK = 111
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        val slide1 = makeSlide(
            "Another player is on the phone",
            "Find a calm environment and put your headphones on. We will connect you to another player through a voice call",
            R.drawable.instructions_headphone,
            this
        )

        val slide2 = makeSlide(
            "Let's play a game together",
            "You will play a game called \"Taboo\", switching roles between \"Talker\" and \"Listener\"",
            R.drawable.image_tabooinstructions_roles_picapp_removebg_filesmerge,
            this
        )

        val slide3 = makeSlide(
            "The rules of the game: Talker",
            "As Talker, make the Listener find the word in green. It's forbidden to use sound effects and the words in red (or any part of them)",
            R.drawable.image_tabooinstructions_talker_picapp_removebg,
            this
        )

        val slide4 = makeSlide(
            "The rules of the game: Listener",
            "As Listener, listen carefully, ask questions, and find the word the other player is talking about",
            R.drawable.image_tabooinstructions_listener_picapp_removebg,
            this
        )

        addSlide(slide1)
        addSlide(slide2)
        addSlide(slide3)
        addSlide(slide4)
        setDoneText("OK")
        showSkipButton(false)
        setBarColor(ContextCompat.getColor(this, R.color.myColorPrimary))

    }

    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        setResult(RESULT_OK)
        finish()
    }
}