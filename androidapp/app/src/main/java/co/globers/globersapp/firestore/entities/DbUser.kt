package co.globers.globersapp.firestore.entities

import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ServerTimestamp
import java.util.*



data class DbUserPublicProfile(
    val userUid: String, // uid of the entity
    val displayName: String, // first name
    val username: String, // unique (readable / modifiable) identifier, @ + first name + random number
    val userPhotoUrl: String?,
    val nativeLanguageIsoCode: String,
    @ServerTimestamp
    val creationDate: Date = Date()
){
    constructor(): this( "", "", "", null, "")

    companion object {
        const val COLLECTION_NAME = "userPublicProfiles"
        const val FIELD_USERNAME = "username"

        fun buildDocRef(db: FirebaseFirestore, userUid: String) = db.collection(COLLECTION_NAME).document(userUid)
    }

}


data class DbUser (
    val uid: String,
    val learnedLanguages: List<DbLearnedLanguage>,
    val userState: DbUserState,
    @ServerTimestamp
    val creationDate: Date = Date()
){

    constructor(): this("", listOf(), DbUserState())

    companion object {
        const val COLLECTION_NAME = "users"
        const val FIELD_USER_STATE = "userState"
    }

}


data class DbUserState(
    val tabooInstructionsAlreadyDisplayed: Boolean
){
    constructor(): this(false)


    companion object {
        const val FIELD_TABOO_INSTRUCTIONS_ALREADY_DISPLAYED = "tabooInstructionsAlreadyDisplayed"
    }

}


data class DbLanguageLevel(
    val logitLogFreqInverseParams: DbLogitParams,
    val estimatedNbWordsKnown: Int
){
    constructor(): this(DbLogitParams(), -1)
}

data class DbLogitParams(
    val intercept: Double,
    val coeff: Double
){
    constructor(): this(0.0, 0.0)
}

data class DbLearnedLanguage(val languageIsoCode: DbLanguageIsoCode, val level: DbLanguageLevel, val userProgramUid: String){
    constructor() : this(DbLanguageIsoCode.ENG, DbLanguageLevel(), "")

    companion object {
        const val FIELD_LANGUAGE_ISO_CODE = "languageIsoCode"
    }

}

enum class DbLanguageIsoCode {
    ENG,
    FRA
}
