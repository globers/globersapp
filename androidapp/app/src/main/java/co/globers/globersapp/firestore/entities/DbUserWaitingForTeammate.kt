package co.globers.globersapp.firestore.entities

import com.google.firebase.firestore.ServerTimestamp
import java.util.*

enum class DbWaitingUserStatus{
    WAITING,
    NOT_WAITING
}

data class DbWaitingUser(
    val userUid: String, // uid is userUid
    val status: DbWaitingUserStatus,
    @ServerTimestamp
    val creationDate: Date = Date()
){

    constructor(): this("", DbWaitingUserStatus.WAITING)

    companion object {
        const val COLLECTION_NAME = "waitingUsers"
    }

}