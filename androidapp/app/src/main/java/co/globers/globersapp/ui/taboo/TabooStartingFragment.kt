package co.globers.globersapp.ui.taboo

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import co.globers.globersapp.R
import co.globers.globersapp.notifservice.WaitingUserService
import co.globers.globersapp.utils.Screen
import co.globers.globersapp.utils.Tracking
import co.globers.globersapp.utils.localeToEmoji
import com.firebase.ui.auth.data.model.CountryInfo.localeToEmoji
import kotlinx.android.synthetic.main.taboo_starting_fragment.*
import java.util.*


class TabooStartingFragment : Fragment() {

    companion object {
        fun newInstance() = TabooStartingFragment()
    }


    private lateinit var tracking: Tracking
    private lateinit var activityViewModel: TabooViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.taboo_starting_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        tracking = Tracking(this, Screen.TabooStarting)
        activityViewModel = ViewModelProvider(requireActivity()).get(TabooViewModel::class.java)

        val peerMatchResult = activityViewModel.peerMatchResult!!
        val peerMatch = peerMatchResult.peerMatch

        val isUserCreator = peerMatchResult.isCreator

        val userInfo = if (isUserCreator) peerMatch.creatorGameInfo else peerMatch.joinerGameInfo!!
        val otherPlayerInfo = if (isUserCreator) peerMatch.joinerGameInfo!! else peerMatch.creatorGameInfo

        textView_tabooStarting_userName.text = userInfo.displayName
        textView_tabooStarting_userLesson.text = userInfo.lessonLabel
        textView_tabooStarting_userFlag.text = localeToEmoji(userInfo.userCountryIsoCode)

        textView_tabooStarting_otherPlayerName.text = otherPlayerInfo.displayName
        textView_tabooStarting_otherPlayerLesson.text = otherPlayerInfo.lessonLabel
        textView_tabooStarting_otherPlayerFlag.text = localeToEmoji(otherPlayerInfo.userCountryIsoCode)

        activityViewModel.state.observe(viewLifecycleOwner, Observer {
            if (it.status == TabooViewModel.Status.GameReady){

                button_tabooStarting_imReady.setOnClickListener {
                    activityViewModel.setUserReady()
                    button_tabooStarting_imReady.isEnabled = false
                }

            } else if (
                it.status == TabooViewModel.Status.RunningTalker ||
                it.status == TabooViewModel.Status.RunningGuesser
            ){
                val action = TabooStartingFragmentDirections.actionTabooStartingFragmentToTabooStartedFragment()
                findNavController().navigate(action)
            }

        })

        button_tabooStarting_rulesTalker.setOnClickListener {
            buildTalkerRulesSnackbar(requireView()).show()
        }

        button_tabooStarting_rulesListener.setOnClickListener {
            buildListenerRulesSnackbar(requireView()).show()
        }

        manageGameInterrupted(activityViewModel, this)

        // kill Waiting user service when a game has been joined
        val stopIntent = Intent(context, WaitingUserService::class.java)
        stopIntent.putExtra(WaitingUserService.EXTRA_ARGS, WaitingUserService.Args(WaitingUserService.ArgsType.STOP))
        requireActivity().startService(stopIntent)

    }



}






