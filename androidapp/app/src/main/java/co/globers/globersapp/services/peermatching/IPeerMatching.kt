package co.globers.globersapp.services.peermatching

import co.globers.globersapp.firestore.entities.*
import co.globers.globersapp.firestore.entities.tokeepinsync.DbTabooLesson
import co.globers.globersapp.firestore.entities.tokeepinsync.DbTabooProgram
import co.globers.globersapp.firestore.executeGet
import co.globers.globersapp.firestore.executeSet
import co.globers.globersapp.firestore.executeUpdate
import co.globers.globersapp.services.TabooGame
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FieldPath
import com.google.firebase.firestore.FirebaseFirestore
import io.reactivex.subjects.BehaviorSubject
import java.util.*

interface IPeerMatching {
    val peerMatchingState: BehaviorSubject<State>

    // internal classes
    class PeerMatchingException(message: String) : java.lang.Exception(message)

    enum class Status { NotStarted, Started, Completed, Error, Aborted}
    data class State(val status: Status, val result: Result?)
    data class Result(
        val peerMatch: DbPeerMatch,
        val isCreator: Boolean,
        val tabooGame: TabooGame,
        val teammateContact: DbContact?)

    fun start()
    fun abort()

    companion object {
        fun isFinished(status: Status): Boolean {
            return status == Status.Completed || status == Status.Error || status == Status.Aborted
        }
    }
}

suspend fun associateGameToPeerMatching(peerMatch: DbPeerMatch, peerMatchRef: DocumentReference, db: FirebaseFirestore) : String {
    val game = buildTabooGameNotSaved(peerMatch)
    executeSet(db.collection(DbTabooGame.COLLECTION_NAME).document(game.uid), game)
    executeUpdate(peerMatchRef, FieldPath.of(DbPeerMatch.FIELD_GAME_UID), game.uid)
    return game.uid
}

fun buildTabooGameNotSaved(peerMatch: DbPeerMatch): DbTabooGame {

    val gameState = DbTabooGameState(null, null, listOf(), DbTabooGameStatus.READY)
    return DbTabooGame(UUID.randomUUID().toString(), peerMatch.creatorGameInfo.userUid, peerMatch.joinerGameInfo!!.userUid, peerMatch.uid, gameState)
}


suspend fun getLesson(programUid: String, lessonUid: String, db: FirebaseFirestore): DbTabooLesson {
    val programsCol = db.collection(DbTabooProgram.COLLECTION_NAME)
    val programDocRef = programsCol.document(programUid)
    val lessonsCol = programDocRef.collection(DbTabooLesson.COLLECTION_NAME)
    val lessonDocRef = lessonsCol.document(lessonUid)
    return executeGet(lessonDocRef)
}


suspend fun getTabooGame(db: FirebaseFirestore, peerMatch: DbPeerMatch, isCreator: Boolean, userUid: String) : TabooGame {
    val creatorLesson =
        getLesson(
            peerMatch.creatorGameInfo.programUid,
            peerMatch.creatorGameInfo.tabooLessonUserState.lessonUid,
            db
        )
    val joinerLesson =
        getLesson(
            peerMatch.joinerGameInfo!!.programUid,
            peerMatch.joinerGameInfo.tabooLessonUserState.lessonUid,
            db
        )

    return TabooGame(
        peerMatch.gameUid!!,
        isCreator,
        userUid,
        peerMatch,
        creatorLesson,
        joinerLesson
    )
}