package co.globers.globersapp.ui.newuser

import android.graphics.Color
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController

import co.globers.globersapp.R
import co.globers.globersapp.utils.Event
import co.globers.globersapp.utils.Screen
import co.globers.globersapp.utils.Tracking
import kotlinx.android.synthetic.main.newuser_leveltest_fragment.*

class NewUserLevelTestFragment : Fragment() {

    companion object {
        fun newInstance() = NewUserLevelTestFragment()
    }

    private lateinit var activityViewModel: NewUserViewModel
    private lateinit var fragmentViewModel: NewUserLevelTestViewModel

    private lateinit var newWordsButton: Button
    private lateinit var wordsButtons: Array<Button>
    private lateinit var tracking: Tracking

    private var secondaryColor: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.newuser_leveltest_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tracking = Tracking(this, Screen.NewUserLevelTest)

        activityViewModel = ViewModelProvider(requireActivity()).get(NewUserViewModel::class.java)
        fragmentViewModel = ViewModelProvider(this).get(NewUserLevelTestViewModel::class.java)
        secondaryColor = ContextCompat.getColor(requireActivity(), R.color.myColorSecondary)

        newWordsButton = button_newsUser_nextWords

        val view = requireView()
        wordsButtons = arrayOf(
            view.findViewById(R.id.button_leveltest_word_1),
            view.findViewById(R.id.button_leveltest_word_2),
            view.findViewById(R.id.button_leveltest_word_3),
            view.findViewById(R.id.button_leveltest_word_4),
            view.findViewById(R.id.button_leveltest_word_5),
            view.findViewById(R.id.button_leveltest_word_6)
        )

        val progressBar = progressBar_newsUserLevelTest_progress

        activityViewModel.currentUserCreationStep.observe(viewLifecycleOwner, Observer {
            if (activityViewModel.currentUserCreationStep.value == NewUserViewModel.UserCreationStep.LanguageTestResult){

                val result = activityViewModel.testResult!!
                val startLevel = activityViewModel.program!!.levels.first { it.uid == result.startLevelUid }

                val action = NewUserLevelTestFragmentDirections.actionNewUserLevelTestFragmentToNewUserLevelTestResultFragment(
                    startLevel.label, result.estimatedNbWordsKnown
                )
                findNavController().navigate(action)
            }
        })

        fragmentViewModel.init(activityViewModel.dictionary!!)

        fragmentViewModel.state.observe(viewLifecycleOwner, Observer {state ->

            when(state.step){
                NewUserLevelTestViewModel.Step.STARTING -> {}
                NewUserLevelTestViewModel.Step.TEST -> {

                    val words = state.guessedWordsByTestStep[state.currentTestStepIndex].toList()
                    wordsButtons.zip(words).forEach {
                        it.first.text = it.second.first
                        if (it.second.second){
                            it.first.setBackgroundColor(secondaryColor)
                            it.first.setTextColor(Color.WHITE)
                        } else {
                            it.first.setBackgroundColor(Color.WHITE)
                            it.first.setTextColor(secondaryColor)
                        }

                            /*
                        it.first.setBackgroundColor(
                            if(it.second.second)
                                ContextCompat.getColor(requireActivity(), R.color.colorPrimary)
                            else
                                Color.WHITE)*/


                    }

                    progressBar.progress = (5 + 0.95 * state.percentTestCompletion).toInt() // display 10% when test start


                }
                NewUserLevelTestViewModel.Step.FINISHED -> {

                    val logitResult = state.finalLevelLogitParams!!
                    val estimatedNbWordsKnown = state.finalEstimatedNbWordsKnown!!

                    activityViewModel.setTestResult(logitResult, estimatedNbWordsKnown)

                }
            }
        })

        wordsButtons.forEach { button -> button.setOnClickListener { fragmentViewModel.onSwitch(button.text.toString()) } }
        newWordsButton.setOnClickListener {

            fragmentViewModel.state.value.let {
                tracking.event(Event.NewUserLevelTestStepCompleted(it.currentTestStepIndex))
            }

            fragmentViewModel.next()
        }

    }


}
