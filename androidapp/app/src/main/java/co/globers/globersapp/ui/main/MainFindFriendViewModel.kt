package co.globers.globersapp.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.globers.globersapp.firestore.entities.DbUserPublicProfile
import co.globers.globersapp.firestore.executeQuery
import co.globers.globersapp.services.ContactsManager
import co.globers.globersapp.utils.NonNullLiveData
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainFindFriendViewModel : ViewModel() {

    enum class Status{
        NOT_STARTED,
        IN_PROCESS,
        USER_NOT_FOUND,
        REQUEST_SEND
    }

    val status = NonNullLiveData(Status.NOT_STARTED)
    private val db = FirebaseFirestore.getInstance()

    fun sendFriendRequest(username: String) {

        status.value = Status.IN_PROCESS

        var normalizedUsername = username.toLowerCase()
        if (!normalizedUsername.startsWith("@")){
            normalizedUsername = "@$normalizedUsername"
        }

        viewModelScope.launch(Dispatchers.IO) {

            val userPublicProfile = executeQuery<DbUserPublicProfile>(
                db.collection(DbUserPublicProfile.COLLECTION_NAME)
                    .whereEqualTo(DbUserPublicProfile.FIELD_USERNAME, normalizedUsername)
                    .limit(1)
            ).firstOrNull()

            if (userPublicProfile == null) {
                status.postValue(Status.USER_NOT_FOUND)
            } else {
                val contactsManager = ContactsManager()
                contactsManager.sendFriendRequest(userPublicProfile.userUid)
                status.postValue(Status.REQUEST_SEND)
            }

        }

    }

    fun reset() {
        status.value = Status.NOT_STARTED
    }
}
