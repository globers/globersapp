package co.globers.globersapp.notifservice

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.PendingIntent.FLAG_CANCEL_CURRENT
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.AudioManager
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.DEFAULT_ALL
import androidx.core.content.ContextCompat
import co.globers.globersapp.R
import co.globers.globersapp.firestore.entities.DbUserAwardType
import co.globers.globersapp.firestore.entities.toDisplayMessage
import co.globers.globersapp.notifservice.WaitingUserService.Companion.WAITING_PEER_NOTIFICATION_ID
import co.globers.globersapp.services.updateMessageTokenInFirestore
import co.globers.globersapp.ui.main.MainActivity
import co.globers.globersapp.ui.main.MainActivityArgs
import co.globers.globersapp.ui.phonecall.PhoneCallActivity
import co.globers.globersapp.ui.taboo.TabooActivity
import co.globers.globersapp.ui.taboo.TabooActivityArgs
import co.globers.globersapp.ui.taboo.TabooActivityLessonArgs
import co.globers.globersapp.ui.taboo.TabooActivityTeammateArgs
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MessageService: FirebaseMessagingService() {

    companion object {
        const val LOG_TAG = "MESSAGE_SERVICE"
        const val TABOO_GAME_PHONE_CALL_CHANEL_ID = "TABOO_GAME_PHONE_CALL_CHANEL_ID"
        const val FRIEND_REQUEST_CHANEL_ID = "FRIEND_REQUEST_CHANEL_ID"
        const val FRIEND_REQUEST_ACCEPTED_CHANEL_ID = "FRIEND_REQUEST_ACCEPTED_CHANEL_ID"
        const val USER_AWARD_RECEIVED_CHANEL_ID = "USER_AWARD_RECEIVED_CHANEL_ID"
        const val WAITING_PEER_MATCH_CHANEL_ID = "WAITING_PEER_MATCH_CHANEL_ID"

        const val TABOO_GAME_PHONE_CALL_CHANEL_READABLE = "Phone call from friend"
        const val FRIEND_REQUEST_CHANEL_READABLE = "Friend request"
        const val FRIEND_REQUEST_ACCEPTED_CHANEL_READABLE = "Friend request accepted"
        const val USER_AWARD_RECEIVED_CHANEL_READABLE = "Award received"
        const val WAITING_PEER_MATCH_CHANEL_READABLE = "Player available"

        const val TABOO_GAME_PHONE_CALL_NOTIFICATION_ID = 77
        const val FRIEND_REQUEST_NOTIFICATION_ID = 78
        const val FRIEND_REQUEST_ACCEPTED_NOTIFICATION_ID = 79
        const val USER_AWARD_RECEIVED_NOTIFICATION_ID = 80
        const val WAITING_PEER_MATCH_NOTIFICATION_ID = 81

        const val MESSAGE_FIELD_MESSAGE_TYPE = "messageType"
    }

    init {
        Log.d(LOG_TAG, "start Message service")
    }

    override fun onNewToken(token: String) {

        Log.d(LOG_TAG, "onNewToken: $token")

        val userUid = FirebaseAuth.getInstance().uid
        if (userUid == null){
            Log.e(LOG_TAG, "new token received but user is not logged in")
        } else {
            updateMessageTokenInFirestore(userUid, token)
        }


    }

    private sealed class MessageData {



        class Unknown(val messageType: String?): MessageData()


        class TabooGameCallRequest(
            val callRequestUid: String,
            val callerUid: String,
            val peerMatchUid: String,
            val callerDisplayName: String
        ) : MessageData(){

            constructor(messageData: Map<String, String>): this(
                messageData.getValue("callRequestUid"),
                messageData.getValue("callerUid"),
                messageData.getValue("peerMatchUid"),
                messageData.getValue("callerDisplayName")
            )

            companion object {
                const val MESSAGE_TYPE = "tabooGameCallRequest"
            }
        }

        class FriendRequest(
            val friendRequestUid: String,
            val senderUserUid: String,
            val senderDisplayName: String
        ): MessageData(){

            constructor(messageData: Map<String, String>): this(
                messageData.getValue("friendRequestUid"),
                messageData.getValue("senderUserUid"),
                messageData.getValue("senderDisplayName")
            )

            companion object {
                const val MESSAGE_TYPE = "friendRequest"
            }

        }

        class FriendRequestAccepted(
            val friendRequestUid: String,
            val receiverUserUid: String,
            val receiverDisplayName: String
        ): MessageData(){

            constructor(messageData: Map<String, String>): this(
                messageData.getValue("friendRequestUid"),
                messageData.getValue("receiverUserUid"),
                messageData.getValue("receiverDisplayName")
            )

            companion object {
                const val MESSAGE_TYPE = "friendRequestAccepted"
            }

        }

        class UserAwardReceived(
            val userAwardUid: String,
            val giverDisplayName: String,
            val userAwardType: String
        ): MessageData(){

            constructor(messageData: Map<String, String>): this(
                messageData.getValue("userAwardUid"),
                messageData.getValue("giverDisplayName"),
                messageData.getValue("userAwardType")
            )

            companion object {
                const val MESSAGE_TYPE = "userAwardReceived"
            }

        }

        class WaitingPeerMatch(
            val peerMatchUid: String,
            val userDisplayName: String,
            val userCountryIsoCode: String,
            val userNativeLanguageLabel: String
        ): MessageData(){

            constructor(messageData: Map<String, String>): this(
                messageData.getValue("peerMatchUid"),
                messageData.getValue("userDisplayName"),
                messageData.getValue("userCountryIsoCode"),
                messageData.getValue("userNativeLanguageLabel")
            )

            companion object {
                const val MESSAGE_TYPE = "waitingPeerMatch"
            }

        }



        companion object {

            fun build(data: Map<String, String>): MessageData {
                return when(data[MESSAGE_FIELD_MESSAGE_TYPE]){
                    TabooGameCallRequest.MESSAGE_TYPE -> TabooGameCallRequest(data)
                    FriendRequest.MESSAGE_TYPE -> FriendRequest(data)
                    FriendRequestAccepted.MESSAGE_TYPE -> FriendRequestAccepted(data)
                    UserAwardReceived.MESSAGE_TYPE -> UserAwardReceived(data)
                    WaitingPeerMatch.MESSAGE_TYPE -> WaitingPeerMatch(data)
                    else -> Unknown(data[MESSAGE_FIELD_MESSAGE_TYPE])
                }
            }
        }


    }

    override fun onMessageReceived(message: RemoteMessage) {

        Log.d(LOG_TAG, "received message: messageId: ${message.messageId} data:${message.data}")

        val data = message.data

        createChannels()

        when(val messageData = MessageData.build(data)){
            is MessageData.TabooGameCallRequest -> {
                sendTabooGameCallNotif(messageData)
            }
            is MessageData.FriendRequest -> {
                sendFriendRequestNotif(messageData)
            }
            is MessageData.FriendRequestAccepted -> {
                sendFriendRequestAcceptedNotif(messageData)
            }
            is MessageData.UserAwardReceived-> {
                sendUserAwardReceivedNotif(messageData)
            }
            is MessageData.WaitingPeerMatch -> {
                waitingPeerMatchNotif(messageData)
            }
            is MessageData.Unknown -> {
                Log.w(LOG_TAG, "received message with unknown messageType: ${messageData.messageType}, will be ignored")
            }
        }
    }

    private fun waitingPeerMatchNotif(messageData: MessageData.WaitingPeerMatch) {

        Log.d(LOG_TAG, "waitingPeerMatchNotif, message:$messageData")

        // to manage case where status is still "waiting" in firestore (inconsistency between notif & firestore state)
        val isWaitingNotifActive = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val notifs = notificationManager.activeNotifications
            notifs.any { it.id == WAITING_PEER_NOTIFICATION_ID }
        } else {
            true // TODO: if api < 23, we suppose notif is active, this is a problem if app is killed without time to set NOT_WAITING status
        }

        if (isWaitingNotifActive) {
            val notificationBuilder = NotificationCompat.Builder(this, WAITING_PEER_MATCH_CHANEL_ID)
                .setSmallIcon(R.drawable.notif_globers_icon) // icon on the bar on the screen
                .setColor(ContextCompat.getColor(this, R.color.myColorPrimary))
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.globers_icon))
                .setContentTitle("${messageData.userDisplayName} want to play, tap to join!")
                .setContentText("Hurry up before someone else join \u263A")
                .setTimeoutAfter(15000) // cancelled after 15sec
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH) // don't know what is really for but just in case...
                .setContentIntent(startTabooActivityPendingIntent())
                .setDefaults(DEFAULT_ALL)

            val ringerMode = (getSystemService(Context.AUDIO_SERVICE) as AudioManager).ringerMode

            if (ringerMode == AudioManager.RINGER_MODE_NORMAL){
                notificationBuilder.setSound(RingtoneManager.getActualDefaultRingtoneUri(this, RingtoneManager.TYPE_NOTIFICATION))
            } else if (ringerMode == AudioManager.RINGER_MODE_VIBRATE){
                notificationBuilder.setVibrate(longArrayOf(0L, 1000L, 1000L, 1000L, 1000L, 1000L, 1000L, 1000L, 1000L)) // delay, vibrate, sleep, vibrate, sleep...
            }

            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.notify(
                WAITING_PEER_MATCH_NOTIFICATION_ID,
                notificationBuilder.build()
            )
        }

    }

    private fun startTabooActivityPendingIntent(): PendingIntent {

        val intent = Intent(this, TabooActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK // must be added to start activity from outside activity
        val tabooArgs = TabooActivityArgs(
            null,
            null,
            false
        )
        intent.putExtra(TabooActivity.EXTRA_TABOO_ACTIVITY_ARGS, tabooArgs)

        return PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT)
    }



    private fun sendFriendRequestNotif(friendRequest: MessageData.FriendRequest) {


        val intent = Intent(this, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        intent.putExtra(MainActivity.EXTRA_MAIN_ACTIVITY_ARGS, MainActivityArgs(true))

        val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT)

        val notificationBuilder = NotificationCompat.Builder(this, FRIEND_REQUEST_CHANEL_ID)
            .setSmallIcon(R.drawable.notif_globers_icon) // icon on the bar on the screen
            .setColor(ContextCompat.getColor(this, R.color.myColorPrimary))
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.globers_icon))
            .setContentTitle("${friendRequest.senderDisplayName} sent you a friend request")
            .setAutoCancel(true) // to close notification when user click on it (launching fullScreen activity)
            .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(FRIEND_REQUEST_NOTIFICATION_ID, notificationBuilder.build())
    }

    private fun sendFriendRequestAcceptedNotif(friendRequest: MessageData.FriendRequestAccepted) {

        val intent = Intent(this, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        intent.putExtra(MainActivity.EXTRA_MAIN_ACTIVITY_ARGS, MainActivityArgs(true))

        val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT)

        val notificationBuilder = NotificationCompat.Builder(this, FRIEND_REQUEST_ACCEPTED_CHANEL_ID)
            .setSmallIcon(R.drawable.notif_globers_icon) // icon on the bar on the screen
            .setColor(ContextCompat.getColor(this, R.color.myColorPrimary))
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.globers_icon))
            .setContentTitle("Friend request accepted!")
            .setContentText("You are now friend with ${friendRequest.receiverDisplayName}")
            .setAutoCancel(true) // to close notification when user click on it (launching fullScreen activity)
            .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(FRIEND_REQUEST_ACCEPTED_NOTIFICATION_ID, notificationBuilder.build())
    }

    private fun sendUserAwardReceivedNotif(userAwardReceived: MessageData.UserAwardReceived) {

        val text = "${userAwardReceived.giverDisplayName} gave you the award: \"${toDisplayMessage(
            DbUserAwardType.valueOf(userAwardReceived.userAwardType))}\"!"
        val notificationBuilder = NotificationCompat.Builder(this, USER_AWARD_RECEIVED_CHANEL_ID)
            .setSmallIcon(R.drawable.notif_globers_icon) // icon on the bar on the screen
            .setColor(ContextCompat.getColor(this, R.color.myColorPrimary))
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.newuser_leveltestresult_welldone))
            .setContentTitle("Award received!")
            .setContentText(text)
            .setStyle(NotificationCompat.BigTextStyle().bigText(text))

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(USER_AWARD_RECEIVED_NOTIFICATION_ID, notificationBuilder.build())
    }


    private fun phoneCallIntent(messageData: MessageData.TabooGameCallRequest): PendingIntent{
        // activity displayed if phone is locked
        val fullScreenIntent = Intent(this, PhoneCallActivity::class.java)
        fullScreenIntent.putExtra(PhoneCallActivity.EXTRA_ARGS,
            PhoneCallActivity.Args(
                TABOO_GAME_PHONE_CALL_NOTIFICATION_ID,
                messageData.callerUid,
                messageData.callRequestUid,
                messageData.peerMatchUid,
                messageData.callerDisplayName
            ))


        // checker if usefull fullScreenIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        val replyPendingIntent = PendingIntent.getActivity(
            this, 0 /* Request code */, fullScreenIntent, PendingIntent.FLAG_CANCEL_CURRENT) // cancel the previous if new one started https://developer.android.com/reference/android/app/PendingIntent

        return replyPendingIntent
    }

    private fun callReplyAcceptIntent(data: MessageData.TabooGameCallRequest): PendingIntent{
        val intent = Intent(this, CallReplyAcceptBroadcastReceiver::class.java)

        val args = CallReplyAcceptBroadcastReceiver.Args(
            TABOO_GAME_PHONE_CALL_NOTIFICATION_ID,
            data.callRequestUid,
            data.callerUid,
            data.peerMatchUid
        )

        intent.putExtra(CallReplyAcceptBroadcastReceiver.EXTRA_ARGS, args)
        return PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT)
    }

    private fun callReplyDeclineIntent(data: MessageData.TabooGameCallRequest): PendingIntent{
        val denyCallIntent = Intent(this, CallReplyDeclineBroadcastReceiver::class.java)

        val args = CallReplyDeclineBroadcastReceiver.Args(
            TABOO_GAME_PHONE_CALL_NOTIFICATION_ID,
            data.callRequestUid,
            data.callerUid,
            data.peerMatchUid
        )

        denyCallIntent.putExtra(CallReplyDeclineBroadcastReceiver.EXTRA_ARGS, args)
        return PendingIntent.getBroadcast(this, 0, denyCallIntent, PendingIntent.FLAG_CANCEL_CURRENT)
    }



    private fun sendTabooGameCallNotif(messageData: MessageData.TabooGameCallRequest){

        val callerName = messageData.callerDisplayName

        val notificationBuilder = NotificationCompat.Builder(this, TABOO_GAME_PHONE_CALL_CHANEL_ID)
            .setSmallIcon(R.drawable.notif_globers_icon) // icon on the bar on the screen
            .setColor(ContextCompat.getColor(this, R.color.myColorPrimary))
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.globers_icon))
            .setContentTitle(callerName)
            .setAutoCancel(true) // to close notification when user click on it (launching fullScreen activity)
            .setTimeoutAfter(15000) // cancelled after 15sec
            .addAction(android.R.drawable.ic_menu_close_clear_cancel, "Deny", callReplyDeclineIntent(messageData))
            .addAction(android.R.drawable.ic_menu_call, "Accept", callReplyAcceptIntent(messageData))
            .setFullScreenIntent(phoneCallIntent(messageData), true) // force notif to display "open" and launch phoneCallIntent if locked. use with permission USE_FULL_SCREEN_INTENT https://developer.android.com/training/notify-user/build-notification.html
            .setCategory(NotificationCompat.CATEGORY_CALL) // don't know what is really for but just in case...
            .setPriority(NotificationCompat.PRIORITY_HIGH) // don't know what is really for but just in case...

        val ringerMode = (getSystemService(Context.AUDIO_SERVICE) as AudioManager).ringerMode

        if (ringerMode == AudioManager.RINGER_MODE_NORMAL){
            notificationBuilder.setSound(RingtoneManager.getActualDefaultRingtoneUri(this, RingtoneManager.TYPE_RINGTONE))
        } else if (ringerMode == AudioManager.RINGER_MODE_VIBRATE){
            notificationBuilder.setVibrate(longArrayOf(0L, 1000L, 1000L, 1000L, 1000L, 1000L, 1000L, 1000L, 1000L, 1000L, 1000L, 1000L, 1000L)) // delay, vibrate, sleep, vibrate, sleep...
        }


        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(TABOO_GAME_PHONE_CALL_NOTIFICATION_ID, notificationBuilder.build())


        // start service to kill notification if call canceled
        val listenCallToCancelNotifServiceIntent = Intent(this, ListenCallRequestStatusService::class.java)
        listenCallToCancelNotifServiceIntent.putExtra(ListenCallRequestStatusService.EXTRA_ARGS,
            ListenCallRequestStatusService.Args(
                TABOO_GAME_PHONE_CALL_NOTIFICATION_ID,
                messageData.callerUid,
                messageData.callRequestUid,
                messageData.peerMatchUid,
                messageData.callerDisplayName
            ))
        startService(listenCallToCancelNotifServiceIntent)

    }

    private fun createChannels(){

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val channel = NotificationChannel(TABOO_GAME_PHONE_CALL_CHANEL_ID, TABOO_GAME_PHONE_CALL_CHANEL_READABLE, NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(channel)

            val channelFriendRequests = NotificationChannel(FRIEND_REQUEST_CHANEL_ID, FRIEND_REQUEST_CHANEL_READABLE, NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channelFriendRequests)

            val channelFriendRequestsAccepted = NotificationChannel(FRIEND_REQUEST_ACCEPTED_CHANEL_ID, FRIEND_REQUEST_ACCEPTED_CHANEL_READABLE, NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channelFriendRequestsAccepted)

            val userAwardReceived = NotificationChannel(USER_AWARD_RECEIVED_CHANEL_ID, USER_AWARD_RECEIVED_CHANEL_READABLE, NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(userAwardReceived)

            val waitingPeerMatch = NotificationChannel(WAITING_PEER_MATCH_CHANEL_ID, WAITING_PEER_MATCH_CHANEL_READABLE, NotificationManager.IMPORTANCE_HIGH)
            waitingPeerMatch.enableVibration(true)
            notificationManager.createNotificationChannel(waitingPeerMatch)

        }
    }

    // https://github.com/firebase/quickstart-android/blob/d886d348a681f41f02e78d720cb74fb8c162e339/messaging/app/src/main/java/com/google/firebase/quickstart/fcm/kotlin/MyFirebaseMessagingService.kt#L26-L61
    // https://firebase.google.com/docs/cloud-messaging/android/receive

}