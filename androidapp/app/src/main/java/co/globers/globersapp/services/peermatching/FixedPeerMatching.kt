package co.globers.globersapp.services.peermatching

import android.util.Log
import co.globers.globersapp.firestore.*
import co.globers.globersapp.firestore.entities.DbContact
import co.globers.globersapp.firestore.entities.DbPeerMatch
import co.globers.globersapp.firestore.entities.DbPeerMatchStatus
import co.globers.globersapp.firestore.entities.DbUserGameInfo
import co.globers.globersapp.ui.taboo.TabooActivityTeammateArgs
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FieldPath
import com.google.firebase.firestore.FirebaseFirestore
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import java.util.*

private const val LOG_TAG = "FixedPeerMatching"

class FixedPeerMatching(
    private val userGameInfo: DbUserGameInfo,
    private val teammateArgs: TabooActivityTeammateArgs
) : IPeerMatching {


    private var peerMatchSub: Disposable? = null
    // db service
    private val db = FirebaseFirestore.getInstance()
    private val peerMatchUid = teammateArgs.peerMatchUid
    private val peerMatchDocRef: DocumentReference = db.collection(DbPeerMatch.COLLECTION_NAME_FIXED).document(peerMatchUid)

    override val peerMatchingState =
        BehaviorSubject.createDefault<IPeerMatching.State>(
            IPeerMatching.State(
                IPeerMatching.Status.NotStarted,
                null
            )
        )

    override fun start() {

        peerMatchingState.onNext(
            IPeerMatching.State(
                IPeerMatching.Status.Started,
                null
            )
        )

        if (teammateArgs.isCreator){

            MainScope().launch {

                try {
                    val canceledByJoiner =
                        executeTransaction { transition ->

                            val snapshot = transition.get(peerMatchDocRef)
                            val alreadyCanceledByJoiner = snapshot.exists()
                            if (alreadyCanceledByJoiner) {
                                // we must update a doc read in a transaction: firestore constraint
                                // so we make a "void" update
                                transition.update(peerMatchDocRef, mapOf())
                            } else {
                                // create peerMatch and wait for joiner to join
                                val peerMatchToCreate =
                                    DbPeerMatch(
                                        peerMatchUid,
                                        teammateArgs.teammateUserUid,
                                        userGameInfo,
                                        null,
                                        DbPeerMatchStatus.WAITING_FOR_PEER,
                                        UUID.randomUUID().toString(),
                                        null,
                                        0
                                    )

                                transition.set(peerMatchDocRef, peerMatchToCreate)
                            }
                            alreadyCanceledByJoiner
                        }

                    if (canceledByJoiner) {
                        setPeerMatchCanceled()
                    } else {
                        peerMatchSub = listen<DbPeerMatch>(
                            peerMatchDocRef
                        ).subscribe { peerMatch ->
                            onCreatedPeerMatchUpdate(peerMatch)
                        }
                    }
                } catch (e: java.lang.Exception){
                    setError(e)
                }
            }

        } else {
            peerMatchSub = listenNullable<DbPeerMatch>(
                peerMatchDocRef
            ).subscribe {
                onPeerMatchToJoinUpdate(it.value)
            }
        }

    }

    private fun onCreatedPeerMatchUpdate(peerMatch: DbPeerMatch){

        if (peerMatch.status == DbPeerMatchStatus.JOINED){
            if (peerMatch.gameUid == null){
                MainScope().launch {
                    associateGameToPeerMatching(
                        peerMatch,
                        peerMatchDocRef,
                        db
                    )
                }
            } else {
                MainScope().launch {
                    setPeerMatchCompleted(peerMatch)
                }
            }

        } else if (peerMatch.status == DbPeerMatchStatus.CANCELED){
            setPeerMatchCanceled()
        }

    }


    private fun onPeerMatchToJoinUpdate(peerMatch: DbPeerMatch?){

        if (peerMatch != null){
            if (peerMatch.status == DbPeerMatchStatus.WAITING_FOR_PEER) {

                MainScope().launch {

                    try {
                        executeUpdate(
                            peerMatchDocRef, mapOf(
                                DbPeerMatch.FIELD_STATUS to DbPeerMatchStatus.JOINED,
                                DbPeerMatch.FIELD_JOINER to userGameInfo
                            )
                        )
                    } catch (e: Exception){
                        setError(e)
                    }
                }

            } else if(peerMatch.status == DbPeerMatchStatus.JOINED) {

                if (peerMatch.gameUid != null){
                    MainScope().launch {
                        setPeerMatchCompleted(peerMatch)
                    }
                }
            } else if(peerMatch.status == DbPeerMatchStatus.CANCELED) {
                // send cancel status
                setPeerMatchCanceled()

            }

        }

    }

    override fun abort() {

        if (!IPeerMatching.isFinished(
                peerMatchingState.value!!.status
            )
        ) {
            MainScope().launch {
                if (teammateArgs.isCreator) {

                    try {
                        executeUpdate(
                            peerMatchDocRef,
                            FieldPath.of(DbPeerMatch.FIELD_STATUS),
                            DbPeerMatchStatus.CANCELED
                        )
                    } catch (e: Exception) {
                        setError(e)
                    }

                } else {
                    // joiner: we must handle the case where joiner abort before creator created docRef

                    try {
                        executeTransaction { transaction ->

                            val snapshot = transaction.get(peerMatchDocRef)
                            val alreadyCreatedByCreator = snapshot.exists()
                            if (alreadyCreatedByCreator) {
                                transaction.update(
                                    peerMatchDocRef,
                                    DbPeerMatch.FIELD_STATUS,
                                    DbPeerMatchStatus.CANCELED
                                )
                            } else {

                                // create peerMatch as already "canceled"
                                val canceledPeerMatch =
                                    DbPeerMatch(
                                        peerMatchUid,
                                        userGameInfo.userUid,
                                        DbUserGameInfo(),// should not be used
                                        null,
                                        DbPeerMatchStatus.CANCELED,
                                        "FIXED_PEERMATCH_CANCELED_CHANEL",
                                        null,
                                        0
                                    )

                                transaction.set(peerMatchDocRef, canceledPeerMatch)
                            }

                        }

                    } catch (e: Exception) {
                        setError(e)
                    }

                }
            }
        }
        setPeerMatchCanceled()
    }



    private suspend fun setPeerMatchCompleted(peerMatch: DbPeerMatch) {
        Log.d(
            LOG_TAG,
            "Fixed peer match completed"
        )

        val tabooGame = getTabooGame(
            db,
            peerMatch,
            teammateArgs.isCreator,
            userGameInfo.userUid
        )

        val teammateContact =
            executeGetOrNull<DbContact>(
                DbContact.getDocReference(
                    db,
                    userGameInfo.userUid,
                    teammateArgs.teammateUserUid
                )
            )

        peerMatchingState.onNext(
            IPeerMatching.State(
                IPeerMatching.Status.Completed,
                IPeerMatching.Result(
                    peerMatch,
                    teammateArgs.isCreator,
                    tabooGame,
                    teammateContact
                )
            )
        )
        finishSubscriptions()
    }

    private fun setPeerMatchCanceled() {
        Log.e(
            LOG_TAG,
            "Fixed peer match canceled"
        )
        finishSubscriptions()
        peerMatchingState.onNext(
            IPeerMatching.State(
                IPeerMatching.Status.Aborted,
                null
            )
        )
    }

    private fun finishSubscriptions(){
        peerMatchSub?.dispose()
    }

    private fun setError(e: Exception){
        Log.e(
            LOG_TAG,
            "Error in Fixed PeerMatching",
            e
        )
        finishSubscriptions()
        peerMatchingState.onNext(
            IPeerMatching.State(
                IPeerMatching.Status.Error,
                null
            )
        )
    }

}