package co.globers.globersapp.ui.newuser

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.globers.globersapp.firestore.entities.DbLanguageIsoCode
import co.globers.globersapp.firestore.entities.DbLogitParams
import co.globers.globersapp.firestore.entities.DbUser
import co.globers.globersapp.firestore.entities.tokeepinsync.DbDictionary
import co.globers.globersapp.firestore.entities.tokeepinsync.DbTabooProgram
import co.globers.globersapp.firestore.executeGet
import co.globers.globersapp.firestore.executeGetOrNull
import co.globers.globersapp.services.createUserAndProgram
import co.globers.globersapp.services.getProgram
import co.globers.globersapp.services.getStartingLevelUid
import co.globers.globersapp.utils.NonNullLiveData
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

data class TestResult(
    val logitResult: DbLogitParams,
    val estimatedNbWordsKnown: Int,
    val startLevelUid: String
)

class NewUserViewModel() : ViewModel() {

    companion object {
        const val LOG_TAG = "NewUserVM"
    }

    enum class UserCreationStep {
        WaitingForTestLoading,
        LanguageTest,
        LanguageTestResult,
        UserAlreadyExists,
        WaitingForUserAndProgramCreation,
        Finished
    }

    val currentUserCreationStep = NonNullLiveData(UserCreationStep.WaitingForTestLoading)
    val languageIsoCode = DbLanguageIsoCode.ENG

    var testResult: TestResult? = null

    var dictionary: DbDictionary? = null
    var program: DbTabooProgram? = null
    private val db: FirebaseFirestore = FirebaseFirestore.getInstance()


    fun startTest(){

        viewModelScope.launch {
            val dictNameInDb = "sample_$languageIsoCode" // TODO NICO: See how to manage hardcoded string
            val dictDocRef = db.collection(DbDictionary.COLLECTION_NAME).document(dictNameInDb)

            val dictDiffered = async { executeGet<DbDictionary>(dictDocRef)}
            val programDiffered = async { getProgram(languageIsoCode)}

            dictionary = dictDiffered.await()
            program = programDiffered.await()
            currentUserCreationStep.value = UserCreationStep.LanguageTest
        }

    }

    fun setTestResult(logitResult: DbLogitParams, estimatedNbWordsKnown: Int) {

        val startingLevelUid = getStartingLevelUid(program!!, logitResult)

        testResult = TestResult(logitResult, estimatedNbWordsKnown, startingLevelUid)
        currentUserCreationStep.value = UserCreationStep.LanguageTestResult
    }


    fun createUser(firebaseUserUid: String, name: String, photoUrl: String?, overrideExistingUser: Boolean = false) {

        currentUserCreationStep.value = UserCreationStep.WaitingForUserAndProgramCreation

        viewModelScope.launch {

            val userAlreadyExists = if (overrideExistingUser) false else
                executeGetOrNull<DbUser>(db.collection(DbUser.COLLECTION_NAME).document(firebaseUserUid)) != null

            if (userAlreadyExists) {
                currentUserCreationStep.postValue(UserCreationStep.UserAlreadyExists)
            } else {

                createUserAndProgram(
                    firebaseUserUid,
                    name.split(' ').first(),
                    photoUrl,
                    languageIsoCode,
                    program!!,
                    testResult!!
                )
                currentUserCreationStep.postValue(UserCreationStep.Finished)
            }
        }
    }





}
