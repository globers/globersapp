package co.globers.globersapp.utils

open class GlobersException(message: String): Exception(message) {
}


class InconsistantDbStateExcepton(message: String): GlobersException(message) {}


// should never happens
class BugException(message: String): GlobersException(message) {}