package co.globers.globersapp.services

import co.globers.globersapp.firestore.entities.*
import co.globers.globersapp.firestore.entities.tokeepinsync.DbTabooProgram
import co.globers.globersapp.firestore.executeBatch
import co.globers.globersapp.ui.newuser.TestResult
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.WriteBatch
import java.util.*
import kotlin.random.Random

suspend fun createUserAndProgram(
    firebaseUid: String,
    displayName: String,
    photoUrl: String?,
    languageIsoCode: DbLanguageIsoCode,
    program: DbTabooProgram,
    testResult: TestResult): DbUser {

    val db: FirebaseFirestore = FirebaseFirestore.getInstance()

    val userProgram = buildUserProgram(program, testResult.logitResult)

    val learnedLanguage = DbLearnedLanguage(
        languageIsoCode,
        DbLanguageLevel(testResult.logitResult, testResult.estimatedNbWordsKnown),
        userProgram.uid
    )

    val userName = "@" + displayName.toLowerCase() + Random.nextInt(100000, 999999)

    val dbUser = DbUser(firebaseUid, listOf(learnedLanguage), DbUserState(false))
    val dbUserPublicProfile = DbUserPublicProfile(firebaseUid, displayName, userName, photoUrl, Locale.getDefault().isO3Language.toUpperCase())

    executeBatch { batch: WriteBatch ->
        batch.set(db.collection(DbUser.COLLECTION_NAME).document(firebaseUid), dbUser)
        batch.set(db.collection(DbUserPublicProfile.COLLECTION_NAME).document(firebaseUid), dbUserPublicProfile)
        batch.set(DbTabooProgramUserState.buildDocRef(db, firebaseUid, userProgram.uid), userProgram)
    }
    return dbUser
}

private fun buildUserProgram(program: DbTabooProgram, logitLogFreqInverseParams: DbLogitParams): DbTabooProgramUserState {

    val uid = UUID.randomUUID().toString()
    val startingLevel = getStartingLevelUid(program, logitLogFreqInverseParams)
    return buildUserProgram(uid, program, startingLevel)
}

private fun buildUserProgram(userProgramUid: String, program: DbTabooProgram, minLevelUid: String): DbTabooProgramUserState {
    val userLevels = program.levels.map {level ->
        level.uid to
                DbTabooLevelUserState(level.lessons.map {
                        lesson -> lesson.lessonUid to DbTabooLessonUserStateSummary("userLesson-${lesson.lessonUid}", 0) }.toMap())

    }.toMap()
    return DbTabooProgramUserState(userProgramUid, program.uid, minLevelUid, userLevels)
}

