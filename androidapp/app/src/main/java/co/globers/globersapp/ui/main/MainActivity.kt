package co.globers.globersapp.ui.main

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import co.globers.globersapp.R
import co.globers.globersapp.services.UploadRecordingsWorker
import co.globers.globersapp.utils.GlobersAppUpdateManager
import co.globers.globersapp.utils.InternetConnectionManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.launch


@Parcelize
class MainActivityArgs(
    val openFriendsTab: Boolean
) : Parcelable

class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController
    private val updateManager = GlobersAppUpdateManager()

    companion object {
        private const val LOG_TAG = "MainActivity"
        const val EXTRA_MAIN_ACTIVITY_ARGS = "extraMainActivityArgs"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        InternetConnectionManager(this, false)

        if (savedInstanceState != null) {
            Log.d(LOG_TAG, "MainActivity:onCreate, savedInstanteState != null, we restart activity")
            // it means it has been "killed" by android (waited too long or memory too low), we restart at the beginning
            finish()
            startActivity(intent)
        }

        lifecycleScope.launch {

            val updateRequired = updateManager.updateAppIfUpdateAvailable(this@MainActivity)

            if (!updateRequired) {

                navController =
                    Navigation.findNavController(this@MainActivity, R.id.main_navhost_fragment)
                main_bottom_navigation_view.setupWithNavController(navController)

                val args = intent.getParcelableExtra<MainActivityArgs>(EXTRA_MAIN_ACTIVITY_ARGS)
                if (args != null && args.openFriendsTab) {
                    val bottomNavView =
                        findViewById<BottomNavigationView>(R.id.main_bottom_navigation_view)
                    bottomNavView.selectedItemId = R.id.mainFriendsFragment
                }

                UploadRecordingsWorker.start(applicationContext)
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d(LOG_TAG, "onActivityResult called, requestCode: $requestCode, resultCode: $resultCode")
        updateManager.onActivityResult(this, requestCode, resultCode)
        super.onActivityResult(requestCode, resultCode, data)
    }


    override fun onResume() {
        updateManager.onResume(this)
        super.onResume()
    }

}


