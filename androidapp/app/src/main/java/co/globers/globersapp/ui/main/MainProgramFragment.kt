package co.globers.globersapp.ui.main

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import co.globers.globersapp.Globers
import co.globers.globersapp.R
import co.globers.globersapp.services.LessonStatus
import co.globers.globersapp.services.askPermissions
import co.globers.globersapp.services.permissionsOk
import co.globers.globersapp.ui.taboo.TabooActivity
import co.globers.globersapp.ui.taboo.TabooActivityArgs
import co.globers.globersapp.ui.taboo.TabooActivityLessonArgs
import co.globers.globersapp.utils.Screen
import co.globers.globersapp.utils.Tracking
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.main_program_fragment.*
import kotlin.math.max


class MainProgramFragment : Fragment() {

    companion object {
        fun newInstance() = MainProgramFragment()
        private const val RC_TABOO_ONBOARDING = 1
        private const val RC_TABOO_WITH_LESSON = 2

        private const val PERMISSIONS_REQUEST_RECORD_AUDIO = 78
    }

    private lateinit var viewModel: MainProgramViewModel
    private lateinit var tracking: Tracking
    private var tabooInstructionsAlreadyDisplayed = false


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_program_fragment, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tracking = Tracking(this, Screen.MainProgram)
        //lifetime is related to activity to keep between on tab navigation
        viewModel = ViewModelProvider(requireActivity()).get(MainProgramViewModel::class.java)


        val bottomNavView = requireActivity().findViewById<BottomNavigationView>(R.id.main_bottom_navigation_view)
        bottomNavView.visibility = View.VISIBLE

        viewModel.program.observe(viewLifecycleOwner, Observer {

            val adapter = LessonSnippetRecyclerViewAdapter(requireActivity(), it, this::onLessonClick)
            recyclerView_mainProgram_lessonsList.adapter = adapter
            recyclerView_mainProgram_lessonsList.layoutManager = LinearLayoutManager(requireActivity())

            recyclerView_mainProgram_lessonsList.scrollToPosition(max(0, adapter.getCurrentLessonIndex() - 2))// by default scrollTo make index at the top, i want to see 2 elt above it
        })

        // dummy subscriptions because else ".value" on LiveData is null
        viewModel.user.observe(viewLifecycleOwner, Observer {
            tabooInstructionsAlreadyDisplayed = it.userState.tabooInstructionsAlreadyDisplayed
        })
    }


    private var lessonToStart: LessonViewData? = null
    private fun goToInstructions()
    {
        val intent = Intent(activity, TabooOnboardingActivity::class.java)
        startActivityForResult(intent, RC_TABOO_ONBOARDING)
    }


    private fun onLessonClick(lessonViewData: LessonViewData){

        if (lessonViewData.lesson.status == LessonStatus.NOT_STARTED) {

            AlertDialog.Builder(requireActivity())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Lesson locked")
                .setMessage("Please complete the previous lessons to unlock")
                .setNeutralButton("Ok") { _, _ -> }
                .show()

            return
        }

        if (!tabooInstructionsAlreadyDisplayed){
            this.lessonToStart = lessonViewData
            goToInstructions()
            return
        }

        checkPermissionsAndStartLessonActivity(lessonViewData)
    }

    private fun checkPermissionsAndStartLessonActivity(lessonViewData: LessonViewData){

        if ( !permissionsOk(requireActivity()))
        {
            this.lessonToStart = lessonViewData
            askPermissions(this, PERMISSIONS_REQUEST_RECORD_AUDIO)
        } else {

            startLessonActivity(lessonViewData)
        }
    }

    private fun startLessonActivity(lessonViewData: LessonViewData) {
        this.lessonToStart = null
        val intent = Intent(activity, TabooActivity::class.java)

        val program = viewModel.program.value

        if (program != null) {

            val lessonArgs = TabooActivityLessonArgs(
                program.userProgramUid,
                lessonViewData.lesson.userLessonUid,
                lessonViewData.level.levelUid
            )

            val args = TabooActivityArgs(
                lessonArgs,
                null,
                (requireActivity().applicationContext as Globers).adminParams?.fakeVoiceCall ?: false
            )

            intent.putExtra(TabooActivity.EXTRA_TABOO_ACTIVITY_ARGS, args)
            startActivityForResult(intent, RC_TABOO_WITH_LESSON)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_TABOO_ONBOARDING && resultCode == TabooOnboardingActivity.RESULT_OK){

            viewModel.saveTabooInstructionsAlreadyDisplayed()

            val lessonToStartLocal = lessonToStart
            if (lessonToStartLocal != null){
                lessonToStart = null
                checkPermissionsAndStartLessonActivity(lessonToStartLocal)
            }
        } else if (requestCode == RC_TABOO_WITH_LESSON && resultCode == TabooActivity.RESULT_CODE_OPEN_FRIENDS_TAB){
            val bottomNavView = requireActivity().findViewById<BottomNavigationView>(R.id.main_bottom_navigation_view)
            bottomNavView.selectedItemId = R.id.mainFriendsFragment
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == PERMISSIONS_REQUEST_RECORD_AUDIO){
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                startLessonActivity(lessonToStart!!)
            }
        }
    }


}
