package co.globers.globersapp.services

import android.os.CountDownTimer
import android.util.Log
import co.globers.globersapp.AppConfig
import co.globers.globersapp.firestore.entities.*
import co.globers.globersapp.firestore.entities.tokeepinsync.DbTabooCard
import co.globers.globersapp.firestore.entities.tokeepinsync.DbTabooLesson
import co.globers.globersapp.utils.BugException
import com.google.firebase.Timestamp
import com.google.firebase.firestore.*


class TabooGame(
    gameUid: String,
    val isUserCreator: Boolean,
    val userUid: String,
    val peerMatch: DbPeerMatch,
    creatorTabooLesson: DbTabooLesson,
    joinerTabooLesson: DbTabooLesson,
    private var voiceCallState: IVoiceCall.State = IVoiceCall.State(IVoiceCall.Status.NotStarted, null, null)) {

    companion object {
        const val LOG_TAG = "TabooGame"
    }

    enum class Status {
        GameLoading,
        GameReady,
        RunningTalker,
        RunningGuesser,
        Finished,
        AbortedBySelf,
        AbortedByTeammate,
        Error
    }

    private val creatorLessonWords = creatorTabooLesson.cards.flatMap { it.forbiddenWords + it.wordToGuess }.toSet()
    private val joinerLessonWords = joinerTabooLesson.cards.flatMap { it.forbiddenWords + it.wordToGuess }.toSet()

    class TabooGameException(message: String) : Exception(message)
    data class Card(val wordToGuess: String, val forbiddenWords: List<String>, val proposedWords: List<String>)
    data class CardPlayed(val card: Card, val cardPlayed: DbTabooCardPlayed)

    data class State(val status: Status, val currentCard: Card?, val cardsPlayed: List<CardPlayed>, val userReady: Boolean, val teammateReady: Boolean)

    private var state: State = State(
        Status.GameLoading,
        null,
        listOf(),
        userReady = false,
        teammateReady = false
    )
        private set(value) {
            field = value
            Log.d(LOG_TAG, "state update, new state $state")
            onStateChangedCallbacks.forEach { it(field) }
    }

    private val db: FirebaseFirestore = FirebaseFirestore.getInstance()
    private val tabooGameDocRef: DocumentReference
    private val gameListenerRegistration: ListenerRegistration

    private var dbTabooGame: DbTabooGame? = null
    private val creatorCardsUidToPlay = getCardsUidToPlay(peerMatch.creatorGameInfo.tabooLessonUserState)
    private val joinerCardsUidToPlay = getCardsUidToPlay(peerMatch.joinerGameInfo!!.tabooLessonUserState)
    private val cards: Map<String, Card> = creatorTabooLesson.cards.map { it.uid to toCard(it, creatorLessonWords) }.toMap() +
            joinerTabooLesson.cards.map { it.uid to toCard(it, joinerLessonWords) }.toMap()


    private var gameTimer: CountDownTimer? = null


    // listeners
    private val onStateChangedCallbacks: MutableList<((State) -> Unit)> = mutableListOf()
    private var onPercentTimeRemainingCallback: ((Int) -> Unit)? = null

    init {
        tabooGameDocRef = db.collection(DbTabooGame.COLLECTION_NAME).document(gameUid)
        gameListenerRegistration = tabooGameDocRef.addSnapshotListener{s, e -> dbGameListener(s, e)}
    }

    fun registerOnPercentTimeRemainingListener(onPercentTimeRemaining: (Int) -> Unit){
        onPercentTimeRemainingCallback = onPercentTimeRemaining
    }

    fun addOnStateChangedListener(onStateChangedCallback: ((State) -> Unit)){
        onStateChangedCallbacks.add(onStateChangedCallback)
        onStateChangedCallback.invoke(state)
    }

    fun removeListeners(){
        onStateChangedCallbacks.clear()
        onPercentTimeRemainingCallback = null
    }




    private fun start(){

        Log.d(LOG_TAG, "Start TabooGame")

        if (setErrorIfInvalidStatusForAction("start", Status.GameReady)) return
        if (!isUserCreator){
            throw TabooGameException("creatorGameInfo has the responsability to start game")
        }
        val talker = DbTabooPlayer.CREATOR
        updateInFirestore(
            mapOf(
                fieldGameStatus(DbTabooGameStatus.RUNNING),
                fieldActiveCard(
                    getNextActiveCardUid(
                        dbTabooGame!!,
                        talker
                    )
                ),
                fieldTalker(talker)
            )
        )
    }


    fun abort(){

        Log.d(LOG_TAG, "Abort TabooGame")

        if (!isFinished()) {
            updateInFirestore(
                mapOf(
                    fieldGameStatus(DbTabooGameStatus.ABORTED),
                    fieldGameAbortInitiatorUsedUid(userUid),
                    fieldActiveCard(null),
                    fieldTalker(null)
                )
            )
        }
    }

    fun setUserReady(){
        updateInFirestore(mapOf(fieldUserReady(isUserCreator)))
    }


    fun notifyCallStatusChanged(state: IVoiceCall.State){

        if (!isFinished()){
            voiceCallState = state
            val status = state.status
            if (status == IVoiceCall.Status.Finished || status == IVoiceCall.Status.Error){
                updateInFirestore(
                    mapOf(
                        fieldGameStatus(DbTabooGameStatus.ERROR),
                        fieldGameErrorReason("phone call disconnected with status: ${state.status}"),
                        fieldActiveCard(null),
                        fieldTalker(null)
                    )
                )
            }

            updateGameState()
        }
    }

    fun guessWord(){
        if (setErrorIfInvalidStatusForAction("guess", Status.RunningGuesser)) return
        addActionOnActiveCardInFirestore(DbTabooCardAction.FOUND, true)
    }

    fun passCard(){
        if (setErrorIfInvalidStatusForAction("pass", Status.RunningTalker)) return
        addActionOnActiveCardInFirestore(DbTabooCardAction.PASSED, false)
    }

    fun buzzGuesserCard(){
        if (setErrorIfInvalidStatusForAction("buzzGuesser", Status.RunningGuesser)) return
        addActionOnActiveCardInFirestore(DbTabooCardAction.BUZZED_GUESSER, false)
    }

    fun isFinished(): Boolean{
        val status = state.status
        return status == Status.Error ||
                status == Status.Finished ||
                status == Status.AbortedBySelf ||
                status == Status.AbortedByTeammate
    }


    private fun dbGameListener(snapshot: DocumentSnapshot?, exception: FirebaseFirestoreException?){
        when {
            exception != null -> setError(exception)
            snapshot == null -> setError(NullPointerException("dbTabooGame snapshot is null"))
            else -> {

                Log.d(LOG_TAG, "dbGameListener, game update")

                val newDbTabooGame = snapshot.toObject(DbTabooGame::class.java)!!

                if (dbTabooGame == null){
                    Log.d(LOG_TAG, "dbTabooGame was null, so it's first update, we init cards")
                    initGameData(newDbTabooGame)
                }

                dbTabooGame = newDbTabooGame
                updateGameState()
            }
        }
    }


    private fun getCardsUidToPlay(tabooLessonUserState: DbTabooLessonUserState): List<String>{


        val cardsUidToPlay =
            tabooLessonUserState.cardUidToState.filter { it.value.status != DbTabooCardUserStatus.Found }.map { it.key }
                .toMutableList()
        if (cardsUidToPlay.size < AppConfig.TABOO_GAME_MIN_CARD_TO_PLAY){
            // complete with "found cards
            val nbCardsToAdd = AppConfig.TABOO_GAME_MIN_CARD_TO_PLAY - cardsUidToPlay.size
            val cardsToAdd = tabooLessonUserState.cardUidToState.filter { it.value.status == DbTabooCardUserStatus.Found }.map { it.key }.take(nbCardsToAdd)
            cardsUidToPlay += cardsToAdd
        }
        return cardsUidToPlay
    }

    private fun initGameData(newDbTabooGame: DbTabooGame) {

        val gameDurationInMs = newDbTabooGame.gameConfig.nbSecGameDuration * 1000
        gameTimer = object : CountDownTimer(gameDurationInMs.toLong(), 2000) {
            override fun onFinish() {
                if (isUserCreator) {
                    updateInFirestore(fieldsGameFinished())// creatorGameInfo finish the game
                }
            }

            override fun onTick(millisUntilFinished: Long) {
                onPercentTimeRemainingCallback?.invoke((millisUntilFinished * 100 / gameDurationInMs).toInt())
            }
        }
    }


    private fun stopEvents(){
        gameTimer?.cancel()
        gameListenerRegistration.remove()
    }


    private fun updateInFirestore(fields: Map<String, Any?>, callFromSetError: Boolean = false){
        tabooGameDocRef.update(fields).addOnFailureListener { if (!callFromSetError) setError(it) }
    }


    private fun addActionOnActiveCardInFirestore(tabooCardAction: DbTabooCardAction, swapTalkerOnNextCard: Boolean){

        val tabooGame = dbTabooGame!!
        val currentTalker = tabooGame.gameState.currentTalker!!
        val newTalker = if (swapTalkerOnNextCard == (currentTalker == DbTabooPlayer.CREATOR)) DbTabooPlayer.JOINER else DbTabooPlayer.CREATOR

        val fieldsToUpdate: MutableMap<String, Any?> = mutableMapOf(
            fieldNewCardPlayed(tabooGame.gameState.activeCardUid!!, currentTalker, tabooCardAction)
        )

        val nextCardUid = getNextActiveCardUid(tabooGame, newTalker)
        if(nextCardUid == null){
            fieldsToUpdate.putAll(fieldsGameFinished())
        }
        else {
            val swapTalkerField = fieldTalker(newTalker)
            fieldsToUpdate[swapTalkerField.first] = swapTalkerField.second
            val activeCardField = fieldActiveCard(nextCardUid)
            fieldsToUpdate[activeCardField.first] = activeCardField.second


        }
        updateInFirestore(fieldsToUpdate)
    }


    private fun setError(e: Exception){
        Log.e(LOG_TAG, "Error in TabooGame:", e)
        stopEvents()

        state = state.copy(status = Status.Error)

        updateInFirestore(mapOf(
            fieldGameStatus(DbTabooGameStatus.ERROR),
            fieldGameErrorReason(e.toString())
        ), callFromSetError = true)
    }

    private fun setErrorIfInvalidStatusForAction(gameAction: String, expectedStatus: Status) : Boolean{
        if (state.status != expectedStatus){
            setError(TabooGameException("cannot $gameAction if game is not in $expectedStatus status"))
            return true
        }
        return false
    }


    private fun updateGameState() {

        val oldState = state
        state = computeState(dbTabooGame, voiceCallState, isUserCreator)

        if (state.status == Status.GameReady && state.userReady && state.teammateReady && isUserCreator){
            start()
        }
        else if (oldState.status == Status.GameReady && (state.status == Status.RunningTalker || state.status == Status.RunningGuesser)){

            Log.d(LOG_TAG, "game just started, we start end game timer")

            gameTimer!!.start() // game just started, creatorGameInfo manage timer
        }
        else if (isFinished()){
            stopEvents()
        }
    }



    /*
    dbState \ voiceCall	notStarted	starting	started	finished	error
    READY,	            Loading,	Loading,	Ready,	    Error	Error
    RUNNING,	        EXCEPTION	EXCEPTION	Running	    Error	Error
    FINISHED,	        EXCEPTION	EXCEPTION	Finished	Finished	Error
    ABORTED,	        AbordedBy	AbordedBy	AbordedBy	AbordedBy	Error
    ERROR	            Error	    Error	    Error	    Error	Error
     */
    private fun computeState(dbTabooGame: DbTabooGame?, voiceCallState: IVoiceCall.State, isUserCreator: Boolean): State {

        Log.d(LOG_TAG, "compute state, gameState:${dbTabooGame?.gameState}, voiceCallState:${voiceCallState}")

        val voiceStatus = voiceCallState.status
        if (dbTabooGame == null){
            return if (voiceCallState.status == IVoiceCall.Status.NotStarted ||
                voiceCallState.status == IVoiceCall.Status.Starting ||
                voiceCallState.status == IVoiceCall.Status.Started){
                State(Status.GameLoading, null, listOf(), userReady = false, teammateReady = false)
            } else{
                State(Status.Error, null, listOf(), userReady = false, teammateReady = false)
            }
        }

        val gameState = dbTabooGame.gameState
        val playedCards = gameState.playedCards.map { CardPlayed(cards.getValue(it.cardUid), it) }

        val dbGameStatus = gameState.status
        return if ( voiceStatus == IVoiceCall.Status.Error || dbGameStatus == DbTabooGameStatus.ERROR ||
            (voiceStatus == IVoiceCall.Status.Finished && (dbGameStatus == DbTabooGameStatus.READY || dbGameStatus == DbTabooGameStatus.RUNNING))){
            State(
                Status.Error,
                null,
                playedCards,
                userReady = false,
                teammateReady = false
            )
        } else if (dbGameStatus == DbTabooGameStatus.READY && (voiceStatus == IVoiceCall.Status.NotStarted || voiceStatus == IVoiceCall.Status.Starting)){
            State(
                Status.GameLoading,
                null,
                playedCards,
                userReady = false,
                teammateReady = false
            )
        } else if (dbGameStatus == DbTabooGameStatus.READY && voiceStatus == IVoiceCall.Status.Started) {
            State(
                Status.GameReady,
                null, listOf(),
                if(isUserCreator) gameState.creatorReady else gameState.joinerReady,
                if(isUserCreator) gameState.joinerReady else gameState.creatorReady
            )
        } else if (dbGameStatus == DbTabooGameStatus.RUNNING && voiceStatus == IVoiceCall.Status.Started) {

            val talker = gameState.currentTalker

            val runningState = if ((talker == DbTabooPlayer.CREATOR) == isUserCreator)
                Status.RunningTalker
            else
                Status.RunningGuesser


            State(
                runningState,
                getActiveCard(dbTabooGame),playedCards,
                userReady = true,
                teammateReady = true
            )
        } else if (dbGameStatus == DbTabooGameStatus.FINISHED){
            State(
                Status.Finished,
                null,
                playedCards,
                userReady = true,
                teammateReady = true
            )
        } else if (dbGameStatus == DbTabooGameStatus.ABORTED){
            State(
                if (gameState.abortInitiatorUserUid == userUid) Status.AbortedBySelf else Status.AbortedByTeammate,
                null,
                playedCards,
                if(isUserCreator) gameState.creatorReady else gameState.joinerReady,
                if(isUserCreator) gameState.joinerReady else gameState.creatorReady
            )
        } else {
            val e = BugException("Inconsistent dbGame / voiceCall state: dbGameStatus: $dbGameStatus, voiceCallStatus: $voiceStatus")
            Log.e(LOG_TAG, "computeState error", e)
            throw e
        }
    }


    private fun getNextActiveCardUid(game: DbTabooGame, player: DbTabooPlayer): String? {

        val gameState = game.gameState

        val cardsUidsToPlayAtStart = if (player == DbTabooPlayer.CREATOR) creatorCardsUidToPlay else joinerCardsUidToPlay

        val playedCards = game.gameState.playedCards.filter { it.talker == player }
        val playedCardUidsSet = playedCards.map { it.cardUid }.toSet()

        val firstNotPlayedCard = cardsUidsToPlayAtStart.firstOrNull { ! playedCardUidsSet.contains(it) && it != gameState.activeCardUid }

        if (firstNotPlayedCard != null){
            return firstNotPlayedCard
        }


        val foundCards = playedCards.filter { it.action == DbTabooCardAction.FOUND }.map { it.cardUid }.toSet()
        val playedNotFoundCards = playedCards.filter {
            it.action != DbTabooCardAction.FOUND
                    &&! foundCards.contains(it.cardUid)}

        return if (playedNotFoundCards.isNotEmpty()){

            val notFoundCardsExceptActive = playedNotFoundCards.filter { it.cardUid != gameState.activeCardUid }
            if (notFoundCardsExceptActive.isNotEmpty()){
                notFoundCardsExceptActive.random().cardUid
            } else {
                gameState.activeCardUid // there is only one passedCard, it's the current one...we keep it
            }

        } else {
            null
        }

    }

    private fun getActiveCard(dbTabooGame: DbTabooGame): Card? {
        val activeCardUid = dbTabooGame.gameState.activeCardUid ?: return null
        return cards.getValue(activeCardUid)
    }




}

private fun fieldsGameFinished() = mapOf(
    fieldGameStatus(DbTabooGameStatus.FINISHED),
    fieldActiveCard(null),
    fieldTalker(null)
)

private fun fieldGameStatus(status: DbTabooGameStatus) =
    "${DbTabooGame.FIELD_GAME_STATE}.${DbTabooGameState.FIELD_STATUS}" to status


private fun fieldUserReady(isUserCreator: Boolean) =
    "${DbTabooGame.FIELD_GAME_STATE}.${if(isUserCreator) DbTabooGameState.FIELD_CREATOR_READY else DbTabooGameState.FIELD_JOINER_READY}" to true

private fun fieldGameAbortInitiatorUsedUid(userUid: String) =
    "${DbTabooGame.FIELD_GAME_STATE}.${DbTabooGameState.FIELD_ABORT_INITIATOR_USER_UID}" to userUid

private fun fieldNewCardPlayed(cardUid: String, player: DbTabooPlayer, cardState: DbTabooCardAction): Pair<String, Any> {

    val newCardPlayed = FieldValue.arrayUnion(DbTabooCardPlayed(cardUid, cardState, Timestamp.now(), player))
    return "${DbTabooGame.FIELD_GAME_STATE}.${DbTabooGameState.FIELD_PLAYED_CARDS}" to newCardPlayed
}

private fun fieldGameErrorReason(errorReason: String) =
    "${DbTabooGame.FIELD_GAME_STATE}.${DbTabooGameState.FIELD_ERROR_REASON}" to errorReason

private fun fieldActiveCard(cardUid: String?) =
    "${DbTabooGame.FIELD_GAME_STATE}.${DbTabooGameState.FIELD_ACTIVE_CARD_UID}" to cardUid

private fun fieldTalker(player: DbTabooPlayer?) =
    "${DbTabooGame.FIELD_GAME_STATE}.${DbTabooGameState.FIELD_CURRENT_TALKER}" to player


private fun toCard(dbTabooCard: DbTabooCard, possibleWords: Set<String>): TabooGame.Card {

    // take 10 words in the list. must contain the word to guess

    val words = (possibleWords
        .shuffled().asSequence()
        .filter { it != dbTabooCard.wordToGuess }
        .take(12) + dbTabooCard.wordToGuess)
        .toList()
        .sorted()

    return TabooGame.Card(
        dbTabooCard.wordToGuess,
        dbTabooCard.forbiddenWords.take(AppConfig.TABOO_GAME_NB_FORBIDDEN_WORDS),
        words
        )
}