package co.globers.globersapp.services

import co.globers.globersapp.firestore.entities.*
import co.globers.globersapp.firestore.executeBatch
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FieldPath
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch


// Receive changes in TabooGame and update TabooProgramUserState accordingly
class TabooProgramUserStateUpdater(
    private val userGameInfo: DbUserGameInfo,
    lessonUserState: DbTabooLessonUserState,
    tabooGame: TabooGame
) {


    private val db = FirebaseFirestore.getInstance()

    private val tabooPlayer: DbTabooPlayer = if (tabooGame.isUserCreator) DbTabooPlayer.CREATOR else DbTabooPlayer.JOINER

    private val programUserStateDocRef: DocumentReference
    private val lessonUserStateDocRef: DocumentReference

    private var foundCards: Set<String>
    private var notFoundCards: Set<String>

    init {
        foundCards = lessonUserState.cardUidToState.filter { it.value.status == DbTabooCardUserStatus.Found }.map { it.key }.toSet()
        notFoundCards = lessonUserState.cardUidToState.filter { it.value.status == DbTabooCardUserStatus.NotFound }.map { it.key }.toSet()

        programUserStateDocRef = DbTabooProgramUserState.buildDocRef(db, userGameInfo.userUid, userGameInfo.userProgramUid)
        lessonUserStateDocRef = DbTabooLessonUserState.buildDocRefFromProgramUserState(programUserStateDocRef, userGameInfo.tabooLessonUserState.uid)
        tabooGame.addOnStateChangedListener(this::onStateChanged)
    }

    private fun onStateChanged(cardStateUpdate: TabooGame.State){

        val cardsFoundThisGame = cardStateUpdate.cardsPlayed.filter { it.cardPlayed.talker == tabooPlayer && it.cardPlayed.action == DbTabooCardAction.FOUND }.map { it.cardPlayed.cardUid }.toSet()
        val cardsNotFoundThisGame = cardStateUpdate.cardsPlayed.filter { it.cardPlayed.talker == tabooPlayer && it.cardPlayed.action != DbTabooCardAction.FOUND }.map { it.cardPlayed.cardUid }.toSet()

        val cardsNewlyFound = cardsFoundThisGame - foundCards
        val cardsNewlyNotFound = cardsNotFoundThisGame - notFoundCards - foundCards // once found, a card cannot go back to passed status

        if (cardsNewlyFound.isNotEmpty() || cardsNewlyNotFound.isNotEmpty()){

            foundCards = cardsNewlyFound + foundCards
            notFoundCards = cardsNewlyNotFound + notFoundCards

            MainScope().launch {
                executeBatch { batch ->

                    cardsNewlyFound.forEach {
                        batch.update(lessonUserStateDocRef, FieldPath.of(DbTabooLessonUserState.FIELD_CARD_UID_TO_STATE, it, DbTabooCardUserState.FIELD_CARD_STATUS), DbTabooCardUserStatus.Found)
                    }

                    cardsNewlyNotFound.forEach {
                        batch.update(lessonUserStateDocRef, FieldPath.of(DbTabooLessonUserState.FIELD_CARD_UID_TO_STATE, it, DbTabooCardUserState.FIELD_CARD_STATUS), DbTabooCardUserStatus.NotFound)
                    }


                    if ( cardsNewlyFound.isNotEmpty()) {
                        batch.update(
                            programUserStateDocRef,
                            FieldPath.of(
                                DbTabooProgramUserState.FIELD_LEVEL_UID_TO_STATE,
                                userGameInfo.levelUid,
                                DbTabooLevelUserState.FIELD_LESSON_UID_TO_STATE_SUMMARY,
                                userGameInfo.tabooLessonUserState.lessonUid,
                                DbTabooLessonUserStateSummary.FIELD_NB_CARDS_COMPLETED
                            ),
                            foundCards.size
                        )
                    }
                }
            }
        }
    }
}