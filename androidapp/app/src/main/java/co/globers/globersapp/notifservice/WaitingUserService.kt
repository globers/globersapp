package co.globers.globersapp.notifservice

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.os.Parcelable
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import co.globers.globersapp.R
import co.globers.globersapp.firestore.entities.DbWaitingUser
import co.globers.globersapp.firestore.entities.DbWaitingUserStatus
import co.globers.globersapp.firestore.executeSet
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.parcel.Parcelize
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch


class StopWaitingUserBroadcastReceiver: BroadcastReceiver() {

    companion object{
        const val LOG_TAG = "StopWaitingUserBR"
    }

    override fun onReceive(context: Context, intent: Intent) {

        Log.d(LOG_TAG, "StopWaitingUserBroadcastReceiver:onReceive")

        val stopIntent = Intent(context, WaitingUserService::class.java)
        stopIntent.putExtra(WaitingUserService.EXTRA_ARGS, WaitingUserService.Args(WaitingUserService.ArgsType.STOP))
        context.startService(stopIntent)
    }
}



class WaitingUserService: Service() {

    enum class ArgsType{
        START,
        STOP
    }

    @Parcelize
    data class Args(
        val argsType: ArgsType
    ) : Parcelable

    companion object {
        const val EXTRA_ARGS = "WAITING_USER_SESSION_SERVICE_EXTRA_ARGS"
        const val WAITING_PEER_CHANEL_ID = "WAITING_PEER_CHANEL_ID"
        const val WAITING_PEER_CHANEL_READABLE = "Waiting for another player"
        const val WAITING_PEER_NOTIFICATION_ID = 7798
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {

        createChannel()

        val extra = intent.getParcelableExtra<Args>(EXTRA_ARGS)!!

        return when(extra.argsType) {
            ArgsType.STOP -> {
                val db = FirebaseFirestore.getInstance()
                val userUid = FirebaseAuth.getInstance().uid
                if (userUid != null) {
                    MainScope().launch {
                        executeSet(
                            db.collection(DbWaitingUser.COLLECTION_NAME).document(userUid),
                            DbWaitingUser(userUid, DbWaitingUserStatus.NOT_WAITING)
                        )
                        stopForeground(true)
                        stopSelf()
                    }
                }

                START_REDELIVER_INTENT
            }
            ArgsType.START -> {

                val notification = NotificationCompat.Builder(this, WAITING_PEER_CHANEL_ID)
                    .setContentTitle("Waiting for another player...")
                    .setSmallIcon(R.drawable.notif_globers_icon) // icon on the bar on the screen
                    .setColor(ContextCompat.getColor(this, R.color.myColorPrimary))
                    .addAction(
                        android.R.drawable.ic_menu_close_clear_cancel,
                        "Stop waiting",
                        stopWaitingIntent()
                    )
                    .build()

                val userUid = FirebaseAuth.getInstance().uid
                if (userUid != null) {
                    startForeground(WAITING_PEER_NOTIFICATION_ID, notification)

                    val db = FirebaseFirestore.getInstance()
                    MainScope().launch {
                        executeSet(
                            db.collection(DbWaitingUser.COLLECTION_NAME).document(userUid),
                            DbWaitingUser(userUid, DbWaitingUserStatus.WAITING)
                        )
                    }

                }

                START_STICKY
            }
        }

    }

    private fun stopWaitingIntent(): PendingIntent{

        val intent = Intent(this, StopWaitingUserBroadcastReceiver::class.java)
        return PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT)
    }

    private fun createChannel(){

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val channel = NotificationChannel(WAITING_PEER_CHANEL_ID, WAITING_PEER_CHANEL_READABLE, NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }
    }


    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

}

