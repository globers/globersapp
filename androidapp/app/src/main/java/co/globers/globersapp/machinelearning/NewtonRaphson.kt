package co.globers.globersapp.machinelearning

import android.util.Log

// convergence issue: https://pdfs.semanticscholar.org/4f17/1322108dff719da6aa0d354d5f73c9c474de.pdf

sealed class NewtonRaphsonResult{

    data class Ok(val vector: Vector): NewtonRaphsonResult()
    object NonConvergence : NewtonRaphsonResult()
}

fun newtonRaphson(
    betaInit: Vector,
    fnGradient: (Vector) -> Vector,
    fnHessian: (Vector) -> Matrix
): NewtonRaphsonResult {

    var betaCurrent = betaInit
    repeat(50) {
        val newBeta = betaCurrent - fnHessian(betaCurrent).inverse22() * fnGradient(betaCurrent)
        Log.d("newtonRaphson", "newBeta: ${newBeta.values.joinToString { it.toString() }}")
        val error = (newBeta - betaCurrent).norm()
        if (error < 1e-2){
            return NewtonRaphsonResult.Ok(newBeta)
        }
        betaCurrent = newBeta
    }

    return NewtonRaphsonResult.NonConvergence
}