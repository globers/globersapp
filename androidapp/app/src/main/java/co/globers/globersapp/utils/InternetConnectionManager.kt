package co.globers.globersapp.utils

import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer

class InternetConnectionManager(private val activity: AppCompatActivity, private val quitActivity: Boolean){

    private var noInternetAlertDialog: AlertDialog? = null

    init {

        val connectionStateMonitor = ConnectionStateMonitor(activity)
        connectionStateMonitor.observe(activity, Observer { connected ->

            noInternetAlertDialog = if (connected){
                noInternetAlertDialog?.dismiss()
                null
            } else {
                if (quitActivity){
                    AlertDialog.Builder(activity)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Internet connection lost")
                        .setCancelable(false)
                        .setMessage("You are offline. Please connect to the Internet and restart")
                        .setPositiveButton("Quit"){_, _ -> activity.finish()}
                        .show()
                } else {
                    AlertDialog.Builder(activity)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("No Internet connection")
                        .setCancelable(false)
                        .setMessage("You are offline. Please connect to the Internet")
                        .show()
                }
            }

        })

    }

}