package co.globers.globersapp.firestore.entities

import com.google.firebase.firestore.ServerTimestamp
import java.util.*

enum class DbFriendRequestReply {
    DENY,
    ACCEPT
}

enum class DbFriendRequestStatus {
    PENDING,
    REPLIED,
    REPLIED_ACKNOWLEDGED,
    CANCELED_BY_SENDER,
}


data class DbFriendRequest(
    val uid: String,
    val senderUserUid: String,
    val receiverUserUid: String,
    val reply: DbFriendRequestReply? = null, // not null if repliedByReceiver true
    val status: DbFriendRequestStatus = DbFriendRequestStatus.PENDING,
    @ServerTimestamp
    val creationDate: Date = Date()
){

    constructor(): this("", "", "")

    companion object {
        const val COLLECTION_NAME = "friendRequests"

        const val FIELD_SENDER_USER_UID = "senderUserUid"
        const val FIELD_RECEIVER_USER_UID = "receiverUserUid"
        const val FIELD_REPLY = "reply"
        const val FIELD_STATUS = "status"
    }
}