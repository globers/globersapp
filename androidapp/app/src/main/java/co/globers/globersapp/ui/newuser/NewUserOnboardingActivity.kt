package co.globers.globersapp.ui.newuser

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import co.globers.globersapp.R
import com.github.paolorotolo.appintro.AppIntro
import com.github.paolorotolo.appintro.AppIntroFragment
import com.github.paolorotolo.appintro.model.SliderPage


private fun makeSlide(title: String, description: String, image: Int, context: Context): AppIntroFragment {

    val sliderPage = SliderPage()
    sliderPage.title = title
    sliderPage.description = description
    sliderPage.imageDrawable = image
    sliderPage.bgColor = Color.WHITE
    sliderPage.titleColor = ContextCompat.getColor(context, R.color.myColorPrimary)
    sliderPage.descColor = Color.BLACK
    return AppIntroFragment.newInstance(sliderPage)

}

class NewUserOnboardingActivity : AppIntro() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        val slide1 = makeSlide(
            "Placement test",
            "Let's take a test to customize your learning program, it takes about 3 minutes.",
            R.drawable.image_test_screenshot_picapp_removebg,
            this
        )

        val slide2 = makeSlide(
            "Select the words you know",
            "At each step you will see a few words, select those for which you can give a definition",
            R.drawable.image_test_screenshot_picapp_removebg,
            this
        )


        val slide3 = makeSlide(
            "Start the test",
            "The test adapts to your level by getting harder (or easier) based on your answers",
            R.drawable.image_test_screenshot_picapp_removebg,
            this
        )

        addSlide(slide1)
        addSlide(slide2)
        addSlide(slide3)

        showSkipButton(false)
        setDoneText("Start Test")
        setBarColor(ContextCompat.getColor(this, R.color.myColorPrimary))


    }


    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)

        finish()
        val intent = Intent(this, NewUserActivity::class.java)
        startActivity(intent)
    }
}