package co.globers.globersapp.ui.taboo

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.globers.globersapp.firestore.entities.DbInappropriateBehaviorReport
import co.globers.globersapp.firestore.entities.DbInappropriateBehaviorType
import co.globers.globersapp.firestore.entities.DbPeerMatch
import co.globers.globersapp.firestore.executeSet
import co.globers.globersapp.utils.NonNullLiveData
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.launch

class TabooReportUserViewModel : ViewModel() {

    enum class Status {
        NOT_SAVED,
        SAVE_IN_PROGRESS,
        FINISHED
    }

    data class State (val status: Status, val behaviorType: DbInappropriateBehaviorType?)

    val status: NonNullLiveData<State> = NonNullLiveData(State(Status.NOT_SAVED, null))

    private lateinit var peerMatch: DbPeerMatch
    private var isReporterCreator: Boolean? = null

    fun init(peerMatch: DbPeerMatch, isReporterCreator: Boolean){
        this.peerMatch = peerMatch
        this.isReporterCreator = isReporterCreator
    }

    fun setBehaviorType(type: DbInappropriateBehaviorType?){
        if ( status.value.status == Status.NOT_SAVED){
            status.value = State(Status.NOT_SAVED, type)
        }
    }


    fun save(message: String) {

        val state = status.value
        if (state.status == Status.NOT_SAVED && state.behaviorType != null){

            status.value = state.copy(status = Status.SAVE_IN_PROGRESS)

            val db = FirebaseFirestore.getInstance()

            val reporterUserUid = if (isReporterCreator!!) peerMatch.creatorGameInfo.userUid else peerMatch.joinerGameInfo!!.userUid
            val reportedUserUid = if (isReporterCreator!!) peerMatch.joinerGameInfo!!.userUid else peerMatch.creatorGameInfo.userUid

            viewModelScope.launch {

                val docRef = db.collection(DbInappropriateBehaviorReport.COLLECTION_NAME).document()
                val report = DbInappropriateBehaviorReport(reporterUserUid, reportedUserUid, peerMatch.uid, state.behaviorType, message)

                executeSet(docRef, report)
                status.value = state.copy(status = Status.FINISHED)
            }

        }

    }
}
