package co.globers.globersapp.ui.taboo

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.telephony.TelephonyManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import co.globers.globersapp.R
import co.globers.globersapp.notifservice.WaitingUserService
import co.globers.globersapp.ui.commoncomponents.rotateAnimation
import co.globers.globersapp.utils.Screen
import co.globers.globersapp.utils.Tracking
import co.globers.globersapp.utils.localeToEmoji
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.taboo_peer_matching_fragment.*
import java.util.*

class TabooPeerMatchingFragment : Fragment(){

    companion object {
        fun newInstance() = TabooPeerMatchingFragment()
        private const val LOG_TAG = "PeerMatchingFrag"
    }

    private lateinit var activityViewModel: TabooViewModel
    private lateinit var viewModel: TabooPeerMatchingViewModel
    private lateinit var tracking: Tracking

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.taboo_peer_matching_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        tracking = Tracking(this, Screen.TabooPeerMatching)
        activityViewModel = ViewModelProvider(requireActivity()).get(TabooViewModel::class.java)
        viewModel = ViewModelProvider(this).get(TabooPeerMatchingViewModel::class.java)


        val tm = requireActivity().getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

        // TODO Probleme: difference ISO code pour COUNTRY vs LANGUAGE ?
        val countryIsoCode = if(tm.networkCountryIso.isNotBlank()) Locale("", tm.networkCountryIso).isO3Country else null

        val userUid = FirebaseAuth.getInstance().currentUser!!.uid
        viewModel.init(activityViewModel.tabooActivityArgs, userUid, countryIsoCode)
        viewModel.userGameInfoLiveData.observe(viewLifecycleOwner, Observer {userInfo ->

            if (userInfo != null) {
                textView_taboo_peerMatching_userName.text = userInfo.displayName
                textView_taboo_peerMatching_userLesson.text = userInfo.lessonLabel
                textView_taboo_peerMatching_userFlag.text = localeToEmoji(userInfo.userCountryIsoCode)
            }
        })


        // copy loading screen animation
        imageView_taboo_peerMatching_loading.startAnimation(rotateAnimation())


        textView_taboo_peerMatching_waitingTeammate.text = if(activityViewModel.tabooActivityArgs.teammateArgs !== null){
            "Connecting to your friend"
        } else {
            "Looking for another player..."
        }


        button_taboo_peerMatching_cancel.setOnClickListener {
            requireActivity().onBackPressed()
        }

        viewModel.state.observe(viewLifecycleOwner, Observer {

            when (it.status){
                TabooPeerMatchingViewModel.Status.NotStarted -> {}
                TabooPeerMatchingViewModel.Status.Running -> {}
                TabooPeerMatchingViewModel.Status.Joined -> {}
                TabooPeerMatchingViewModel.Status.Completed -> {activityViewModel.createAndLaunchGame(it.peerMatchingResult!!)}
                TabooPeerMatchingViewModel.Status.Failed -> {

                    val dialog = AlertDialog.Builder(requireActivity())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Teammate Disconnected")
                        .setCancelable(false)
                        .setMessage("You have been disconnected from your teammate")
                        .setNeutralButton("Ok") { _, _ -> requireActivity().finish() }
                        .create()

                    dialog.show()

                }
            }

        })

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {

            val status = viewModel.state.value.status
            if (
                (status == TabooPeerMatchingViewModel.Status.NotStarted ||
                status == TabooPeerMatchingViewModel.Status.Running) &&
                activityViewModel.tabooActivityArgs.teammateArgs == null
                    )
            {
                    AlertDialog.Builder(requireActivity())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Be notified?")
                        .setMessage("Do you want to receive a notification when another player is available?")
                        .setPositiveButton("Yes") { _, _ ->

                            val intent = Intent(requireActivity(), WaitingUserService::class.java)
                            intent.putExtra(WaitingUserService.EXTRA_ARGS, WaitingUserService.Args(WaitingUserService.ArgsType.START))
                            requireActivity().startService(intent)
                            requireActivity().finish()

                        }
                        .setNegativeButton("Quit") { _, _ ->
                            requireActivity().finish() }
                        .show()
            } else {
                AlertDialog.Builder(requireActivity())
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Quit game?")
                    .setCancelable(false)
                    .setMessage("Game is starting, are you sure you want to quit the game?")
                    .setPositiveButton("Quit") { _, _ ->
                        val statusAfterQuitClick = viewModel.state.value.status
                        if (statusAfterQuitClick != TabooPeerMatchingViewModel.Status.Completed){
                            viewModel.abortPeerMatch()
                        }
                        activityViewModel.quitGame()
                    }
                    .setNegativeButton("Cancel", null)
                    .show()
            }

        }

        manageGameInterrupted(activityViewModel, this)

        activityViewModel.state.observe(viewLifecycleOwner, Observer {
            if (it.status == TabooViewModel.Status.GameReady){
                val action = TabooPeerMatchingFragmentDirections.actionTabooPeerMatchingFragmentToTabooStartingFragment()
                findNavController().navigate(action)
            }

        })
    }

    override fun onStop() {
        super.onStop()
        val status = viewModel.state.value.status
        if (status != TabooPeerMatchingViewModel.Status.Completed){
            viewModel.abortPeerMatch()
        }
        if (activityViewModel.state.value.status == TabooViewModel.Status.GameLoading) {
            // If we stop because game is ready => state is GameReady state, but if still on GameLoading it's a non-normal case
            activityViewModel.quitGame()
        }
    }







}
