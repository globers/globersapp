package co.globers.globersapp.ui.taboo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import co.globers.globersapp.R
import co.globers.globersapp.firestore.entities.DbTabooCardAction
import co.globers.globersapp.utils.Screen
import co.globers.globersapp.utils.Tracking
import kotlinx.android.synthetic.main.taboo_finished_fragment.*

class TabooFinishedFragment : Fragment() {

    companion object {
        fun newInstance() = TabooFinishedFragment()
    }

    private lateinit var activityViewModel: TabooViewModel
    private lateinit var tracking: Tracking

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.taboo_finished_fragment, container, false)
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tracking = Tracking(this, Screen.TabooFinished)
        activityViewModel = ViewModelProvider(requireActivity()).get(TabooViewModel::class.java)

        val gameState = activityViewModel.state.value.gameState!!
        val nbGuessed = gameState.cardsPlayed.count { it.cardPlayed.action == DbTabooCardAction.FOUND }
        textView_tabooFinished_nbCardsFound.text = "$nbGuessed Cards Found"


        val hangupButton = button_tabooFinishedFragment_hangup
        val homeButton = button_tabooFinishedFragment_home


        homeButton.visibility = View.INVISIBLE

        hangupButton.setOnClickListener {
            activityViewModel.hangup()

        }

        activityViewModel.state.observe(viewLifecycleOwner, Observer {
            if (it.status == TabooViewModel.Status.FinishedHangup){
                val action = TabooFinishedFragmentDirections.actionTabooFinishedFragmentToTabooTeammateFeedbackFragment()
                findNavController().navigate(action)
            }

            if (it.status == TabooViewModel.Status.FinishedHangup || it.status == TabooViewModel.Status.GameCompletedFeedbackFinished) {
                hangupButton.visibility = View.INVISIBLE
                homeButton.visibility = View.VISIBLE
            }
        })

        homeButton.setOnClickListener {
            requireActivity().finish()
        }

        recyclerView_tabooFinished_cardsList.adapter = TabooFinishedCardsRecyclerViewAdapter(gameState.cardsPlayed)
        recyclerView_tabooFinished_cardsList.layoutManager = LinearLayoutManager(activity)

    }


}
