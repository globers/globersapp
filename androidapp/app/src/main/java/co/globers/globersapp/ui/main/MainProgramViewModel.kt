package co.globers.globersapp.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.globers.globersapp.firestore.entities.DbUser
import co.globers.globersapp.firestore.entities.DbUserState
import co.globers.globersapp.firestore.executeUpdate
import co.globers.globersapp.services.TabooProgram
import co.globers.globersapp.services.TabooProgramService
import co.globers.globersapp.services.UserDataObservables
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldPath
import com.google.firebase.firestore.FirebaseFirestore
import io.reactivex.BackpressureStrategy
import kotlinx.coroutines.launch


class MainProgramViewModel : ViewModel() {

    private val userDataObservables = UserDataObservables()
    val program: LiveData<TabooProgram>
    val user: LiveData<DbUser> = LiveDataReactiveStreams
        .fromPublisher(userDataObservables.getUserObservable().toFlowable(BackpressureStrategy.LATEST))

    companion object {
        const val LOG_TAG = "MainProgramVM"
    }

    init {

        val programObs = TabooProgramService()
            .getTabooProgramObservable()

        program = LiveDataReactiveStreams.fromPublisher(programObs.toFlowable(BackpressureStrategy.LATEST))
    }


    fun saveTabooInstructionsAlreadyDisplayed() {

        val userUid = FirebaseAuth.getInstance().uid!!
        val db: FirebaseFirestore = FirebaseFirestore.getInstance()
        val userDocRef = db.collection(DbUser.COLLECTION_NAME).document(userUid)

        viewModelScope.launch {
            executeUpdate(
                userDocRef,
                FieldPath.of(
                    DbUser.FIELD_USER_STATE,
                    DbUserState.FIELD_TABOO_INSTRUCTIONS_ALREADY_DISPLAYED
                ),
                true
            )
        }

    }

}