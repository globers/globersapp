package co.globers.globersapp.ui.admin

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.globers.globersapp.AdminParams
import co.globers.globersapp.firestore.Nullable
import co.globers.globersapp.firestore.entities.DbAdminUser
import co.globers.globersapp.firestore.executeUpdate
import co.globers.globersapp.services.UserDataObservables
import co.globers.globersapp.utils.NonNullLiveData
import com.google.firebase.firestore.FieldPath
import com.google.firebase.firestore.FirebaseFirestore
import io.reactivex.BackpressureStrategy
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AdminViewModel : ViewModel() {

    val adminParams = NonNullLiveData(AdminParams())
    val db = FirebaseFirestore.getInstance()

    val adminUserLiveData: LiveData<Nullable<DbAdminUser>>

    init {

        val userDataObservables = UserDataObservables()

        adminUserLiveData = LiveDataReactiveStreams.fromPublisher(
            userDataObservables.getAdminUserObservable().toFlowable(BackpressureStrategy.LATEST)
        )

    }

    fun setNotifyWaitingPeerMatch(notifyWaitingPeerMatch: Boolean) {

        adminUserLiveData.value?.value?.let { adminUser ->

            viewModelScope.launch(Dispatchers.IO) {
                executeUpdate(
                    db.collection(DbAdminUser.COLLECTION_NAME).document(adminUser.userUid),
                    FieldPath.of(DbAdminUser.FIELD_NOTIFY_ON_WAITING_PEER_MATCHES),
                    notifyWaitingPeerMatch
                )
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
    }

}
