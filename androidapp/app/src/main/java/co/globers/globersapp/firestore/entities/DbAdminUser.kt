package co.globers.globersapp.firestore.entities

data class DbAdminUser( /*uid is userUid*/
    val userUid: String, /* user Uid*/
    // we create on console a adminUser with only userUid field, so all fields except userUid should be nullable
    val notifyOnWaitingPeerMatches: Boolean? /* warning: keep in sync with cloud function*/
){
    constructor(): this("", false)

    companion object {
        const val COLLECTION_NAME = "adminUsers"
        const val FIELD_NOTIFY_ON_WAITING_PEER_MATCHES = "notifyOnWaitingPeerMatches"
    }

}