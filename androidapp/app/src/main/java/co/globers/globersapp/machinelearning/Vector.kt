package co.globers.globersapp.machinelearning

import kotlin.math.sqrt

data class Vector(val values: DoubleArray){

    constructor(values: List<Double>) : this(values.toDoubleArray())
    operator fun minus(b: Vector) =
        Vector(values.zip(b.values).map { it.first - it.second })
    operator fun times(b: Vector) =
        Vector(values.zip(b.values).map { it.first * it.second })
    operator fun times(scalar: Double) = Vector(values.map { it * scalar })
    operator fun unaryMinus() = Vector(values.map { -it })
    fun scalar(b: Vector) = this.values.zip(b.values).sumByDouble { it.first * it.second }
    operator fun get(i: Int) = values[i]
    fun dim() = values.size
    fun norm() = sqrt(values.sumByDouble { it*it })
}