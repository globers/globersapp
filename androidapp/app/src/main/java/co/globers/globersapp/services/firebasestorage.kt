package co.globers.globersapp.services

import android.content.Context
import android.net.Uri
import android.util.Log
import android.widget.ImageView
import com.google.firebase.storage.StorageReference
import com.bumptech.glide.Glide
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
import com.firebase.ui.storage.images.FirebaseImageLoader
import com.google.firebase.storage.FirebaseStorage
import java.io.File
import java.io.InputStream
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


// https://github.com/firebase/FirebaseUI-Android/blob/master/storage/README.md
@GlideModule
class MyAppGlideModule : AppGlideModule() {

    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
        // Register FirebaseImageLoader to handle StorageReference
        registry.append(
            StorageReference::class.java, InputStream::class.java,
            FirebaseImageLoader.Factory()
        )
    }
}


class FirebaseStorageLoader {

    companion object {

        private val storage = FirebaseStorage.getInstance()

        fun loadImageViewFromGoogleStorageUrl(context: Context, imageView: ImageView, googleStorageUrl: String){
            Glide.with(context).load(storage.getReferenceFromUrl(googleStorageUrl)).into(imageView)
        }

    }
}

class FirebaseStorageUploader {

    companion object {

        private const val LOG_TAG = "StorageUploader"
        private val storage = FirebaseStorage.getInstance()

        suspend fun upload(file: File, relativeFilePathOnStorage: String){

            return suspendCoroutine {cont ->

                val ref = storage.reference.child(relativeFilePathOnStorage)
                val fileUri = Uri.fromFile(file)
                val task = ref.putFile(fileUri)

                task.addOnSuccessListener {
                    Log.d(LOG_TAG, "success uploading ${file.absolutePath} to $relativeFilePathOnStorage")
                    cont.resume(Unit)
                }.addOnFailureListener{
                    Log.e(LOG_TAG, "exception uploading ${file.absolutePath} to $relativeFilePathOnStorage")
                    cont.resumeWithException(it)
                }

            }


        }
    }

}

