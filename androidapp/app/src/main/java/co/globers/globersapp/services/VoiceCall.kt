package co.globers.globersapp.services

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Handler
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import co.globers.globersapp.R
import co.globers.globersapp.utils.NonNullLiveData
import io.agora.rtc.Constants
import io.agora.rtc.Constants.AUDIO_RECORDING_QUALITY_MEDIUM
import io.agora.rtc.IRtcEngineEventHandler
import io.agora.rtc.RtcEngine
import java.io.File
import java.util.*


fun permissionsOk(activity: Activity): Boolean {

    return ContextCompat.checkSelfPermission(
            activity,
            Manifest.permission.RECORD_AUDIO
        ) == PackageManager.PERMISSION_GRANTED
}

fun askPermissions(fragment: Fragment, requestCode: Int){

    fragment.requestPermissions(
        arrayOf(Manifest.permission.RECORD_AUDIO),
        requestCode
    )
}


private class CallEventHandler(
    private val onCallStarted: () -> Unit,
    private val onCallEnded: () -> Unit,
    private val onCallFailed: () -> Unit
): IRtcEngineEventHandler(){

    companion object {
        const val LOG_TAG = "CallEventHandler"
    }

    override fun onUserOffline(uid: Int, reason: Int) {
        Log.i(LOG_TAG, "onUserOffline: uid: $uid, reason: $reason")
        onCallEnded()
    }

    override fun onWarning(warn: Int) {
        Log.w(LOG_TAG, "warn: $warn")
    }

    // https://docs.agora.io/en/Interactive%20Gaming/the_error_game
    override fun onError(err: Int) {
        Log.e(LOG_TAG, "err: $err")
        onCallFailed() // else I think onUserOffline will be called
    }

    override fun onUserJoined(uid: Int, elapsed: Int) {
        Log.e(LOG_TAG, "onUserJoined, uid: $uid")
        onCallStarted()
    }

    override fun onConnectionLost() {
        Log.i(LOG_TAG, "onConnectionLost")
        onCallEnded()
    }


}


interface IVoiceCall {

    data class State(val status: Status, val startDate: Date?, val endDate: Date?)// start null if not started, end null if not ended

    enum class Status {
        NotStarted,
        Starting,
        Started,
        Finished,
        Error
    }

    fun startCall(chanelName: String, isCreator: Boolean)

    fun hangup()

    val state: MutableLiveData<State>

}

class MockVoiceCall: IVoiceCall {

    override val state = NonNullLiveData(IVoiceCall.State(IVoiceCall.Status.NotStarted, null, null))

    override fun startCall(chanelName: String, isCreator: Boolean) {
        state.value = IVoiceCall.State(IVoiceCall.Status.Starting, null, null)
        state.value = IVoiceCall.State(IVoiceCall.Status.Started, Date(), null)
    }

    override fun hangup() {
        state.value = state.value.copy(status = IVoiceCall.Status.Finished, endDate = Date())
    }


}


class VoiceCall(private val context: Context): IVoiceCall {

    companion object {
        const val LOG_TAG = "VoiceCall"
    }

    private var mRtcEngine: RtcEngine? = null
    private var eventHandler: CallEventHandler? = null
    private var chanelName: String? = null


    override val state = NonNullLiveData(IVoiceCall.State(IVoiceCall.Status.NotStarted, null, null))

    override fun startCall(
        chanelName: String,
        isCreator: Boolean){

        this.chanelName = chanelName

        eventHandler =
            CallEventHandler(this::onCallStarted, this::onCallEnded, this::onCallFailed)
        mRtcEngine = RtcEngine.create(context, context.getString(R.string.agora_app_id), eventHandler)
        mRtcEngine!!.setChannelProfile(Constants.CHANNEL_PROFILE_COMMUNICATION)

        mRtcEngine!!.joinChannel(
            null, // only for security
            chanelName,
            "",
            if(isCreator) 1 else 2 // // if you do not specify the uid, we will generate the uid for you
        )
        Log.i(LOG_TAG, "call Starting: isCreator: $isCreator, chanelName: $chanelName")
        state.value = IVoiceCall.State(IVoiceCall.Status.Starting, null, null)

        // if user is not joined 5sec after having started call...user didn't connect
        val handler = Handler()
        handler.postDelayed({
            if (state.value.status == IVoiceCall.Status.Starting){
                onCallFailed()
            }
        }, 5000)
    }


    override fun hangup(){
        Log.d(LOG_TAG, "hangup")
        leaveChannel()
        state.postValue(state.value.copy(status = IVoiceCall.Status.Finished, endDate = Date()))
    }

    private fun leaveChannel() {
        mRtcEngine?.leaveChannel()
        RtcEngine.destroy()
    }

    private fun onCallStarted(){
        Log.d(LOG_TAG, "onCallStarted")
        state.postValue(state.value.copy(status = IVoiceCall.Status.Started, startDate = Date()))
        startRecording()
    }


    // https://docs.agora.io/en/Voice/API%20Reference/java/classio_1_1agora_1_1rtc_1_1_rtc_engine.html#a44744695d723b7d18c704a57f828cddb
    private fun startRecording(){

        val recordingDir = File(context.cacheDir, "recordings")
        recordingDir.mkdir()

        val filePath = context.cacheDir.absolutePath + "/recordings/$chanelName.aac"

        Log.d(LOG_TAG, "start recording at path: $filePath")

        mRtcEngine?.startAudioRecording(
            // Local path of the recording file specified by the user, including the filename and format.
            filePath,
            32,
            AUDIO_RECORDING_QUALITY_MEDIUM
        )
    }

    private fun onCallEnded(){
        Log.d(LOG_TAG, "onCallEnded")
        leaveChannel()
        state.postValue(state.value.copy(status = IVoiceCall.Status.Finished, endDate = Date()))
    }


    private fun onCallFailed(){
        Log.d(LOG_TAG, "onCallFailed")
        leaveChannel()
        state.postValue(state.value.copy(status = IVoiceCall.Status.Error))
    }

}



