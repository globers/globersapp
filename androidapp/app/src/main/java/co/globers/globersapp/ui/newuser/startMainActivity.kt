package co.globers.globersapp.ui.newuser

import android.content.Intent
import androidx.fragment.app.Fragment
import co.globers.globersapp.ui.main.MainActivity
import com.google.firebase.iid.FirebaseInstanceId

fun startMainActivity(fragment: Fragment){
    fragment.requireActivity().finish()
    val intent = Intent(
        fragment.activity,
        MainActivity::class.java
    )
    // to not reuse MainActivity in the stack,because visible fragment is currently GetStarted, and we want this only
    // if we press back button. But if finished, we want splash screen then program directly without "blinking" get started fragment
    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
    fragment.startActivity(intent)
}