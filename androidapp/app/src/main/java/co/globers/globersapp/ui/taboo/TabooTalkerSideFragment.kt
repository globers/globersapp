package co.globers.globersapp.ui.taboo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import co.globers.globersapp.R
import co.globers.globersapp.services.TabooGame
import co.globers.globersapp.utils.Screen
import co.globers.globersapp.utils.Tracking
import kotlinx.android.synthetic.main.taboo_card_view.view.*
import kotlinx.android.synthetic.main.taboo_talker_side_fragment.*


class TabooTalkerSideFragment : Fragment() {

    companion object {
        fun newInstance() = TabooTalkerSideFragment()
    }

    private lateinit var viewModel: TabooViewModel
    private lateinit var tracking: Tracking

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.taboo_talker_side_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        tracking = Tracking(this, Screen.TabooTalkerSide)
        viewModel = ViewModelProvider(requireActivity()).get(TabooViewModel::class.java)

        val layoutCard = layout_tabooStartedFragment_card

        val tabooWordsRecyclerView = layoutCard.recyclerView_taboo_card_tabooWords
        val wordToGuessTexView = layoutCard.textView_taboo_card_wordToGuess

        val passButton = button_tabooStartedFragment_pass

        passButton.setOnClickListener {
            viewModel.pass()
        }

        floatingActionButton_taboo_talkerSide_help.setOnClickListener {
            buildTalkerRulesSnackbar(requireView()).show()
        }

        viewModel.state.observe( viewLifecycleOwner, Observer {

            if (it.gameState!!.status == TabooGame.Status.RunningTalker){

                // if the user is not the current talker, then it should not see the card, even if fragment is still visible
                val card = it.gameState.currentCard!!
                val wordToGuess = card.wordToGuess
                val tabooWords = card.forbiddenWords

                tabooWordsRecyclerView.adapter = TabooCardForbiddenWordsRecyclerViewAdapter(tabooWords)
                tabooWordsRecyclerView.layoutManager = LinearLayoutManager(requireActivity())
                wordToGuessTexView.text = wordToGuess
            }
        })




    }

}
