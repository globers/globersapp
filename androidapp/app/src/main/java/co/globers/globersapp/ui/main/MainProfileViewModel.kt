package co.globers.globersapp.ui.main


import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.ViewModel
import co.globers.globersapp.firestore.entities.DbUserPublicProfile
import co.globers.globersapp.services.UserDataObservables
import io.reactivex.BackpressureStrategy

class MainProfileViewModel : ViewModel() {

    companion object {
        const val LOG_TAG = "MainVM"
    }

    val userPublicProfile: LiveData<DbUserPublicProfile>
    val isAdmin: LiveData<Boolean>

    init{

        val userDataObservables = UserDataObservables()
        userPublicProfile = LiveDataReactiveStreams.fromPublisher(userDataObservables.getProfileObservable().toFlowable(BackpressureStrategy.LATEST))

        val isAdminObs = userDataObservables.getAdminUserObservable().map { it.value != null }
        isAdmin = LiveDataReactiveStreams.fromPublisher(isAdminObs.toFlowable(BackpressureStrategy.LATEST))
    }





}

