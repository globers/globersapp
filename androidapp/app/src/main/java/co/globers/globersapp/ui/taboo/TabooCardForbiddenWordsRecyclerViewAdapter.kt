package co.globers.globersapp.ui.taboo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.globers.globersapp.R

class TabooCardForbiddenWordsRecyclerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val wordTextView: TextView = view.findViewById(R.id.textView_taboo_talkerSide_tabooWord)
}


class TabooCardForbiddenWordsRecyclerViewAdapter(
    val words: List<String>
) : RecyclerView.Adapter<TabooCardForbiddenWordsRecyclerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TabooCardForbiddenWordsRecyclerViewHolder {
        val layoutView = R.layout.taboo_talker_side_tabooword_view
        val v = LayoutInflater.from(parent.context).inflate(layoutView, parent, false)
        return TabooCardForbiddenWordsRecyclerViewHolder(v)
    }

    override fun getItemCount() = words.size

    override fun onBindViewHolder(holder: TabooCardForbiddenWordsRecyclerViewHolder, position: Int) {
        holder.wordTextView.text = words[position]
    }

}