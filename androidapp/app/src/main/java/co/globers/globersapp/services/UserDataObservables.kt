package co.globers.globersapp.services

import co.globers.globersapp.firestore.Nullable
import co.globers.globersapp.firestore.entities.DbAdminUser
import co.globers.globersapp.firestore.entities.DbTabooProgramUserState
import co.globers.globersapp.firestore.entities.DbUser
import co.globers.globersapp.firestore.entities.DbUserPublicProfile
import co.globers.globersapp.firestore.entities.tokeepinsync.DbTabooProgram
import co.globers.globersapp.firestore.listen
import co.globers.globersapp.firestore.listenNullable
import co.globers.globersapp.firestore.listenOnce
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import io.reactivex.Observable


class UserDataObservables {

    private val db = FirebaseFirestore.getInstance()

    fun getAuthenticatedUserUidObservable(): Observable<String> = Observable.create<String> { emitter ->

        val authInstance = FirebaseAuth.getInstance()
        val authListener = FirebaseAuth.AuthStateListener { auth ->
            val firebaseUser = auth.currentUser
            if (firebaseUser != null && !firebaseUser.isAnonymous){
                emitter.onNext(firebaseUser.uid)
                emitter.onComplete()
            }
        }

        authInstance.addAuthStateListener(authListener)
        emitter.setCancellable { authInstance.removeAuthStateListener(authListener)}
    }

    fun getAdminUserObservable(): Observable<Nullable<DbAdminUser>> = getAuthenticatedUserUidObservable().flatMap { userUid ->
        listenNullable<DbAdminUser>(db.collection(DbAdminUser.COLLECTION_NAME).document(userUid))
    }


    fun getUserObservable(): Observable<DbUser> = getAuthenticatedUserUidObservable().flatMap { userUid ->
        val nullableUser = listenNullable<DbUser>(
            db.collection(DbUser.COLLECTION_NAME).document(userUid)
        )

        // when user is just created (but dbUser not created yet), user is null
        nullableUser.filter { it.value != null }.map { it.value }
    }

    fun getProfileObservable(): Observable<DbUserPublicProfile> =
        getUserObservable().flatMap { user: DbUser ->
            listen<DbUserPublicProfile>(
                db.collection(DbUserPublicProfile.COLLECTION_NAME).document(
                    user.uid
                )
            )
        }

    fun getProgramUserStateObservable(): Observable<DbTabooProgramUserState> =
        getUserObservable().take(1).flatMap { user: DbUser ->
            val userProgramUid = user.learnedLanguages.first().userProgramUid
            val userProgramDocRef =
                DbTabooProgramUserState.buildDocRef(db, user.uid, userProgramUid)

            val programUserStateObservable = listen<DbTabooProgramUserState>(userProgramDocRef)
            programUserStateObservable
        }

    fun getProgramObservable(): Observable<DbTabooProgram> =
        getProgramUserStateObservable().take(1).flatMap { programUserState ->
            val programDocRef =
                db.collection(DbTabooProgram.COLLECTION_NAME).document(programUserState.programUid)

            val program = listenOnce<DbTabooProgram>(programDocRef)
            program
        }
}