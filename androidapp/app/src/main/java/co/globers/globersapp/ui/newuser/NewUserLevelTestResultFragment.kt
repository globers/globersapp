package co.globers.globersapp.ui.newuser

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import co.globers.globersapp.R
import co.globers.globersapp.utils.Event
import co.globers.globersapp.utils.Screen
import co.globers.globersapp.utils.Tracking
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.new_user_level_test_result_fragment.*

class NewUserLevelTestResultFragment : Fragment() {

    companion object {
        fun newInstance() = NewUserLevelTestResultFragment()
        const val RC_SIGN_IN = 67
        private const val LOG_TAG = "newUserLevelTestResFrg"
    }

    private lateinit var activityViewModel: NewUserViewModel
    private lateinit var tracking: Tracking

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.new_user_level_test_result_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tracking = Tracking(this, Screen.NewUserLevelTestResult)

        activityViewModel = ViewModelProvider(requireActivity()).get(NewUserViewModel::class.java)
        activityViewModel.currentUserCreationStep.observe(viewLifecycleOwner, Observer {

            if (activityViewModel.currentUserCreationStep.value ==
                NewUserViewModel.UserCreationStep.WaitingForUserAndProgramCreation){
                val action = NewUserLevelTestResultFragmentDirections.actionNewUserLevelTestResultFragmentToNewUserWaitingProgramCreationFragment()
                findNavController().navigate(action)
            }
        })

        val args = NewUserLevelTestResultFragmentArgs.fromBundle(requireArguments())

        val estimatedNbWordsKnown = args.nbWordsKnown
        val startLevelLabel = args.startLevelLabel

        textView_newUser_levelTestResult_nbWords.text = "You know about ${estimatedNbWordsKnown} English words and jump directly to"
        textView_newUser_levelTestResult_level.text = startLevelLabel

        button_newUser_createProfile.setOnClickListener {


            val providers = arrayListOf(
                AuthUI.IdpConfig.FacebookBuilder().build(),
                AuthUI.IdpConfig.GoogleBuilder().build(),
                AuthUI.IdpConfig.EmailBuilder().build())

            startActivityForResult(
                AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .enableAnonymousUsersAutoUpgrade()
                    .setAvailableProviders(providers)
                    .setTosAndPrivacyPolicyUrls("https://globers.github.io/tos_globersapp",
                        "https://globers.github.io/privacy_policy_globersapp")
                    .setLogo(R.drawable.globers_logo)      // Set logo drawable
                    .setTheme(R.style.AppThemeFirebaseAuth) // https://stackoverflow.com/questions/39136738/unable-to-hide-the-title-bar-of-authorization-activity-using-firebaseui
                    .build(), RC_SIGN_IN)

        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        Log.d(LOG_TAG, "onActivityResult, requestCode: $requestCode")

        if (requestCode == RC_SIGN_IN){

            val response = IdpResponse.fromResultIntent(data)

            if (resultCode == Activity.RESULT_OK) {
                // Successfully signed in
                val firebaseUser = FirebaseAuth.getInstance().currentUser!!

                activityViewModel.createUser(firebaseUser.uid, firebaseUser.displayName!!, firebaseUser.photoUrl.toString())
            } else {

                if (response != null) {
                    Log.e(LOG_TAG, response.error.toString())
                    // Sign in failed. If response is null the user canceled the
                    // sign-in flow using the back button. Otherwise check
                    // response.getError().getErrorCode() and handle the error.
                    // ...
                }
            }

        }
    }

}

