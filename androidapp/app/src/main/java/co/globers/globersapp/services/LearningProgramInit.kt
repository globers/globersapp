package co.globers.globersapp.services

import co.globers.globersapp.firestore.entities.DbLanguageIsoCode
import co.globers.globersapp.firestore.entities.DbLogitParams
import co.globers.globersapp.firestore.entities.tokeepinsync.DbProgramStatus
import co.globers.globersapp.firestore.entities.tokeepinsync.DbTabooProgram
import co.globers.globersapp.firestore.executeQuery
import co.globers.globersapp.machinelearning.Logit1DCoeffs
import co.globers.globersapp.machinelearning.computeX
import com.google.firebase.firestore.FirebaseFirestore
import kotlin.math.max
import kotlin.math.min


suspend fun getProgram(language: DbLanguageIsoCode): DbTabooProgram {

    val db: FirebaseFirestore = FirebaseFirestore.getInstance()

    val query = db.collection(DbTabooProgram.COLLECTION_NAME)
        .whereEqualTo(DbTabooProgram.FIELD_STATUS, DbProgramStatus.Active)
        .whereEqualTo(DbTabooProgram.FIELD_LANGUAGE_ISO_CODE, language)
        .limit(1)

    return executeQuery<DbTabooProgram>(query).first()
}

fun getStartingLevelUid(program: DbTabooProgram, logitLogFreqInverseParams: DbLogitParams): String {

    // Return hardest level for which you know more than 90% of the words
    val logit1DCoeffs = Logit1DCoeffs(logitLogFreqInverseParams.intercept, logitLogFreqInverseParams.coeff)

    val lnFrequencyInverseAt50percent = computeX(logit1DCoeffs, 0.50)

    // 0.9 x freq for 0.5...because it seems to work OK. We take 0.5 ref because it's the most stable estimation
    // then coeff 0.9 to get an easier level
    val halfFrequencyInverse = lnFrequencyInverseAt50percent * 0.9

    val level = program.levels.indexOfLast { level -> level.averageLnFrequencyInverse < halfFrequencyInverse }

    val minLevelIndex = 1 // to not start at "level 1": words are not interesting and result is depressing
    val maxLevelIndex = program.levels.size - 10 // to have still level to do even if you are very strong

    val startLevel = max(minLevelIndex, min(level, maxLevelIndex))

    return program.levels[startLevel].uid
}

