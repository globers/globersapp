package co.globers.globersapp.ui.taboo

// https://www.kotlindevelopment.com/kotlin_android_extensions_eliminate_findviewbyid/

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import co.globers.globersapp.R
import co.globers.globersapp.utils.Screen
import co.globers.globersapp.utils.Tracking
import kotlinx.android.synthetic.main.taboo_teammate_feedback_fragment.*

class TabooTeammateFeedbackFragment : Fragment() {

    companion object {
        fun newInstance() = TabooTeammateFeedbackFragment()
        private val LOG_TAG = "TabooTeammateFeedbackFg"
    }

    private lateinit var viewModel: TabooTeammateFeedbackViewModel
    private lateinit var tracking: Tracking

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.taboo_teammate_feedback_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        tracking = Tracking(this, Screen.TabooTeammateFeedback)
        viewModel = ViewModelProvider(this).get(TabooTeammateFeedbackViewModel::class.java)
        val activityViewModel = ViewModelProvider(requireActivity()).get(TabooViewModel::class.java)


        button_tabooTeammateFeedback_Close.setOnClickListener {
            Log.d(LOG_TAG, "click on button_tabooTeammateFeedback_Close")

            if (activityViewModel.isTeammateInContacts()){
                activityViewModel.setFeedbackGivenOrDiscarded()
            } else {

                val teammateName = activityViewModel.teammateGameInfo.displayName
                AlertDialog.Builder(requireActivity())
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Add '$teammateName' to your friends?")
                    .setMessage("Do you want to add '$teammateName' to your friends? You can call your friends directly to play")
                    .setCancelable(false)
                    .setPositiveButton("Add") { _, _ ->
                        activityViewModel.setFeedbackGivenOrDiscarded()
                        activityViewModel.sendFriendRequestToTeammate()
                    }
                    .setNegativeButton("No") { _, _ ->
                        activityViewModel.setFeedbackGivenOrDiscarded()
                    }
                    .show()
            }

        }

        button_tabooTeammateFeedback_GiveAward.setOnClickListener {
            val action = TabooTeammateFeedbackFragmentDirections.actionTabooTeammateFeedbackFragmentToTabooGiveAwardFragment()
            findNavController().navigate(action)
        }

        button_tabooTeammateFeedback_Report.setOnClickListener {
            val action = TabooTeammateFeedbackFragmentDirections.actionTabooTeammateFeedbackFragmentToTabooReportUserFragment()
            findNavController().navigate(action)
        }

        // for viewLifecycleOwner reason see https://medium.com/@BladeCoder/architecture-components-pitfalls-part-1-9300dd969808
        activityViewModel.state.observe(viewLifecycleOwner, Observer {
            if (it.status == TabooViewModel.Status.GameCompletedFeedbackFinished){
                Log.d(LOG_TAG, "status updated to GameCompletedFeedbackFinished, navigate to tabooFinished")
                val action = TabooTeammateFeedbackFragmentDirections.actionTabooTeammateFeedbackFragmentToTabooFinishedFragment()
                findNavController().navigate(action)
            }
        })


    }

}
