package co.globers.globersapp.ui.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import co.globers.globersapp.R
import co.globers.globersapp.ui.main.MainFindFriendViewModel.Status.*
import co.globers.globersapp.utils.Screen
import co.globers.globersapp.utils.Tracking
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.main_find_friend_fragment.*

class MainFindFriendFragment : Fragment() {

    companion object {
        fun newInstance() = MainFindFriendFragment()
    }

    private lateinit var viewModel: MainFindFriendViewModel
    private lateinit var tracking: Tracking

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_find_friend_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tracking = Tracking(this, Screen.MainFindFriend)
        val bottomNavView = requireActivity().main_bottom_navigation_view
        bottomNavView.visibility = View.GONE

        viewModel = ViewModelProvider(this).get(MainFindFriendViewModel::class.java)


        editText_main_findFriend_userName.setOnFocusChangeListener { _, focus ->
            if (focus) {
                val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.toggleSoftInput(
                    InputMethodManager.SHOW_FORCED,
                    InputMethodManager.HIDE_IMPLICIT_ONLY
                )
            }
        }

        viewModel.status.observe(viewLifecycleOwner, Observer { status: MainFindFriendViewModel.Status ->

            button_main_findFriend_addFriend.isEnabled = status != IN_PROCESS

            when(status){
                NOT_STARTED -> {}
                IN_PROCESS -> {}
                USER_NOT_FOUND -> {

                    AlertDialog.Builder(requireActivity())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("User not found")
                        .setMessage("Please check if username is valid")
                        .setNeutralButton("OK"){ _, _ -> viewModel.reset()}
                        .show()
                }
                REQUEST_SEND -> {
                    Toast.makeText(context, "Friend request sent", Toast.LENGTH_LONG).show()
                    requireActivity().onBackPressed()
                }
            }
        })

        button_main_findFriend_addFriend.setOnClickListener {
            val username = editText_main_findFriend_userName.text.toString()
            viewModel.sendFriendRequest(username)
        }



    }



    override fun onResume() {
        super.onResume()

        editText_main_findFriend_userName.requestFocus()
    }

    override fun onPause() {
        super.onPause()
        hideKeyboard()
    }


    private fun hideKeyboard() {
        val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(requireActivity().currentFocus!!.windowToken, 0)
    }

}
