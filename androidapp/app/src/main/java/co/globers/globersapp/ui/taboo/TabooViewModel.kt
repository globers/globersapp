package co.globers.globersapp.ui.taboo

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.globers.globersapp.firestore.entities.DbLanguageIsoCode
import co.globers.globersapp.firestore.entities.DbUserGameInfo
import co.globers.globersapp.services.*
import co.globers.globersapp.services.peermatching.IPeerMatching
import co.globers.globersapp.utils.NonNullLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch


class TabooViewModel : ViewModel() {

    enum class Status {
        PeerMatching,
        GameLoading,
        GameReady,
        RunningGuesser,
        RunningTalker,
        Finished,
        FinishedHangup,
        GameCompletedFeedbackFinished,
        Disconnected,
        AbortedByUser,
        GameAbortedFeedbackFinished
    }

    enum class CallStatusRequest {
        Nothing,
        StartCall,
        Hangup
    }

    companion object {
        private const val LOG_TAG = "TabooVM"
    }

    data class State(val status: Status, val gameState: TabooGame.State?, val voiceCallState: IVoiceCall.State)


    lateinit var tabooActivityArgs: TabooActivityArgs
    lateinit var languageLearnedIsoCode: DbLanguageIsoCode

    // initialized when peerMatching and tabooGame created

    val isCreator: Boolean?
        get() = peerMatchResult?.isCreator

    val voiceCallChanelName: String?
        get() = peerMatchResult?.peerMatch?.voiceCallChanelName

    val teammateGameInfo: DbUserGameInfo
        get() = if (peerMatchResult!!.isCreator)
            peerMatchResult!!.peerMatch.joinerGameInfo!!
        else
            peerMatchResult!!.peerMatch.creatorGameInfo

    var peerMatchResult: IPeerMatching.Result? = null
    var tabooGame: TabooGame? = null

    //last taboo game et voice call status received
    private var tabooGameState: TabooGame.State? = null
    private var voiceCallState: IVoiceCall.State = IVoiceCall.State(IVoiceCall.Status.NotStarted, null, null)
    private var feedbackGivenOrDiscarded = false

    // notifier for Activity
    val callStateRequest = MutableLiveData(CallStatusRequest.Nothing)
    val state = NonNullLiveData(State(Status.PeerMatching, null, voiceCallState))
    val remainingPercentTime = MutableLiveData(100)


    fun init(args: TabooActivityArgs){
        tabooActivityArgs = args

    }


    fun createAndLaunchGame(result: IPeerMatching.Result) {

        Log.d(LOG_TAG, "createAndLaunchGame")

        val isCreator = result.isCreator
        peerMatchResult = result
        val newTabooGame = result.tabooGame
        tabooGame = newTabooGame
        val userGameInfo = if (isCreator) result.peerMatch.creatorGameInfo else result.peerMatch.joinerGameInfo!!
        languageLearnedIsoCode = userGameInfo.learnedLanguageIsoCode

        newTabooGame.registerOnPercentTimeRemainingListener { remainingPercentTime.value = it }
        TabooProgramUserStateUpdater(userGameInfo, userGameInfo.tabooLessonUserState, newTabooGame)

        newTabooGame.addOnStateChangedListener {
            Log.i(LOG_TAG, "TabooGame status changed to ${it.status}")
            tabooGameState = it
            updateState()
        }

        callStateRequest.value = CallStatusRequest.StartCall
    }

    fun guessWord() = tabooGame!!.guessWord()
    fun pass() = tabooGame!!.passCard()

    fun buzzWordGuesser() = tabooGame!!.buzzGuesserCard()

    fun setUserReady(){
        tabooGame!!.setUserReady()
    }

    fun hangup(){
        callStateRequest.value = CallStatusRequest.Hangup
    }

    override fun onCleared() {
        super.onCleared()

        quitGame()
        // NB If taboo is yet to be initialized, then it won't be aborted
        tabooGame?.removeListeners()
    }

    fun onVoiceCallStatusUpdate(state: IVoiceCall.State) {

        val currentTabooGame = tabooGame
        Log.d(LOG_TAG, "onVoiceCallStatusUpdate, state:$state, currentTabooGame is null: ${currentTabooGame == null}")
        voiceCallState = state
        if(currentTabooGame != null &&  ! currentTabooGame.isFinished()){ // game running
            currentTabooGame.notifyCallStatusChanged(state) // let game change it's state and notify ViewModel back
        } else {
            updateState()
        }
    }

    fun setFeedbackGivenOrDiscarded() {
        if (!feedbackGivenOrDiscarded) {
            feedbackGivenOrDiscarded = true
            updateState()
        }
    }


    private fun updateState(){

        Log.d(LOG_TAG, "updateState, voice: $voiceCallState, game: ${tabooGameState?.status}")
        val callState = voiceCallState
        val voiceCallStatus = callState.status
        val gameStatus = tabooGameState?.status ?: return // if tabooGameStatus not initialized, model status remains the same

        if (tabooGameState == null) return

        val status = when (gameStatus) {
            TabooGame.Status.GameLoading -> Status.GameLoading
            TabooGame.Status.GameReady -> Status.GameReady
            TabooGame.Status.RunningTalker -> Status.RunningTalker
            TabooGame.Status.RunningGuesser -> Status.RunningGuesser
            TabooGame.Status.Finished -> if (voiceCallStatus == IVoiceCall.Status.Started){
                Status.Finished
            } else {
                if (feedbackGivenOrDiscarded)Status.GameCompletedFeedbackFinished else Status.FinishedHangup
            }
            TabooGame.Status.AbortedBySelf -> if (feedbackGivenOrDiscarded) Status.GameAbortedFeedbackFinished else Status.AbortedByUser
            TabooGame.Status.AbortedByTeammate -> if (feedbackGivenOrDiscarded) Status.GameAbortedFeedbackFinished else Status.Disconnected
            TabooGame.Status.Error -> if (feedbackGivenOrDiscarded) Status.GameAbortedFeedbackFinished else Status.Disconnected
        }


        val newState = State(status, tabooGameState, callState)

        // prevent useless event firing
        if (newState != state.value) {
            Log.d(LOG_TAG, "Event change: from ${state.value} to $newState")
            state.value = newState
        }

        if ((gameStatus == TabooGame.Status.Error ||
                    gameStatus == TabooGame.Status.AbortedBySelf ||
                    gameStatus == TabooGame.Status.AbortedByTeammate)
            && voiceCallStatus == IVoiceCall.Status.Started){
            hangup()
        }

    }

    fun quitGame() {
        callStateRequest.value = CallStatusRequest.Hangup
        tabooGame?.abort() // will abort game which will hangup when done
    }


    fun sendFriendRequestToTeammate() {
        val contactsManager = ContactsManager()
        MainScope().launch(Dispatchers.IO) {
            contactsManager.sendFriendRequest(teammateGameInfo.userUid)
        }
    }

    fun isTeammateInContacts(): Boolean {
        return peerMatchResult!!.teammateContact != null
    }


}


