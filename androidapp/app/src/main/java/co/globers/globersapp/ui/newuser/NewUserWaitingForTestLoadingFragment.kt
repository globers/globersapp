package co.globers.globersapp.ui.newuser

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import co.globers.globersapp.ui.commoncomponents.ProgressFragment
import co.globers.globersapp.utils.Screen
import co.globers.globersapp.utils.Tracking

class NewUserWaitingForTestLoadingFragment : ProgressFragment(
    "Loading test"
) {

    private lateinit var tracking: Tracking

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tracking = Tracking(this, Screen.NewUserWaitingForTestLoading)

        val activityViewModel = ViewModelProvider(requireActivity()).get(NewUserViewModel::class.java)
        activityViewModel.currentUserCreationStep.observe(viewLifecycleOwner, Observer {

            if (activityViewModel.currentUserCreationStep.value == NewUserViewModel.UserCreationStep.LanguageTest){
                val action = NewUserWaitingForTestLoadingFragmentDirections.actionNewUserWaitingForTestLoadingToNewUserLevelTestFragment()
                findNavController().navigate(action)
            }

        })
        activityViewModel.startTest()
    }

}
