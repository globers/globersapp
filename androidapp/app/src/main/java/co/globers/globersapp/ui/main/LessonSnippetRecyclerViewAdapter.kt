package co.globers.globersapp.ui.main

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import co.globers.globersapp.R
import co.globers.globersapp.services.*
import co.globers.globersapp.utils.BugException


data class LessonViewData(
    val lesson: TabooLesson,
    val level: TabooLevel
)


data class LevelViewData(
    val level: TabooLevel
)


class LessonRecyclerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val startLessonButton: Button = view.findViewById(R.id.button_lessonSnippetView_current)
    val nameTextView: TextView = view.findViewById(R.id.textView_lessonSnipetView_name)
    val progressBar: ProgressBar = view.findViewById(R.id.progressBar_lessonSnipetView_progress)
    val layout: ConstraintLayout = view.findViewById(R.id.constraintLayout_main_program_lessonSnippetView)
    val innerLayout: ConstraintLayout = view.findViewById(R.id.constraintLayout_main_program_lessonSnippetView_inner)
    val imageView: ImageView = view.findViewById(R.id.imageView_main_programLessonSnipet)
}

class LevelRecyclerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val nameTextView: TextView = view.findViewById(R.id.textView_main_program_levelSnippetView_name)
}


class LessonSnippetRecyclerViewAdapter(
    private val context: Context,
    program: TabooProgram,
    private val onLessonClick: (LessonViewData) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    companion object {
        private const val LEVEL_VIEW_TYPE = 0
        private const val LESSON_VIEW_TYPE = 1
    }


    data class ListItem(
        val viewType: Int,
        val levelViewData: LevelViewData?,
        val lessonViewData: LessonViewData?
    )

    private val items: List<ListItem>

    init {
        items = program.levels.flatMap { level ->
            listOf(ListItem(LEVEL_VIEW_TYPE, LevelViewData(level), null)) +
                    level.lessons.map { ListItem(LESSON_VIEW_TYPE, null, LessonViewData(it, level)) } }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when(viewType){
            LEVEL_VIEW_TYPE -> {
                val layoutView = R.layout.main_program_level_snipet_view
                val v = LayoutInflater.from(parent.context).inflate(layoutView, parent, false)
                LevelRecyclerViewHolder(v)
            }
            LESSON_VIEW_TYPE -> {
                val layoutView = R.layout.main_program_lesson_snipet_view
                val v = LayoutInflater.from(parent.context).inflate(layoutView, parent, false)
                LessonRecyclerViewHolder(v)
            }
            else -> throw BugException("view type should be level or lesson but is $viewType")
        }
    }



    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {

        when(viewHolder.itemViewType){
            LEVEL_VIEW_TYPE -> {
                val holder = viewHolder as LevelRecyclerViewHolder
                val level = items[position].levelViewData!!
                holder.nameTextView.text = level.level.label

            }

            LESSON_VIEW_TYPE -> {
                val holder = viewHolder as LessonRecyclerViewHolder
                val lessonViewData = items[position].lessonViewData!!
                val lesson = lessonViewData.lesson
                holder.nameTextView.text = lesson.label

                val percentCompletion = (100 * lesson.nbCardsFound) / lesson.nbCardsObjective
                val visible =  percentCompletion > 0
                holder.progressBar.progress = percentCompletion
                holder.progressBar.visibility = if (visible) View.VISIBLE else View.INVISIBLE

                holder.layout.setOnClickListener {
                    onLessonClick(lessonViewData)
                }
                holder.startLessonButton.setOnClickListener {
                    onLessonClick(lessonViewData)
                }

                if (lesson.status == LessonStatus.CURRENT){
                    holder.nameTextView.setTypeface(null, Typeface.BOLD)
                    holder.startLessonButton.visibility = View.VISIBLE

                }
                else {
                    holder.nameTextView.setTypeface(null, Typeface.NORMAL)
                    holder.startLessonButton.visibility = View.GONE
                }
                holder.layout.setBackgroundColor(ContextCompat.getColor(context, R.color.greyBackground))

                holder.innerLayout.setBackgroundColor(
                    if (lesson.status == LessonStatus.NOT_STARTED) {
                        ContextCompat.getColor(context, R.color.greyDisabled)
                    } else {
                        ContextCompat.getColor(context, R.color.white)
                    }
                )

                FirebaseStorageLoader.loadImageViewFromGoogleStorageUrl(context, holder.imageView, lesson.imageUrl)

                // https://stackoverflow.com/questions/25454316/how-do-i-partially-gray-out-an-image-when-pressed

                holder.imageView.setColorFilter(
                    if (lesson.status == LessonStatus.NOT_STARTED) {
                        Color.argb(150,200,200,200)
                    } else {
                        0
                    }
                )
            }
            else -> throw BugException("view type should be level or lesson but is ${viewHolder.itemViewType}")
        }

    }

    override fun getItemViewType(position: Int) = items[position].viewType
    override fun getItemCount() = items.size

    fun getCurrentLessonIndex() = items.indexOfFirst { it.viewType == LESSON_VIEW_TYPE && it.lessonViewData!!.lesson.status == LessonStatus.CURRENT }

}

