package co.globers.globersapp.ui.taboo

import android.view.View
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar

fun buildListenerRulesSnackbar(view: View): Snackbar{

    val rules = Snackbar.make(view,
        """Objective: Find the word that your friend is talking about, listen and ask questions
                
* Rule 1: You can't say the words on the screen
* Rule 2: You can't ask things like "Does it start with a 'F'?" or "how many letters?"
* Rule 3: You can only make 1 mistake""",
        Snackbar.LENGTH_INDEFINITE)

    rules.view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text).maxLines = 10
    view.setOnClickListener { rules.dismiss() }
    rules.view.setOnClickListener { rules.dismiss() }
    return rules
}

fun buildTalkerRulesSnackbar(view: View): Snackbar {
    val rules = Snackbar.make(view,

        """Objective: Explain the word so your friend can find it
                
* Rule 1: You can't say the words in orange, or any part of them
* Rule 2: You can't say things like "It starts with a 'F'" or "It has 5 letters"
* Rule 3: You can't make sounds like 'Vrooom' for the word 'Car'""",
        Snackbar.LENGTH_INDEFINITE)

    rules.view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text).maxLines = 10
    view.setOnClickListener { rules.dismiss() }
    rules.view.setOnClickListener { rules.dismiss() }
    return rules
}