package co.globers.globersapp.ui.phonecall

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import co.globers.globersapp.R
import co.globers.globersapp.services.ReceivedPhoneCallManager
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.phone_call_fragment.*


class PhoneCallFragment : Fragment() {

    companion object {
        fun newInstance() = PhoneCallFragment()
    }

    private lateinit var viewModel: PhoneCallViewModel
    private var cancelSubscription: Disposable? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.phone_call_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(PhoneCallViewModel::class.java)

        val args = viewModel.args

        textView_phoneCallFragment_callerName.text = args.callerUserName

        val phoneCallManager = ReceivedPhoneCallManager(requireContext(), args.notificationId, args.callRequestsUid, args.peerMatchUid, args.callerUserUid )

        button_phoneCallFragment_accept.setOnClickListener {
            phoneCallManager.acceptPhoneCall()
            requireActivity().finish()
        }

        button_phoneCallFragment_decline.setOnClickListener {
            phoneCallManager.declinePhoneCall()
            requireActivity().finish()
        }

        cancelSubscription = phoneCallManager.callRequestClosed.subscribe {
            requireActivity().finish()
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        cancelSubscription?.dispose()
    }


}
