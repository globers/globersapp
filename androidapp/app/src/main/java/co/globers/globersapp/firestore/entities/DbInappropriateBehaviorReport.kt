package co.globers.globersapp.firestore.entities

import com.google.firebase.firestore.ServerTimestamp
import java.util.*

data class DbInappropriateBehaviorReport (
    val reporterUserUid: String,
    val reportedUserUid: String,
    val peerMatchUid: String,
    val inappropriateBehaviorType: DbInappropriateBehaviorType,
    val message: String,
    @ServerTimestamp
    val creationDate: Date = Date()
){


    companion object {
        const val COLLECTION_NAME = "inappropriateBehaviorReports"
    }

}

enum class DbInappropriateBehaviorType {
    Cheat,
    RefuseToPlay,
    SexualHarassment,
    OtherHarassment,
    HateSpeech,
    Other
}

