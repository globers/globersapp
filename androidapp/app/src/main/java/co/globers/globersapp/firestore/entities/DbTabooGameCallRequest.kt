package co.globers.globersapp.firestore.entities

import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ServerTimestamp
import java.util.*


// NB: to keep in sync with cloud function tabooGameCallRequestCreates
enum class DbTabooGameCallRequestStatus{
    SENT,
    RECEIVED,
    ERROR_SERVER,
    DENIED_BY_SERVER, // user blocked
    ACCEPTED_BY_CALLED,
    DECLINED_BY_CALLED,
    TIMEOUT_BEFORE_RECEIVED,
    CANCELED_BY_CALLER,
}

data class DbTabooGameCallRequest(
    val uid: String,
    val callerUid: String,
    val calledUid: String,
    val peerMatchUid: String,
    val status: DbTabooGameCallRequestStatus,
    @ServerTimestamp
    val creationDate: Date = Date()
){
    constructor(): this("", "","", "", DbTabooGameCallRequestStatus.SENT)

    companion object {

        const val COLLECTION_NAME = "tabooGameCallRequests"
        const val FIELD_STATUS = "status"

        fun buildDocRef(db: FirebaseFirestore, uid: String) = db.collection(COLLECTION_NAME).document(uid)
    }
}