package co.globers.globersapp.firestore.entities.tokeepinsync

import co.globers.globersapp.firestore.entities.DbLanguageIsoCode
import co.globers.globersapp.firestore.executeGet
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ServerTimestamp

data class DbTabooSchedule(
    val languageIsoCode: DbLanguageIsoCode,
    val matchingTimesInMsSinceMidnightUtc: List<Int>, // Nb of ms from 00H00 UTC
    val matchingDurationInMs: Int,
    @ServerTimestamp
    val creationDate: Timestamp = Timestamp.now()
){

    constructor(): this(DbLanguageIsoCode.ENG, listOf(), -1)

    companion object {
        const val COLLECTION_NAME = "tabooSchedules"
    }

}

suspend fun loadDbTabooSchedule(languageIsoCode: DbLanguageIsoCode): DbTabooSchedule {
    val db = FirebaseFirestore.getInstance()
    val docRef = db.collection(DbTabooSchedule.COLLECTION_NAME).document(languageIsoCode.name)
    return executeGet(docRef)
}
