package co.globers.globersapp.services

import co.globers.globersapp.firestore.entities.*
import co.globers.globersapp.firestore.executeBatch
import co.globers.globersapp.firestore.executeGet
import co.globers.globersapp.firestore.executeUpdate
import co.globers.globersapp.firestore.listenQuery
import com.google.firebase.firestore.FieldPath
import com.google.firebase.firestore.FirebaseFirestore
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.flatMapIterable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FriendsCreatorFromRequestsBackgroundService: Disposable {

    private val repliedRequestsSub: Disposable
    private val db = FirebaseFirestore.getInstance()

    init {

        val userDataObservables = UserDataObservables()

        val replied = userDataObservables.getAuthenticatedUserUidObservable().flatMap { userUid ->

            val repliedRequestsQuery = db.collection(DbFriendRequest.COLLECTION_NAME)
                .whereEqualTo(DbFriendRequest.FIELD_SENDER_USER_UID, userUid)
                .whereEqualTo(
                    DbFriendRequest.FIELD_STATUS,
                    DbFriendRequestStatus.REPLIED
                )

            listenQuery<DbFriendRequest>(
                repliedRequestsQuery
            )
        }


        // TODO NICO It's dirty....if several instances of FriendsCreatorFromRequestsBackgroundService are build
        //  TODO NICO should it be done inside cloud function ?
        // Allow to remove "disposable" on it
        repliedRequestsSub = replied
            .flatMapIterable()
            .subscribe { acceptedFriendRequest ->
                MainScope().launch { managedAcceptedRequests(acceptedFriendRequest) }
            }
    }


    private suspend fun managedAcceptedRequests(request: DbFriendRequest) {

        withContext(Dispatchers.IO) {
            val friendUid = request.receiverUserUid
            val userUid = request.senderUserUid
            val friendDocRef =
                DbContact.getDocReference(
                    db,
                    userUid,
                    friendUid
                )

            val profileRef =
                db.collection(DbUserPublicProfile.COLLECTION_NAME)
                    .document(friendUid)
            val profile =
                executeGet<DbUserPublicProfile>(
                    profileRef
                )

            if (request.reply == DbFriendRequestReply.ACCEPT) {

                executeBatch { batch ->

                    batch.set(
                        friendDocRef,
                        DbContact(
                            profile,
                            DbContactStatus.FRIEND,
                            request.uid
                        )
                    )

                    batch.update(
                        db.collection(DbFriendRequest.COLLECTION_NAME).document(
                            request.uid
                        ),
                        DbFriendRequest.FIELD_STATUS,
                        DbFriendRequestStatus.REPLIED_ACKNOWLEDGED
                    )
                }
            } else {
                executeUpdate(
                    db.collection(DbFriendRequest.COLLECTION_NAME).document(
                        request.uid
                    ),
                    FieldPath.of(DbFriendRequest.FIELD_STATUS),
                    DbFriendRequestStatus.REPLIED_ACKNOWLEDGED
                )
            }
        }
    }

    override fun isDisposed() = repliedRequestsSub.isDisposed

    override fun dispose() {
        repliedRequestsSub.dispose()
    }
}