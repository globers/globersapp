package co.globers.globersapp.utils

import java.util.*


fun localeToEmoji(countryIsoCode: String): String {
    val countryIso2 = Locale("", countryIsoCode).country
    val firstLetter = Character.codePointAt(countryIso2, 0) - 0x41 + 0x1F1E6
    val secondLetter = Character.codePointAt(countryIso2, 1) - 0x41 + 0x1F1E6
    return String(Character.toChars(firstLetter)) + String(Character.toChars(secondLetter))
}