package co.globers.globersapp.ui.taboo


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager

import co.globers.globersapp.R
import co.globers.globersapp.utils.Screen
import co.globers.globersapp.utils.Tracking
import kotlinx.android.synthetic.main.taboo_give_award_fragment.*

class TabooGiveAwardFragment : Fragment() {

    companion object {
        fun newInstance() = TabooGiveAwardFragment()
    }

    private lateinit var tracking: Tracking

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.taboo_give_award_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tracking = Tracking(this, Screen.TabooGiveAward)

        val viewModel = ViewModelProvider(this).get(TabooGiveAwardViewModel::class.java)
        val activityViewModel = ViewModelProvider(requireActivity()).get(TabooViewModel::class.java)
        val peerMatchResult = activityViewModel.peerMatchResult!!
        val peerMatch = peerMatchResult.peerMatch
        val isUserCreator = peerMatchResult.isCreator
        viewModel.init(peerMatch, isUserCreator)


        val adapter = TabooGiveAwardAwardTypeRecyclerViewAdapter()
        recyclerView_tabooGiveAward_awardType.adapter = adapter
        recyclerView_tabooGiveAward_awardType.layoutManager = LinearLayoutManager(activity)

        adapter.selectedAwardType.observe(viewLifecycleOwner, Observer {
            viewModel.setAwardType(it)
        })

        activityViewModel.state.observe(viewLifecycleOwner, Observer {
            if (it.status == TabooViewModel.Status.GameCompletedFeedbackFinished){

                AlertDialog.Builder(requireActivity())
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle("Award sent")
                    .setCancelable(false)
                    .setMessage("Nice! Your award has been sent")
                    .setNeutralButton("Ok") {  _, _ ->
                    val action = TabooGiveAwardFragmentDirections.actionTabooGiveAwardFragmentToTabooFinishedFragment()
                        findNavController().navigate(action)
                    }
                    .show()
            }
        })

        viewModel.status.observe(viewLifecycleOwner, Observer {
            when(it.status){
                TabooGiveAwardViewModel.Status.NOT_SAVED -> {
                    button_tabooGiveAward_sendAward.isEnabled = (it.userAwardType != null)
                }
                TabooGiveAwardViewModel.Status.SAVE_IN_PROGRESS -> {
                    freezeScreen()
                }
                TabooGiveAwardViewModel.Status.FINISHED -> {

                    if (activityViewModel.isTeammateInContacts()){
                        activityViewModel.setFeedbackGivenOrDiscarded()
                    } else {

                        val teammateName = activityViewModel.teammateGameInfo.displayName
                        AlertDialog.Builder(requireActivity())
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Add '$teammateName' to your friends?")
                            .setMessage("Do you want to add '$teammateName' to your friends? You can call your friends directly to play")
                            .setCancelable(false)
                            .setPositiveButton("Add") { _, _ ->
                                activityViewModel.setFeedbackGivenOrDiscarded()
                                activityViewModel.sendFriendRequestToTeammate()
                            }
                            .setNegativeButton("No") { _, _ ->
                                activityViewModel.setFeedbackGivenOrDiscarded()
                            }
                            .show()
                    }
                }
            }
        })

        button_tabooGiveAward_cancel.setOnClickListener {
            requireActivity().onBackPressed()
        }

        button_tabooGiveAward_sendAward.setOnClickListener {
            viewModel.save()
        }

    }

    private fun freezeScreen(){
        button_tabooGiveAward_sendAward.isEnabled = false
        button_tabooGiveAward_cancel.isEnabled = false
    }

}
