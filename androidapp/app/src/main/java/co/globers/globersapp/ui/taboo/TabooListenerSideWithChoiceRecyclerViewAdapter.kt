package co.globers.globersapp.ui.taboo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import co.globers.globersapp.R

class TabooListenerSideWithChoiceRecyclerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val button: Button = view.findViewById(R.id.button_tabooListenerSideWithChoice_word)
}

class TabooListenerSideWithChoiceRecyclerViewAdapter(
    private val words: List<String>,
    private val onWordClick: (String) -> Unit
) : RecyclerView.Adapter<TabooListenerSideWithChoiceRecyclerViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TabooListenerSideWithChoiceRecyclerViewHolder {
        val layoutView = R.layout.taboo_listener_side_with_choice_word_view
        val v = LayoutInflater.from(parent.context).inflate(layoutView, parent, false)
        return TabooListenerSideWithChoiceRecyclerViewHolder(v)
    }

    override fun getItemCount() = words.size

    override fun onBindViewHolder(holder: TabooListenerSideWithChoiceRecyclerViewHolder, position: Int) {

        val word = words[position]
        holder.button.text = word

        holder.button.setOnClickListener {
            onWordClick(word)
        }

    }

}