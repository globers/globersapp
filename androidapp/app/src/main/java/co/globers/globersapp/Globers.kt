package co.globers.globersapp

import android.app.Activity
import android.app.Application
import android.os.Bundle


data class AdminParams(var fakeVoiceCall: Boolean = false)

class Globers : Application() {

    var adminParams: AdminParams? = null


    override fun onCreate() {
        super.onCreate()
        registerActivityLifecycleCallbacks(AppLifecycleTracker())


    }

}


// https://stackoverflow.com/questions/4414171/how-to-detect-when-an-android-app-goes-to-the-background-and-come-back-to-the-fo/44461605#44461605
class AppLifecycleTracker : Application.ActivityLifecycleCallbacks  {


    override fun onActivityStarted(activity: Activity) {

    }

    override fun onActivityDestroyed(activity: Activity) {
    }

    override fun onActivitySaveInstanceState(activity: Activity, p1: Bundle) {
    }

    override fun onActivityStopped(activity: Activity) {

    }

    override fun onActivityCreated(activity: Activity, p1: Bundle?) {
    }

    override fun onActivityResumed(activity: Activity) {
    }

    override fun onActivityPaused(activity: Activity?) {
    }

}