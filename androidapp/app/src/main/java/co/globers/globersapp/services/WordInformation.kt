package co.globers.globersapp.services

import co.globers.globersapp.AppConfig
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


private interface WordnikApi {

    companion object {
        const val API_KEY = AppConfig.WORDNIK_API_KEY
        const val NB_DEFINITIONS = 3
        const val DICTIONARY_NAME = "ahd-5"
    }

    class Definition(val partOfSpeech: String, val attributionText: String?, val text: String?)


    @GET("{word}/definitions?limit=$NB_DEFINITIONS&sourceDictionaries=$DICTIONARY_NAME&api_key=$API_KEY")
    fun getWordDefinitions(@Path("word") word: String): Call<List<Definition>>

}

data class WordDefinition(val wordType: String, val definition: String)

data class WordInformation(val definitions: List<WordDefinition>, val attributionText: String)

class WordInformationService {

    companion object {

        private val wordnikService = Retrofit.Builder()
            .baseUrl("https://api.wordnik.com/v4/word.json/")
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(WordnikApi::class.java)


        suspend fun getWordInformation(word: String): WordInformation? {

            return suspendCoroutine { cont: Continuation<WordInformation?> ->


                val call = wordnikService.getWordDefinitions(word)
                call.enqueue(object : Callback<List<WordnikApi.Definition>> {

                    override fun onResponse(
                        call: Call<List<WordnikApi.Definition>>, response: Response<List<WordnikApi.Definition>>
                    ) {

                        val result = if (response.isSuccessful && response.body() != null) {
                            val wordnikDefs = response.body()!!

                            val defs = wordnikDefs
                                .filter { it.text != null && it.text.isNotBlank() }
                                .map { WordDefinition(it.partOfSpeech, it.text!!) }

                            if (defs.isEmpty()) null else WordInformation(defs, defs.first().wordType)

                        } else {
                            null
                        }

                        cont.resume(result)
                    }

                    override fun onFailure(call: Call<List<WordnikApi.Definition>>, t: Throwable) {
                        cont.resume(null)
                    }

                })


            }

        }


    }


}
