package co.globers.globersapp.ui.taboo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import co.globers.globersapp.R
import co.globers.globersapp.firestore.entities.DbLanguageIsoCode
import co.globers.globersapp.utils.Screen
import co.globers.globersapp.utils.Tracking
import kotlinx.android.synthetic.main.taboo_listener_side_fragment.*


class TabooListenerSideFragment : Fragment() {

    companion object {
        fun newInstance() = TabooListenerSideFragment()
        const val LOG_TAG = "TabooListenerSideFr"


        const val maxLettersByButton = 3
        const val maxNbButtonClicks = 4
    }

    private lateinit var viewModel: TabooViewModel
    private lateinit var lettersButtons: Array<Button>
    private var wordToGuess: String? = null
    private lateinit var tracking: Tracking


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.taboo_listener_side_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tracking = Tracking(this, Screen.TabooListenerSide)

        val view = requireView()
        lettersButtons = arrayOf(
            view.findViewById(R.id.button_tabooKeyboardFragment_11),
            view.findViewById(R.id.button_tabooKeyboardFragment_12),
            view.findViewById(R.id.button_tabooKeyboardFragment_13),
            //view.findViewById(R.id.button_tabooKeyboardFragment_14),
            view.findViewById(R.id.button_tabooKeyboardFragment_21),
            view.findViewById(R.id.button_tabooKeyboardFragment_22),
            view.findViewById(R.id.button_tabooKeyboardFragment_23),
            //view.findViewById(R.id.button_tabooKeyboardFragment_24),
            view.findViewById(R.id.button_tabooKeyboardFragment_31),
            view.findViewById(R.id.button_tabooKeyboardFragment_32),
            view.findViewById(R.id.button_tabooKeyboardFragment_33)
            //view.findViewById(R.id.button_tabooKeyboardFragment_34)
        )

        val replyTextView = textView_tabooListenerSide_wordToGuess
        val clearButton = view.findViewById<Button>(R.id.button_tabooKeyboardFragment_clear)
        clearButton.text = "Clear"

        viewModel = ViewModelProvider(requireActivity()).get(TabooViewModel::class.java)

        for(keyButton in lettersButtons){
            keyButton.setOnClickListener {
                val reply = "${replyTextView.text}${keyButton.text}"
                replyTextView.text = reply
                it.visibility = View.INVISIBLE
                if (reply == this.wordToGuess){
                    viewModel.guessWord()
                }
            }
        }

        clearButton.setOnClickListener {
            replyTextView.text = ""
            //reset button visibility
            lettersButtons.forEach { button -> button.visibility = View.VISIBLE }
        }


        viewModel.state.observe( viewLifecycleOwner, Observer {

            if (it.status == TabooViewModel.Status.RunningGuesser){

                //reset button visibility
                lettersButtons.forEach { button -> button.visibility = View.VISIBLE }
                replyTextView.text = ""

                val activeCard = it.gameState!!.currentCard!!
                val wordToGuess = activeCard.wordToGuess

                if (wordToGuess != this.wordToGuess) { // wordToGuess has changed

                    this.wordToGuess = wordToGuess
                    val keyboard = buildKeyboard(wordToGuess, lettersButtons.size, maxLettersByButton, maxNbButtonClicks, viewModel.languageLearnedIsoCode)
                    for (i in lettersButtons.indices) {
                        lettersButtons[i].text = keyboard[i]
                    }
                }

            }

        })

    }

}


fun buildKeyboard(wordToType: String, nbKeyboardKeys: Int, maxNbLettersByKey: Int, maxNbKeyPress: Int, languageIsoCode: DbLanguageIsoCode): List<String> {

    val wordSize = wordToType.length
    val nbLettersByKey = Math.min(Math.ceil(wordSize.toDouble() / maxNbKeyPress).toInt(), maxNbLettersByKey)

    val nbKeysUsedForWord = Math.ceil(wordSize.toDouble() / nbLettersByKey).toInt()
    val nbKeysWithNbLettersMinus1 = nbKeysUsedForWord * nbLettersByKey - wordSize
    val nbKeysWithNbLetters = nbKeysUsedForWord - nbKeysWithNbLettersMinus1

    val splitSequenceOfWordToType =
        (List(nbKeysWithNbLetters) { nbLettersByKey } + List(nbKeysWithNbLettersMinus1) { nbLettersByKey - 1 }).shuffled()

    var currentIndex = 0
    val textOfKeysUsedForWordToType = mutableListOf<String>()
    for (currentKeyNbChars in splitSequenceOfWordToType) {
        val textThisKey: CharSequence = wordToType.subSequence(currentIndex, currentIndex + currentKeyNbChars)
        currentIndex += currentKeyNbChars
        textOfKeysUsedForWordToType.add(textThisKey.toString())
    }

    val alphabet = getAlphabet(languageIsoCode)
    val textOfKeysNotUsedForWordToType = List(nbKeyboardKeys-nbKeysUsedForWord){ getRandomKeyText(alphabet, nbLettersByKey, wordToType)}

    return (textOfKeysUsedForWordToType + textOfKeysNotUsedForWordToType).shuffled()
}

fun getAlphabet(languageIsoCode: DbLanguageIsoCode): String{

    return when(languageIsoCode){
        DbLanguageIsoCode.ENG -> "abcdefghijklmnopqrstuvwxyz"
        DbLanguageIsoCode.FRA -> "abcdefghijklmnopqrstuvwxyz"
    }
}

fun getRandomKeyText(alphabet: String, maxNbCharsByKey: Int, forbiddenChars: String): String{
    val nbChars = (1..maxNbCharsByKey).random()
    val possibleChars = alphabet.filter { it !in forbiddenChars }

    return String(CharArray(nbChars){possibleChars.random()})
}


