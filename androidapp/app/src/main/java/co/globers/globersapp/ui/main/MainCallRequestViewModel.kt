package co.globers.globersapp.ui.main

import android.os.Handler
import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.ViewModel
import co.globers.globersapp.firestore.entities.DbTabooGameCallRequest
import co.globers.globersapp.firestore.entities.DbTabooGameCallRequestStatus
import co.globers.globersapp.firestore.executeGet
import co.globers.globersapp.firestore.executeSet
import co.globers.globersapp.firestore.executeUpdate
import co.globers.globersapp.firestore.listenNullable
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldPath
import com.google.firebase.firestore.FirebaseFirestore
import io.reactivex.BackpressureStrategy
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import java.util.*

class MainCallRequestViewModel : ViewModel(){

    companion object {
        const val LOG_TAG = "MainCallRequestVM"
    }

    private val userUid = FirebaseAuth.getInstance().uid!!
    private val db = FirebaseFirestore.getInstance()

    private val docRef = db.collection(DbTabooGameCallRequest.COLLECTION_NAME).document(UUID.randomUUID().toString())
    val peerMatchUid = UUID.randomUUID().toString()

    enum class CallStatus {
        NotStarted,
        Ongoing,
        Failed,
        CanceledByCaller,
        Accepted
    }

    val callStatus: LiveData<CallStatus>

    init {
        val obs  = listenNullable<DbTabooGameCallRequest>(docRef).map {
            when(it.value?.status){
                DbTabooGameCallRequestStatus.SENT -> CallStatus.Ongoing
                DbTabooGameCallRequestStatus.RECEIVED -> CallStatus.Ongoing
                DbTabooGameCallRequestStatus.ERROR_SERVER -> CallStatus.Failed
                DbTabooGameCallRequestStatus.DENIED_BY_SERVER -> CallStatus.Failed
                DbTabooGameCallRequestStatus.ACCEPTED_BY_CALLED -> CallStatus.Accepted
                DbTabooGameCallRequestStatus.DECLINED_BY_CALLED -> CallStatus.Failed
                DbTabooGameCallRequestStatus.TIMEOUT_BEFORE_RECEIVED -> CallStatus.Failed
                DbTabooGameCallRequestStatus.CANCELED_BY_CALLER -> CallStatus.CanceledByCaller
                null -> CallStatus.NotStarted
            }

        }

        callStatus = LiveDataReactiveStreams.fromPublisher(obs.toFlowable(BackpressureStrategy.LATEST))
    }

    fun startCall(calledUserUid: String) {

        // ici observe call, extract service to do so
        MainScope().launch {
            executeSet(
                docRef,
                DbTabooGameCallRequest(
                    docRef.id, userUid, calledUserUid, peerMatchUid,
                    DbTabooGameCallRequestStatus.SENT
                )
            )

            Handler().postDelayed({

                MainScope().launch {
                    val request = executeGet<DbTabooGameCallRequest>(docRef)
                    if (request.status == DbTabooGameCallRequestStatus.SENT) { // still not recevied after 5 sec => we cancel
                        executeUpdate(docRef, FieldPath.of(DbTabooGameCallRequest.FIELD_STATUS), DbTabooGameCallRequestStatus.TIMEOUT_BEFORE_RECEIVED)
                    }
                }
            }, 15000)

        }
    }

    fun cancelCall(){

        if (callStatus.value == CallStatus.Ongoing){
            MainScope().launch {
                executeUpdate(
                    docRef,
                    FieldPath.of(DbTabooGameCallRequest.FIELD_STATUS),
                    DbTabooGameCallRequestStatus.CANCELED_BY_CALLER
                )
            }
        }
    }


}
