package co.globers.globersapp.firestore.entities

import com.google.firebase.firestore.ServerTimestamp
import java.util.*


enum class DbPeerMatchStatus {
    WAITING_FOR_PEER,
    JOINED,
    CANCELED,
    FORCE_CANCELED
}


data class DbPeerMatch(
    val uid: String,
    val fixedJoinerUid: String?,
    val creatorGameInfo: DbUserGameInfo,
    val joinerGameInfo: DbUserGameInfo?,
    val status: DbPeerMatchStatus,
    val voiceCallChanelName: String,
    val gameUid: String?,
    val waitingTimeInSec: Int,
    @ServerTimestamp
    val creationDate: Date = Date()
){

    // used by buildFindWaitingMatchesQuery query to match on level
    val targetNbWordsKnown = creatorGameInfo.nbWordsKnownInGameLanguage

    companion object {
        const val COLLECTION_NAME_RANDOM = "peerMatchesRandom"
        const val COLLECTION_NAME_FIXED = "peerMatchesFixed"

        const val FIELD_CREATOR = "creatorGameInfo"
        const val FIELD_JOINER = "joinerGameInfo"
        const val FIELD_STATUS= "status"
        const val FIELD_GAME_UID = "gameUid"
        const val FIELD_TARGET_NB_WORDS_KNOWN = "targetNbWordsKnown"
        const val FIELD_WAITING_TIME_IN_SEC = "waitingTimeInSec"
    }

    constructor(): this(
        "", null,
        DbUserGameInfo(), null,
        DbPeerMatchStatus.WAITING_FOR_PEER,
        "",
        "",
        -1
    )

}