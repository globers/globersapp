package co.globers.globersapp.firestore.entities

import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ServerTimestamp
import java.util.*


enum class DbContactStatus {
    FRIEND,
    BLOCKED
}


data class DbContact(
    val publicProfile: DbUserPublicProfile, // denormalized from profiles collection
    val status: DbContactStatus,
    val friendRequestUid: String,
    @ServerTimestamp
    val creationDate: Date = Date()
){

    constructor(): this(DbUserPublicProfile(), DbContactStatus.BLOCKED, "")

    val uid = publicProfile.userUid

    companion object {
        const val COLLECTION_NAME = "contacts"
        const val FIELD_STATUS = "status"

        fun getCollectionReference(db: FirebaseFirestore, userUid: String) =
            db.collection(DbUser.COLLECTION_NAME)
            .document(userUid)
            .collection(COLLECTION_NAME)

        fun getDocReference(db: FirebaseFirestore, userUid: String, friendUserUid: String) =
            getCollectionReference(db, userUid)
                .document(friendUserUid)
    }

}

