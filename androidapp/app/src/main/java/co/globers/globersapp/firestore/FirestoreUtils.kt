package co.globers.globersapp.firestore

import android.util.Log
import co.globers.globersapp.utils.InconsistantDbStateExcepton
import com.google.firebase.firestore.*
import io.reactivex.Observable
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine



suspend inline fun <reified T> executeQuery(query: Query): List<T> {

    return suspendCoroutine { cont: Continuation<List<T>> ->

        query.get()
            .addOnSuccessListener { documents ->
                cont.resume(documents.map { it.toObject(T::class.java) })
            }
            .addOnFailureListener { ex ->
                Log.e("EXEC_QUERY", "error while executing query in firestore, query:$query", ex)
                cont.resumeWithException(ex)
            }
    }
}


suspend fun <T: Any> executeSet(docRef: DocumentReference, data: T){
    return suspendCoroutine { cont: Continuation<Unit> ->

        docRef.set(data)
            .addOnSuccessListener {
                cont.resume(Unit)
            }
            .addOnFailureListener { ex ->
                Log.e("EXEC_SET", "error while executing set query in firestore for doc $docRef", ex)
                cont.resumeWithException(ex)
            }
    }
}

suspend fun <T: Any> executeUpdate(docRef: DocumentReference, fieldPath: FieldPath, value: T){
    executeUpdate(docRef, mapOf(fieldPath.toString() to value))
}

suspend fun executeUpdate(docRef: DocumentReference, fields: Map<String,Any> ){
    Log.d("EXECUTE_UPDATE", "executeUpdate for path ${docRef.path} and fields ${fields}")
    return suspendCoroutine { cont: Continuation<Unit> ->

        docRef.update(fields)
            .addOnSuccessListener {
                cont.resume(Unit)
            }
            .addOnFailureListener { ex ->
                Log.e("EXEC_UPDATE", "error while executing update query in firestore for doc $docRef", ex)
                cont.resumeWithException(ex)
            }
    }
}


suspend inline fun <reified T> executeGet(docRef: DocumentReference, source: Source = Source.DEFAULT): T {
    return executeGetOrNull<T>(docRef, source) ?:
    throw InconsistantDbStateExcepton("document not found for path ${docRef.path} and type ${T::class.java}")
}



suspend inline fun <reified T> executeGetOrNull(docRef: DocumentReference, source: Source = Source.DEFAULT): T? {

    Log.d("EXECUTE_GET_NULL", "executeGetOrNull for path ${docRef.path} and type ${T::class.java}")
    return suspendCoroutine { cont: Continuation<T?> ->

        docRef.get(source)
            .addOnSuccessListener { document ->
                if ( document == null || !document.exists()){
                    cont.resume(null)
                }
                else {
                    val value = document.toObject(T::class.java)!!
                    cont.resume(value)
                }
            }
            .addOnFailureListener { ex ->
                Log.e("EXEC_QUERY", "error while executing query in firestore for doc $docRef", ex)
                cont.resumeWithException(ex)
            }
    }
}



suspend fun executeBatch(batchSteps: (batch: WriteBatch) -> Unit) {

    return suspendCoroutine { cont: Continuation<Unit> ->

        val db: FirebaseFirestore = FirebaseFirestore.getInstance()

        val batch = db.batch()

        batchSteps(batch)

        batch.commit()
            .addOnSuccessListener { cont.resume(Unit) }
            .addOnFailureListener { ex ->
                Log.e("EXEC_BATCH", "error while executing batch in firestore", ex)
                cont.resumeWithException(ex)
            }
    }
}

suspend fun <T> executeTransaction(transactionSteps: (batch: Transaction) -> T): T {

    return suspendCoroutine { cont: Continuation<T> ->

        val db: FirebaseFirestore = FirebaseFirestore.getInstance()

        db.runTransaction{
            transactionSteps(it)
        }
            .addOnSuccessListener { result: T -> cont.resume(result) }
            .addOnFailureListener { ex ->
                Log.e("EXEC_TRANSACTION", "error while executing transaction in firestore", ex)
                cont.resumeWithException(ex)
            }
    }

}


inline fun <reified T> listenQuery(query: Query): Observable<List<T>>{

    Log.d("LISTEN_QUERY", "build Listen observable for quey ${query} and type ${T::class.java}")
    return Observable.create { emitter ->

        val listenerRegistration = query
            .addSnapshotListener { snapshot: QuerySnapshot?, e ->

                if (e != null) {
                    Log.e("LISTEN", "Listen failed for path ${query} and type ${T::class.java}", e)
                    emitter.onError(e)
                } else if (snapshot != null){
                    val docs = snapshot.documents.map { it.toObject(T::class.java)!! }
                    emitter.onNext(docs)
                }
            }

        emitter.setCancellable { listenerRegistration.remove() }
    }

}


inline fun <reified T> listen(docRef: DocumentReference): Observable<T> {

    Log.d("LISTEN", "build Listen observable for path ${docRef.path} and type ${T::class.java}")
    return Observable.create { emitter ->

        val listenerRegistration = docRef
            .addSnapshotListener { snapshot, e ->

                if (e != null) {
                    Log.e("LISTEN", "Listen failed for path ${docRef.path} and type ${T::class.java}", e)
                    emitter.onError(e)
                } else if (snapshot == null || !snapshot.exists()){
                    Log.e("LISTEN", "document not found for path ${docRef.path} and type ${T::class.java}")
                    emitter.onError(NullPointerException("snapshot is null or empty"))
                } else {
                    val doc = snapshot.toObject(T::class.java)!!
                    emitter.onNext(doc)
                }
            }

        emitter.setCancellable { listenerRegistration.remove() }
    }
}

data class Nullable<T>(
    val value: T?
)

inline fun <reified T> listenNullable(docRef: DocumentReference): Observable<Nullable<T>> {

    Log.d("LISTEN_NULLABLE", "build Listen observable for path ${docRef.path} and type ${T::class.java}")
    return Observable.create { emitter ->

        Log.d("LISTEN_NULLABLE", "Start Listen for path ${docRef.path} and type ${T::class.java}")
        val listenerRegistration = docRef
            .addSnapshotListener { snapshot, e ->

                if (e != null) {
                    Log.e("LISTEN_NULLABLE", "Listen failed for path ${docRef.path} and type ${T::class.java}", e)
                    emitter.onError(e)
                } else if (snapshot == null || !snapshot.exists()){
                    emitter.onNext(Nullable(null))
                } else {
                    val doc = snapshot.toObject(T::class.java)!!
                    emitter.onNext(Nullable(doc))
                }
            }

        emitter.setCancellable { listenerRegistration.remove() }
    }
}



inline fun <reified T> listenOnce(docRef: DocumentReference): Observable<T> {

    Log.d("LISTEN_ONCE", "build Listen observable for path ${docRef.path} and type ${T::class.java}")
    return Observable.create<T> { emitter ->

        docRef.get()
            .addOnSuccessListener { document ->
                if (document == null || !document.exists()) {
                    Log.e("LISTEN_ONCE", "document not found for path ${docRef.path} and type ${T::class.java}")
                    emitter.onError(NullPointerException("snapshot is null or empty"))
                } else {
                    val value = document.toObject(T::class.java)!!
                    emitter.onNext(value)
                    emitter.onComplete()
                }
            }
            .addOnFailureListener { e ->
                Log.e("LISTEN_ONCE", "Listen failed for path ${docRef.path} and type ${T::class.java}", e)
                emitter.onError(e)
            }
    }
}
