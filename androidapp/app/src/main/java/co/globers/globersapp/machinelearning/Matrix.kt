package co.globers.globersapp.machinelearning

data class Matrix(val valuesByRow: Array<Vector>){

    constructor(valuesByRow: List<Vector>): this(valuesByRow.toTypedArray())

    companion object {
        fun diag(diag: Vector) : Matrix {
            return Matrix(
                Array(diag.dim()) { j ->
                    Vector(DoubleArray(diag.dim()) { i ->
                        if (i == j) diag[i] else 0.0
                    })
                }
            )

        }
    }

    operator fun get(colIndex: Int, rowIndex: Int): Double{
        return valuesByRow[rowIndex][colIndex]
    }


    fun transpose(): Matrix {
        return Matrix(
            Array(nbCols()) { j ->
                Vector(DoubleArray(nbRows()) { i ->
                    this[j, i]
                })
            })
        //i-em column, j-em row has value of j-em column, i-em row
    }

    fun row(rowIndex: Int) = valuesByRow[rowIndex]
    fun nbCols(): Int = row(0).dim()
    fun nbRows(): Int = valuesByRow.size

    operator fun times(b: Matrix): Matrix {

        val bt = b.transpose()

        return Matrix(Array(nbRows()) { j ->
            Vector(DoubleArray(b.nbCols()) { i ->
                row(j).scalar(bt.row(i))
            })
        })
    }

    operator fun times(vector: Vector) =
        Vector(valuesByRow.map { it.scalar(vector) })
    operator fun times(scalar: Double) = Matrix(valuesByRow.map { it * scalar })
    operator fun unaryMinus() = Matrix(valuesByRow.map { -it })

    fun inverse22(): Matrix {
        val a= this[0,0]
        val b = this[1,0]
        val c = this[0,1]
        val d = this[1,1]

        val determinant = a*d - b*c
        val firstRow = Vector(doubleArrayOf(d, -b))
        val secondRow = Vector(doubleArrayOf(-c, a))

        return Matrix(arrayOf(firstRow, secondRow)) *(1/determinant)
    }



}