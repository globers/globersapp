package co.globers.globersapp.ui.taboo

import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import co.globers.globersapp.NavGraphTabooDirections
import co.globers.globersapp.R
import co.globers.globersapp.services.IVoiceCall
import co.globers.globersapp.services.MockVoiceCall
import co.globers.globersapp.services.VoiceCall
import co.globers.globersapp.utils.InternetConnectionManager
import co.globers.globersapp.utils.allowOnLockedScreen
import kotlinx.android.parcel.Parcelize


// contraintes:
// - à terme, on peut vouloir choisir les params de la partie après être au tel
// - quand le type répond, il faut direct que le telephone réponde


@Parcelize
class TabooActivityTeammateArgs(
    val teammateUserUid: String,
    val peerMatchUid: String,
    val isCreator: Boolean // will create the peerMatch
) : Parcelable

// if null => default lesson
@Parcelize
class TabooActivityLessonArgs(
    val userProgramUid: String,
    val userLessonUid: String,
    val levelUid: String
) : Parcelable

@Parcelize
class TabooActivityArgs(
    val lessonArgs: TabooActivityLessonArgs?, // if null => default program & lesson
    val teammateArgs: TabooActivityTeammateArgs?, // if null => random peer matching
    val fakeVoiceCall: Boolean // TODO Checker to delete: might be useless...
) : Parcelable

class TabooActivity : AppCompatActivity() {

    companion object {
        private const val LOG_TAG = "TabooAct"
        const val EXTRA_TABOO_ACTIVITY_ARGS = "extraTabooActivityArgs"

        const val RESULT_CODE_OPEN_FRIENDS_TAB = 8645
    }

    private lateinit var viewModel: TabooViewModel
    private lateinit var nav: NavController
    private lateinit var voiceCall: IVoiceCall

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        allowOnLockedScreen(this)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON) // prevent screen to shut down during game: https://developer.android.com/training/scheduling/wakelock#screen

        setContentView(R.layout.activity_taboo)

        viewModel = ViewModelProvider(this).get(TabooViewModel::class.java)
        nav = Navigation.findNavController(this, R.id.taboo_navhost_fragment)

        val args = intent.getParcelableExtra<TabooActivityArgs>(EXTRA_TABOO_ACTIVITY_ARGS)!!

        voiceCall = if (args.fakeVoiceCall) MockVoiceCall() else VoiceCall(this)

        viewModel.init(args)

        voiceCall.state.observe(this, Observer {
            viewModel.onVoiceCallStatusUpdate(it)
        })

        viewModel.callStateRequest.observe(this, Observer {callStatusRequest: TabooViewModel.CallStatusRequest ->
            when (callStatusRequest){
                TabooViewModel.CallStatusRequest.Nothing -> {}
                TabooViewModel.CallStatusRequest.StartCall -> voiceCall.startCall(viewModel.voiceCallChanelName!!, viewModel.isCreator!!)
                TabooViewModel.CallStatusRequest.Hangup -> voiceCall.hangup()
            }
        })

        InternetConnectionManager(this, true)

        if(savedInstanceState !=null){
            // it means it has been "killed" by android (waited too long or memory too low), we close it to go back to program
            finish()
        }

    }


    override fun onBackPressed() {

        val status = viewModel.state.value.status

        Log.d(LOG_TAG, "onBackPressed, model status $status")

        if (status in (TabooViewModel.Status.GameLoading..TabooViewModel.Status.RunningTalker)) {

            AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Quit game?")
                .setCancelable(false)
                .setMessage("Are you sure you want to quit the game?")
                .setPositiveButton("Yes") { _, _ ->
                    viewModel.quitGame() }
                .setNegativeButton("No", null)
                .show()
        } else if (status == TabooViewModel.Status.Disconnected ||
            status == TabooViewModel.Status.AbortedByUser ||
            status == TabooViewModel.Status.GameAbortedFeedbackFinished){
            finish()
        } else {
            super.onBackPressed()
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        voiceCall.hangup()
    }

}




fun manageGameInterrupted(activityViewModel: TabooViewModel, fragment: Fragment){

    activityViewModel.state.observe(fragment.viewLifecycleOwner, Observer {

        if ( it.voiceCallState.status != IVoiceCall.Status.Started) { // if voice call still started, we wait for it before managing gameInterrupted event
            if (it.status == TabooViewModel.Status.AbortedByUser) {

                manageReportUserBeforeQuit(it.voiceCallState, fragment)

            } else if (it.status == TabooViewModel.Status.Disconnected) {

                AlertDialog.Builder(fragment.requireActivity())
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Teammate Disconnected")
                    .setCancelable(false)
                    .setMessage("You have been disconnected from your teammate")
                    .setNeutralButton("Ok") { _, _ ->
                        manageReportUserBeforeQuit(it.voiceCallState, fragment)
                    }
                    .show()
            }
        }
    })
}

private fun manageReportUserBeforeQuit(voiceCallState: IVoiceCall.State, fragment: Fragment){

    if (callWasMoreThanTwoSec(voiceCallState)) {
        AlertDialog.Builder(fragment.requireActivity())
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle("Report user?")
            .setCancelable(false)
            .setMessage("If your teammate had an inappropriate behavior, please report user")
            .setNeutralButton("Quit") { _, _ -> fragment.requireActivity().finish() }
            .setNegativeButton("Report User"){ _, _ ->

                val action = NavGraphTabooDirections.actionGlobalTabooReportUserFragment()
                fragment.findNavController().navigate(action)

            }
            .show()
    } else {
        fragment.requireActivity().finish()
    }

}

private fun callWasMoreThanTwoSec(state: IVoiceCall.State) =
    state.endDate != null && state.startDate != null &&
            (state.endDate.time - state.startDate.time) > 2000