package co.globers.globersapp.ui.taboo

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import co.globers.globersapp.R
import co.globers.globersapp.firestore.entities.DbInappropriateBehaviorType

class TabooReportUserReasonTypeRecyclerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val reasonTypeTextView: TextView = view.findViewById(R.id.textView_taboo_reportUser_reasonType)
}


class TabooReportUserReasonTypeRecyclerViewAdapter(
) : RecyclerView.Adapter<TabooReportUserReasonTypeRecyclerViewHolder>() {

    private var selectedItemBackgroundColor: Int = 0

    var selectedInappropriateBehaviorType: MutableLiveData<DbInappropriateBehaviorType?> = MutableLiveData(null)
        private set

    private var selectedHolder: TabooReportUserReasonTypeRecyclerViewHolder? = null


    private fun reportTypeMessage(behaviorType: DbInappropriateBehaviorType) = when (behaviorType){
        DbInappropriateBehaviorType.Cheat -> "Cheat"
        DbInappropriateBehaviorType.RefuseToPlay -> "Refuse to play"
        DbInappropriateBehaviorType.SexualHarassment -> "Sexual harassment"
        DbInappropriateBehaviorType.OtherHarassment -> "Other harassment"
        DbInappropriateBehaviorType.HateSpeech -> "Hate speech (racism...)"
        DbInappropriateBehaviorType.Other -> "Other reason"
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TabooReportUserReasonTypeRecyclerViewHolder {
        val layoutView = R.layout.taboo_reportuser_reasontype_view
        val v = LayoutInflater.from(parent.context).inflate(layoutView, parent, false)
        selectedItemBackgroundColor = ContextCompat.getColor(v.context, R.color.myColorSecondary)
        return TabooReportUserReasonTypeRecyclerViewHolder(v)
    }

    override fun getItemCount() = DbInappropriateBehaviorType.values().size

    override fun onBindViewHolder(holder: TabooReportUserReasonTypeRecyclerViewHolder, position: Int) {

        val behaviorType = DbInappropriateBehaviorType.values()[position]
        holder.reasonTypeTextView.text = reportTypeMessage(behaviorType)


        holder.reasonTypeTextView.setOnClickListener {

            val previousFocusedHolder = selectedHolder

            val backgroundColor = holder.reasonTypeTextView.background
            val textColor = holder.reasonTypeTextView.currentTextColor

            previousFocusedHolder?.reasonTypeTextView?.background = backgroundColor
            previousFocusedHolder?.reasonTypeTextView?.setTextColor(textColor)

            selectedHolder = holder
            selectedInappropriateBehaviorType.value = behaviorType

            holder.reasonTypeTextView.setBackgroundColor(selectedItemBackgroundColor)
            holder.reasonTypeTextView.setTextColor(Color.WHITE)
        }
    }

}