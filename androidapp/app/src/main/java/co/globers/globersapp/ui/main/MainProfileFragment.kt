package co.globers.globersapp.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import co.globers.globersapp.BuildConfig
import co.globers.globersapp.R
import co.globers.globersapp.ui.admin.AdminActivity
import co.globers.globersapp.utils.Screen
import co.globers.globersapp.utils.Tracking
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.main_profile_fragment.*

class MainProfileFragment : Fragment() {

    companion object {
        fun newInstance() = MainProfileFragment()
    }

    private lateinit var tracking: Tracking


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_profile_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tracking = Tracking(this, Screen.MainProfile)
        val bottomNavView = requireActivity().main_bottom_navigation_view
        bottomNavView.visibility = View.VISIBLE

        val buttonLogOut = requireView().findViewById<Button>(R.id.button_profile_logout)

        buttonLogOut.setOnClickListener {
            val firebaseAuth = FirebaseAuth.getInstance()
            val firestoreInstance = FirebaseFirestore.getInstance()
            firestoreInstance.terminate()
            firestoreInstance.clearPersistence()
            firebaseAuth.signOut()
            requireActivity().finish()
            val intent = Intent(activity, MainActivity::class.java)
            startActivity(intent)
        }

        val viewModel = ViewModelProvider(requireActivity()).get(MainProfileViewModel::class.java)

        viewModel.userPublicProfile.observe(viewLifecycleOwner, Observer {
            textView_profile_displayName.text = it.displayName
            textView_profile_username.text = it.username
        })

        FirebaseAuth.getInstance().currentUser?.let {
            textView_profile_email.text = it.email.orEmpty()
            textView_profile_loginProvider.text = (it.getIdToken(false).result?.signInProvider).orEmpty()
        }

        textView_profile_appVersion.text = BuildConfig.VERSION_NAME

        viewModel.isAdmin.observe(viewLifecycleOwner, Observer {

            if (it){
                floatingActionButton_mainProfile_admin.visibility = View.VISIBLE

                floatingActionButton_mainProfile_admin.setOnClickListener {
                    val intent = Intent(activity, AdminActivity::class.java)
                    startActivity(intent)
                }
            }
        })

    }


}
