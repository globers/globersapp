package co.globers.globersapp.utils

import android.app.Activity
import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.util.Log
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.ActivityResult.RESULT_IN_APP_UPDATE_FAILED
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.UpdateAvailability
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

// https://developer.android.com/guide/app-bundle/in-app-updates

class GlobersAppUpdateManager {

    companion object {
        const val REQUEST_CODE = 89
        const val LOG_TAG = "AppUpdate"
    }


    suspend fun updateAppIfUpdateAvailable(activity: Activity): Boolean{

        Log.i(LOG_TAG, "start updateAppIfUpdateAvailable")

        return suspendCoroutine { cont: Continuation<Boolean> ->

            Log.i(LOG_TAG, "start updateAppIfUpdateAvailable coroutine")

            // Creates instance of the manager.
            val appUpdateManager = AppUpdateManagerFactory.create(activity)

// Returns an intent object that you use to check for an update.
            val appUpdateInfoTask = appUpdateManager.appUpdateInfo

// Checks that the platform will allow the specified type of update.
            appUpdateInfoTask.addOnSuccessListener { appUpdateInfo: AppUpdateInfo ->

                logAppUpdateInfo(appUpdateInfo)

                if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    // For a flexible update, use AppUpdateType.FLEXIBLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)
                ) {
                    Log.i(LOG_TAG, "updateAppIfUpdateAvailable onSuccessListener, startUpdateFlowForResult")

                    // Request the update.
                    appUpdateManager.startUpdateFlowForResult(
                        // Pass the intent that is returned by 'getAppUpdateInfo()'.
                        appUpdateInfo,
                        // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                        AppUpdateType.IMMEDIATE,
                        // The current activity making the update request.
                        activity,
                        // Include a request code to later monitor this update request.
                        REQUEST_CODE
                    )
                    cont.resume(true)
                } else {
                    cont.resume(false)
                }

            }.addOnFailureListener {
                Log.e(LOG_TAG, "addOnFailureListener", it)
                cont.resume(false)
            }
        }

    }

    private fun logAppUpdateInfo(appUpdateInfo: AppUpdateInfo) {
        val updateAvailability = appUpdateInfo.updateAvailability()
        val isUpdateTypeAllowed = appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)
        val availableVersionCode = appUpdateInfo.availableVersionCode()
        val installStatus = appUpdateInfo.installStatus()
        val packageName = appUpdateInfo.packageName()

        Log.i(
            LOG_TAG,
            "logAppUpdateInfo: updateAvailability: $updateAvailability isUpdateTypeAllowed: $isUpdateTypeAllowed" +
                    " availableVersionCode: $availableVersionCode, installStatus: $installStatus, packageName: $packageName"
        )
    }

    fun onActivityResult(activity: Activity, requestCode: Int, resultCode: Int) {

        Log.i(LOG_TAG, "onActivityResult, requestCode: $requestCode, resultCode: $resultCode")

        if (requestCode == REQUEST_CODE) {

            when(resultCode){
                RESULT_OK -> {
                    Log.i(LOG_TAG, "update completed")
                }
                RESULT_CANCELED -> {
                    Log.i(LOG_TAG, "update canceled")
                    GlobalScope.launch {
                        updateAppIfUpdateAvailable(activity)
                    }
                }
                RESULT_IN_APP_UPDATE_FAILED -> {
                    Log.i(LOG_TAG, "update failed")
                    GlobalScope.launch {
                        updateAppIfUpdateAvailable(activity)
                    }
                }

            }
        }
    }


    // Checks that the update is not stalled during 'onResume()'.
// However, you should execute this check at all entry points into the app.
    fun onResume(activity: Activity) {

        Log.i(LOG_TAG, "onResume")

        // Creates instance of the manager.
        val appUpdateManager = AppUpdateManagerFactory.create(activity)

        appUpdateManager
            .appUpdateInfo
            .addOnSuccessListener { appUpdateInfo ->

                Log.i(LOG_TAG, "onResume addOnSuccessListener")
                logAppUpdateInfo(appUpdateInfo)

                if (appUpdateInfo.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS
                ) {
                    Log.i(LOG_TAG, "onResume addOnSuccessListener DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS")
                    // If an in-app update is already running, resume the update.
                    appUpdateManager.startUpdateFlowForResult(
                        appUpdateInfo,
                        AppUpdateType.IMMEDIATE,
                        activity,
                        REQUEST_CODE
                    )
                }
            }
    }




}