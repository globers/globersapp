package co.globers.globersapp.services

import co.globers.globersapp.firestore.entities.DbMessageToken
import co.globers.globersapp.firestore.executeSet
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

fun initMessageToken(userUid: String) {
    FirebaseInstanceId.getInstance().instanceId
        .addOnCompleteListener { task ->
            if (task.isSuccessful) {
                // Get new Instance ID token
                val token = task.result!!.token
                updateMessageTokenInFirestore(userUid, token)

            }
        }
}

fun updateMessageTokenInFirestore(userUid: String, token: String){
    val db = FirebaseFirestore.getInstance()
    GlobalScope.launch {
        executeSet(
            db.collection(DbMessageToken.COLLECTION_NAME).document(
                userUid
            ),
            DbMessageToken(token)
        )
    }
}