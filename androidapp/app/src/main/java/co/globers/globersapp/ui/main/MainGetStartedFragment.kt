package co.globers.globersapp.ui.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import co.globers.globersapp.R
import co.globers.globersapp.ui.newuser.NewUserOnboardingActivity
import co.globers.globersapp.utils.Event
import co.globers.globersapp.utils.Screen
import co.globers.globersapp.utils.Tracking
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.main_get_started_fragment.*

class MainGetStartedFragment : Fragment() {

    private lateinit var db: FirebaseFirestore
    private lateinit var tracking: Tracking

    companion object {
        fun newInstance() = MainGetStartedFragment()
        private const val RC_SIGN_IN = 1
        private const val LOG_TAG = "MainGetStartedFrag"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        db = FirebaseFirestore.getInstance()

        return inflater.inflate(R.layout.main_get_started_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tracking = Tracking(this, Screen.MainGetStarted)
        val bottomNavView = requireActivity().main_bottom_navigation_view
        bottomNavView.visibility = View.GONE


        button_main_getstarted_getstarted.setOnClickListener {
            // Create new user activity
            val intent = Intent(activity, NewUserOnboardingActivity::class.java)
            startActivity(intent)
        }

        button_main_getstarted_alreadyaccount.setOnClickListener {
            // Login screen

            val providers = arrayListOf(
                AuthUI.IdpConfig.GoogleBuilder().build(),
                AuthUI.IdpConfig.FacebookBuilder().build(),
                AuthUI.IdpConfig.EmailBuilder().build())

            startActivityForResult(
                AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .setAvailableProviders(providers)
                    .setLogo(R.drawable.globers_logo)      // Set logo drawable
                    .setTheme(R.style.AppThemeFirebaseAuth) // https://stackoverflow.com/questions/39136738/unable-to-hide-the-title-bar-of-authorization-activity-using-firebaseui
                    .build(),
                RC_SIGN_IN)
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        Log.d(LOG_TAG, "onActivityResult, requestCode: $requestCode")

        if (requestCode == RC_SIGN_IN){

            val response = IdpResponse.fromResultIntent(data)

            if (resultCode == Activity.RESULT_OK) {

                FirebaseAuth.getInstance().currentUser?.let {
                    val loginMethod = (it.getIdToken(false).result?.signInProvider).orEmpty()
                    tracking.event(Event.Login(loginMethod))
                }

                // Successfully signed in, go back to splash screen for initialization and checks
                val action = MainGetStartedFragmentDirections.actionMainGetStartedFragmentToMainSplashScreenFragment()
                findNavController().navigate(action)

            } else {

                if (response != null) {
                    Log.e(LOG_TAG, response.error.toString())
                    // Sign in failed. If response is null the user canceled the
                    // sign-in flow using the back button. Otherwise check
                    // response.getError().getErrorCode() and handle the error.
                    // ...

                    AlertDialog.Builder(requireActivity())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Login failed")
                        .setMessage("Are you connected to the internet? If the problem persists, please contact 'support@globers.co")
                        .setCancelable(false)
                        .setNeutralButton("OK"){ _, _ ->
                        }.show()

                }
            }

        }
    }


}
