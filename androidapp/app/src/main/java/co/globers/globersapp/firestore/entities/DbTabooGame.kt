package co.globers.globersapp.firestore.entities

import co.globers.globersapp.AppConfig
import com.google.firebase.Timestamp
import com.google.firebase.firestore.ServerTimestamp
import java.util.*


data class DbTabooGame(
    val uid: String,
    val creatorUserUid: String,
    val joinerUserUid: String,
    val peerMatchUid: String,
    val gameState: DbTabooGameState,
    val gameConfig: DbTabooGameConfig = DbTabooGameConfig(
        AppConfig.TABOO_GAME_DURATION_IN_SEC
        ),
    @ServerTimestamp
    val creationDate: Date = Date()
){

    constructor(): this("", "", "", "", DbTabooGameState())

    companion object {
        const val COLLECTION_NAME = "tabooGames"
        const val FIELD_GAME_STATE = "gameState"
    }
}

enum class DbTabooPlayer {
    CREATOR,
    JOINER
}

enum class DbTabooGameStatus {
    READY,
    RUNNING,
    FINISHED,
    ABORTED,
    ERROR
}

data class DbTabooGameConfig(
    val nbSecGameDuration: Int
){
    constructor(): this(0)
}

data class DbTabooGameState(
    val activeCardUid: String?,
    val currentTalker: DbTabooPlayer?,
    val playedCards: List<DbTabooCardPlayed>,
    val status: DbTabooGameStatus,
    val abortInitiatorUserUid: String? = null, // not null if status is aborted
    val creatorReady: Boolean = false,
    val joinerReady: Boolean = false,
    val errorReason: String? = null // not null if status error
){
    constructor(): this(null, DbTabooPlayer.CREATOR, listOf(), DbTabooGameStatus.ERROR)

    companion object {
        const val COLLECTION_NAME = "tabooGames"
        const val FIELD_PLAYED_CARDS = "playedCards"
        const val FIELD_ACTIVE_CARD_UID = "activeCardUid"
        const val FIELD_CURRENT_TALKER = "currentTalker"
        const val FIELD_ABORT_INITIATOR_USER_UID = "abortInitiatorUserUid"
        const val FIELD_ERROR_REASON = "errorReason"
        const val FIELD_STATUS = "status"
        const val FIELD_JOINER_READY = "joinerReady"
        const val FIELD_CREATOR_READY = "creatorReady"
    }

}

data class DbTabooCardPlayed(val cardUid: String, val action: DbTabooCardAction, val actionTimestamp: Timestamp, val talker: DbTabooPlayer){
    constructor(): this("", DbTabooCardAction.PASSED, Timestamp.now(), DbTabooPlayer.CREATOR)
}

enum class DbTabooCardAction {
    PASSED,
    FOUND,
    BUZZED_GUESSER
}

