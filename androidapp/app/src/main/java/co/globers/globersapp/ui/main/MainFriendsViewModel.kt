package co.globers.globersapp.ui.main

import android.util.Log
import androidx.lifecycle.*
import co.globers.globersapp.firestore.entities.*
import co.globers.globersapp.services.*
import co.globers.globersapp.utils.SingleLiveEvent
import io.reactivex.BackpressureStrategy
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.Observables
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch



class MainFriendsViewModel : ViewModel() {

    sealed class ContactToDisplay {

        data class CategoryHeader(val title: String): ContactToDisplay()
        data class Friend(val contact: DbContact) : ContactToDisplay() // invit to play / block : (vert / orange)
        data class Blocked(val contact: DbContact): ContactToDisplay() // Unblock (orange)
        data class SentRequest(val request: SentFriendRequest): ContactToDisplay() // cancel (orange)
        data class ReceivedRequest(val request: ReceivedFriendRequest): ContactToDisplay() // accept / deny : vert / orange
    }

    val contactsToDisplay: LiveData<List<ContactToDisplay>>

    val status= MutableLiveData<Status>(Status.USERS_NOT_LOADED)

    val calledContact = SingleLiveEvent<DbContact?>()

    private var contactsManager: ContactsManager = ContactsManager()
    private var friendsCreatorFromRequestsService = FriendsCreatorFromRequestsBackgroundService()

    private val compositeDisposable = CompositeDisposable()

    enum class Status {
        USERS_NOT_LOADED,
        LOADING_USERS_FROM_CONTACT,
        LOADED,
    }

    init {
        compositeDisposable.add(friendsCreatorFromRequestsService)

        val contactsObs = contactsManager.contacts()
        val sentRequestsObs = contactsManager.sentFriendRequests()
        val receiverRequestsObs = contactsManager.receivedFriendRequests()

        val resultObservable = Observables.combineLatest(contactsObs, sentRequestsObs, receiverRequestsObs){
            contacts, sent, received ->

            val friends = contacts.filter { it.status == DbContactStatus.FRIEND }.map { ContactToDisplay.Friend(it)}
            val blocked = contacts.filter { it.status == DbContactStatus.BLOCKED }.map { ContactToDisplay.Blocked(it) }
            val sentRequests = sent.map { ContactToDisplay.SentRequest(it) }
            val receivedRequests = received.map { ContactToDisplay.ReceivedRequest(it) }

            val result = mutableListOf<ContactToDisplay>()

            if(receivedRequests.isNotEmpty()){
                result.add(ContactToDisplay.CategoryHeader("Received invitations"))
                result.addAll(receivedRequests)
            }
            if (friends.isNotEmpty()){
                result.add(ContactToDisplay.CategoryHeader("Friends"))
                result.addAll(friends)
            }
            if (blocked.isNotEmpty()){
                result.add(ContactToDisplay.CategoryHeader("Blocked users"))
                result.addAll(blocked)
            }
            if(sent.isNotEmpty()){
                result.add(ContactToDisplay.CategoryHeader("Sent invitations"))
                result.addAll(sentRequests)
            }

            result as List<ContactToDisplay>

        }

        contactsToDisplay = LiveDataReactiveStreams.fromPublisher(resultObservable.toFlowable(BackpressureStrategy.LATEST))

    }


    fun sendFriendRequest(friendUserUid: String) {

        viewModelScope.launch(Dispatchers.IO) {
            contactsManager.sendFriendRequest(friendUserUid)
        }
    }


    fun callFriendToPlay(contact: DbContact) {
        Log.d("MainFriendsVM", "callFriendToPlay, contact:${contact}, current:${calledContact.value}")
        if (calledContact.value == null) { // prevent double calls
            calledContact.value = contact
        }
    }

    fun unblockContact(contact: DbContact) {
        viewModelScope.launch(Dispatchers.IO) {
            contactsManager.changeContactStatus(contact, DbContactStatus.FRIEND)
        }
    }

    fun cancelFriendRequest(request: ContactToDisplay.SentRequest) {
        viewModelScope.launch(Dispatchers.IO) {
            contactsManager.cancelFriendRequest(request.request)
        }
    }

    fun acceptFriendRequest(request: ContactToDisplay.ReceivedRequest) {
        viewModelScope.launch(Dispatchers.IO) {
            contactsManager.replyToFriendRequest(
                request.request.friendRequest.uid,
                DbFriendRequestReply.ACCEPT
            )
        }
    }

    fun blockFriendRequest(request: ContactToDisplay.ReceivedRequest) {
        viewModelScope.launch(Dispatchers.IO) {
            contactsManager.replyToFriendRequest(
                request.request.friendRequest.uid,
                DbFriendRequestReply.DENY
            )
        }
    }

    fun blockContact(contact: DbContact) {
        viewModelScope.launch(Dispatchers.IO) {
            contactsManager.changeContactStatus(contact, DbContactStatus.BLOCKED)
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}


