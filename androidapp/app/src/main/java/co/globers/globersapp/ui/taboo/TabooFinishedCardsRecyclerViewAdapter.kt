package co.globers.globersapp.ui.taboo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.globers.globersapp.R
import co.globers.globersapp.firestore.entities.DbTabooCardAction
import co.globers.globersapp.services.TabooGame

class TabooFinishedCardsRecyclerViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val wordToGuessTextView: TextView = view.findViewById(R.id.textView_taboo_card_wordToGuess)
    val forbiddenWordsRecyclerView: RecyclerView = view.findViewById(R.id.recyclerView_taboo_card_tabooWords)
    val doneImageView: ImageView = view.findViewById(R.id.imageView_taboo_card_done)
    val failedImageView: ImageView = view.findViewById(R.id.imageView_taboo_card_failed)
}

data class Card(val word: String, val forbiddenWords: List<String>, val isFound: Boolean)

class TabooFinishedCardsRecyclerViewAdapter(
    val cards: List<TabooGame.CardPlayed>
) : RecyclerView.Adapter<TabooFinishedCardsRecyclerViewHolder>() {

    lateinit var itemView: View

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TabooFinishedCardsRecyclerViewHolder {

        val layoutView = R.layout.taboo_card_view
        itemView = LayoutInflater.from(parent.context).inflate(layoutView, parent, false)
        return TabooFinishedCardsRecyclerViewHolder(itemView)
    }

    override fun getItemCount() = cards.size

    override fun onBindViewHolder(holder: TabooFinishedCardsRecyclerViewHolder, position: Int) {

        val card = cards[position]
        holder.wordToGuessTextView.text = card.card.wordToGuess

        val tabooWordsRecyclerView = holder.forbiddenWordsRecyclerView

        tabooWordsRecyclerView.adapter = TabooCardForbiddenWordsRecyclerViewAdapter(card.card.forbiddenWords)
        tabooWordsRecyclerView.layoutManager = LinearLayoutManager(itemView.context)

        if (card.cardPlayed.action == DbTabooCardAction.FOUND) {
            holder.doneImageView.visibility = View.VISIBLE
            holder.failedImageView.visibility = View.GONE
        } else {
            holder.doneImageView.visibility = View.GONE
            holder.failedImageView.visibility = View.VISIBLE
        }

    }

}