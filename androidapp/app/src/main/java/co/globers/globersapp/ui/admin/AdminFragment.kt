package co.globers.globersapp.ui.admin

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import co.globers.globersapp.AdminParams
import co.globers.globersapp.Globers
import co.globers.globersapp.R
import kotlinx.android.synthetic.main.admin_fragment.*


class AdminFragment : Fragment() {

    companion object {
        fun newInstance() = AdminFragment()
    }

    private lateinit var viewModel: AdminViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.admin_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(AdminViewModel::class.java)


        val currentParams = (requireActivity().applicationContext as Globers).adminParams
        viewModel.adminParams.value = currentParams ?: AdminParams()


        viewModel.adminParams.observe(viewLifecycleOwner, Observer {
            (requireActivity().applicationContext as Globers).adminParams = it
            switch_admin_fakeVoiceCall.isChecked = it.fakeVoiceCall
        })


        switch_admin_fakeVoiceCall.setOnCheckedChangeListener{ _, isChecked ->
            viewModel.adminParams.value = viewModel.adminParams.value.copy(fakeVoiceCall = isChecked)
        }

        viewModel.adminUserLiveData.observe(viewLifecycleOwner, Observer { adminUser ->
            switch_admin_notifyOnWaitingPeerMatch.isChecked = adminUser?.value?.notifyOnWaitingPeerMatches ?: false
        })

        switch_admin_notifyOnWaitingPeerMatch.setOnClickListener {
            viewModel.setNotifyWaitingPeerMatch(switch_admin_notifyOnWaitingPeerMatch.isChecked)
        }

    }

}
