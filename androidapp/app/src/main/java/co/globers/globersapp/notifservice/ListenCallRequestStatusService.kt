package co.globers.globersapp.notifservice

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.os.Parcelable
import co.globers.globersapp.services.ReceivedPhoneCallManager
import io.reactivex.disposables.Disposable
import kotlinx.android.parcel.Parcelize

class ListenCallRequestStatusService : Service() {

    companion object {
        const val EXTRA_ARGS = "ListenCallRequestStatusServiceExtraArgs"
    }
    @Parcelize
    data class Args(
        val notificationId: Int,
        val callerUserUid: String,
        val callRequestUid: String,
        val peerMatchUid: String,
        val callerUserName: String
    ): Parcelable

    private var subscription: Disposable? = null
    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {

        val args = intent.getParcelableExtra<Args>(EXTRA_ARGS)!!

        val phoneCallManager = ReceivedPhoneCallManager(
            this,
            MessageService.TABOO_GAME_PHONE_CALL_NOTIFICATION_ID,
            args.callRequestUid,
            args.peerMatchUid,
            args.callerUserUid
        )

        phoneCallManager.notifyPhoneCallReceived()
        subscription = phoneCallManager.callRequestClosed.subscribe {
            stopSelf()
        }

        return START_STICKY

    }

    override fun onDestroy() {
        super.onDestroy()
        subscription?.dispose()
    }
}
