package co.globers.globersapp.machinelearning

import android.util.Log
import kotlin.math.exp
import kotlin.math.ln

data class Logit1DCoeffs(
    val intercept: Double,
    val coeff: Double
)

enum class LogisticRegressionResultState {
    OK,
    SEPARATION_PROBLEM,
    NON_CONVERGENCE
}

data class LogisticRegressionResult(
    val state: LogisticRegressionResultState,
    val result: Logit1DCoeffs?
)


fun computeProba(coeffs: Logit1DCoeffs, x: Double): Double{
    val expAxPlusB = exp(coeffs.coeff*x+coeffs.intercept)
    return expAxPlusB / (1 + expAxPlusB)
}

fun computeX(coeffs: Logit1DCoeffs, proba: Double): Double {
    return (ln(proba/(1-proba))-coeffs.intercept)/coeffs.coeff
}

private const val LOG_TAG = "LogsticRegresison"

// Follow "The elements of statistical learning P121"
fun logisticRegression1D(result: BooleanArray, input: DoubleArray): LogisticRegressionResult {

    if (isSeparationProblem(result, input)) return LogisticRegressionResult(LogisticRegressionResultState.SEPARATION_PROBLEM, null)


    val vectOfOnes = Vector(DoubleArray(input.size){1.0})
    val xt = Matrix(arrayOf(vectOfOnes, Vector(input)))
    val x = xt.transpose()
    val y = Vector(result.map { if(it) 1.0 else 0.0 })

    val betaToCoeffs: (Vector) -> Logit1DCoeffs = { beta -> Logit1DCoeffs(beta[0], beta[1]) }

    val p: (Vector) -> Vector = { beta -> Vector(input.map { computeProba(betaToCoeffs(beta), it) })}
    val w: (Vector) -> Matrix = { beta -> Matrix.diag(p(beta)*(vectOfOnes-p(beta))) }

    val betaInit = Vector(doubleArrayOf(0.0,0.0))
    val logLikelihoodGradient: (Vector) -> Vector = { beta -> xt*(y-p(beta))}
    val logLikelihoodHessian: (Vector) -> Matrix = { beta -> -xt*w(beta)*x}

    val betaFinal = newtonRaphson(betaInit, logLikelihoodGradient, logLikelihoodHessian)

    return when(betaFinal){
        is NewtonRaphsonResult.NonConvergence -> LogisticRegressionResult(LogisticRegressionResultState.NON_CONVERGENCE, null)
        is NewtonRaphsonResult.Ok -> LogisticRegressionResult(LogisticRegressionResultState.OK, betaToCoeffs(betaFinal.vector))
    }

}


fun isSeparationProblem(result: BooleanArray, input: DoubleArray): Boolean {

    val points = input.zip(result.toList()).sortedBy { it.first }

    val lastOne = points.indexOfLast { it.second }
    val firsZero = points.indexOfFirst() { !it.second }

    val firstOne = points.indexOfFirst() { it.second }
    val lastZero = points.indexOfLast() { !it.second }

    return lastOne == -1 || firsZero == -1 || lastOne < firsZero || lastZero < firstOne
}



// CAN BE DELETED
private fun getDefaultIfSeparationProblem(result: BooleanArray, input: DoubleArray): Logit1DCoeffs? {

    val points = input.zip(result.toList()).sortedBy { it.first }

    val lastOne = points.indexOfLast { it.second }
    val firsZero = points.indexOfFirst() { !it.second }

    if ( lastOne == -1){
        Log.i(LOG_TAG, "only zero values, generate default logistic params")
        return Logit1DCoeffs(-1000.0, -0.1) // only zero, a always negative, exp(ax+b) must be very small whatever x
    }
    else if (firsZero == -1){
        Log.i(LOG_TAG, "only one values, generate default logistic params")
        return Logit1DCoeffs(1000.0, -0.1) // only one, a always negative, exp(ax+b) must be big whatever x
    }
    else if (lastOne < firsZero){
        Log.i(LOG_TAG, "separation issue, generate default logistic params")
        // separation issue, algorithm do not converge
        val cutoffAbscissae = (points[lastOne].first + points[firsZero].first)/2
        val coeff = -10.0 // arbitrary choice, big coeff => steep change from one to zero in probability
        // we chose b so that P(x=cutoff)=0.5 => ax+b=0
        val intercept = - coeff * cutoffAbscissae
        return Logit1DCoeffs(intercept, coeff)
    }
    else {
        return null
    }
}
