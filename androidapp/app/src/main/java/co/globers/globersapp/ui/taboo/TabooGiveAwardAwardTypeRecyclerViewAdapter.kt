package co.globers.globersapp.ui.taboo

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import co.globers.globersapp.R
import co.globers.globersapp.firestore.entities.DbUserAwardType
import co.globers.globersapp.firestore.entities.toDisplayMessage

class TabooGiveAwardAwardTypeRecyclerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val awardTypeTextView: TextView = view.findViewById(R.id.textView_taboo_giveAward_awardType)
}


class TabooGiveAwardAwardTypeRecyclerViewAdapter(
) : RecyclerView.Adapter<TabooGiveAwardAwardTypeRecyclerViewHolder>() {

    private var selectedItemBackgroundColor: Int = 0

    var selectedAwardType: MutableLiveData<DbUserAwardType?> = MutableLiveData(null)
        private set

    private var selectedHolder: TabooGiveAwardAwardTypeRecyclerViewHolder? = null



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TabooGiveAwardAwardTypeRecyclerViewHolder {
        val layoutView = R.layout.taboo_giveaward_awardtype_view
        val v = LayoutInflater.from(parent.context).inflate(layoutView, parent, false)
        selectedItemBackgroundColor = ContextCompat.getColor(v.context, R.color.myColorPrimary)
        return TabooGiveAwardAwardTypeRecyclerViewHolder(v)
    }

    override fun getItemCount() = DbUserAwardType.values().size

    override fun onBindViewHolder(holder: TabooGiveAwardAwardTypeRecyclerViewHolder, position: Int) {

        val awardType = DbUserAwardType.values()[position]
        holder.awardTypeTextView.text = toDisplayMessage(awardType)

        holder.awardTypeTextView.setOnClickListener {

            val previousFocusedHolder = selectedHolder

            val backgroundColor = holder.awardTypeTextView.background
            val textColor = holder.awardTypeTextView.currentTextColor

            previousFocusedHolder?.awardTypeTextView?.background = backgroundColor
            previousFocusedHolder?.awardTypeTextView?.setTextColor(textColor)

            selectedHolder = holder
            selectedAwardType.value = awardType

            holder.awardTypeTextView.setBackgroundColor(selectedItemBackgroundColor)
            holder.awardTypeTextView.setTextColor(Color.WHITE)
        }





    }

}