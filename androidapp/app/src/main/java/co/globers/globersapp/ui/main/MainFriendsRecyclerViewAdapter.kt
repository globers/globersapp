package co.globers.globersapp.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.globers.globersapp.R
import co.globers.globersapp.utils.BugException


sealed class MainFriendsRecyclerViewHolder(protected val model: MainFriendsViewModel, view: View): RecyclerView.ViewHolder(view){

    abstract fun bind(contact: MainFriendsViewModel.ContactToDisplay)

    class Header(model: MainFriendsViewModel, view: View) : MainFriendsRecyclerViewHolder(model, view){
        private val headerTextView: TextView = view.findViewById(R.id.textView_main_friends_item_header)

        override fun bind(contact: MainFriendsViewModel.ContactToDisplay){
            val header = contact as MainFriendsViewModel.ContactToDisplay.CategoryHeader
            headerTextView.text = header.title
        }
    }

    class Friend(model: MainFriendsViewModel, view: View) : MainFriendsRecyclerViewHolder(model, view){
        private val nameTextView: TextView = view.findViewById(R.id.textView_main_friends_item_friend_name)
        private val callButton: Button = view.findViewById(R.id.button_main_friends_item_friend_call)
        private val blockButton: Button = view.findViewById(R.id.button_main_friends_item_friend_block)


        override fun bind(contact: MainFriendsViewModel.ContactToDisplay){
            val friend = contact as MainFriendsViewModel.ContactToDisplay.Friend
            nameTextView.text = friend.contact.publicProfile.displayName

            callButton.setOnClickListener {
                model.callFriendToPlay(contact.contact)
            }
            blockButton.setOnClickListener {
                model.blockContact(contact.contact)
            }
        }
    }

    class Blocked(model: MainFriendsViewModel, view: View) : MainFriendsRecyclerViewHolder(model, view){
        private val nameTextView: TextView = view.findViewById(R.id.textView_main_friends_item_blocked_name)
        private val unblockButton: Button = view.findViewById(R.id.button_main_friends_item_blocked_unblock)

        override fun bind(contact: MainFriendsViewModel.ContactToDisplay){
            val blocked = contact as MainFriendsViewModel.ContactToDisplay.Blocked
            nameTextView.text = blocked.contact.publicProfile.displayName

            unblockButton.setOnClickListener {
                model.unblockContact(contact.contact)
            }
        }
    }

    class RequestSent(model: MainFriendsViewModel, view: View) : MainFriendsRecyclerViewHolder(model, view){
        private val nameTextView: TextView = view.findViewById(R.id.textView_main_friends_item_requestSent_name)
        private val cancelButton: Button = view.findViewById(R.id.button_main_friends_item_requestSent_cancel)

        override fun bind(contact: MainFriendsViewModel.ContactToDisplay){
            val request = contact as MainFriendsViewModel.ContactToDisplay.SentRequest
            nameTextView.text = request.request.receiverPublicProfile.displayName

            cancelButton.setOnClickListener {
                model.cancelFriendRequest(contact)
            }
        }
    }

    class RequestReceived(model: MainFriendsViewModel, view: View) : MainFriendsRecyclerViewHolder(model, view){
        private val nameTextView: TextView = view.findViewById(R.id.textView_main_friends_item_requestReceived_name)
        private val acceptButton: Button = view.findViewById(R.id.button_main_friends_item_requestReceived_accept)
        private val blockButton: Button = view.findViewById(R.id.button_main_friends_item_requestReceived_block)

        override fun bind(contact: MainFriendsViewModel.ContactToDisplay){
            val request = contact as MainFriendsViewModel.ContactToDisplay.ReceivedRequest
            nameTextView.text = request.request.senderPublicProfile.displayName

            acceptButton.setOnClickListener {
                model.acceptFriendRequest(contact)
            }
            blockButton.setOnClickListener {
                model.blockFriendRequest(contact)
            }
        }
    }


}



class MainFriendsRecyclerViewAdapter(
    private val model: MainFriendsViewModel,
    private val contacts: List<MainFriendsViewModel.ContactToDisplay>
) :
    RecyclerView.Adapter<MainFriendsRecyclerViewHolder>() {


    companion object {
        private const val HEADER_VIEW_TYPE = 0
        private const val FRIEND_VIEW_TYPE = 1
        private const val BLOCKED_VIEW_TYPE = 2
        private const val REQUEST_SENT_VIEW_TYPE = 3
        private const val REQUEST_RECEIVED_VIEW_TYPE = 4
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainFriendsRecyclerViewHolder {

        return when (viewType){

            HEADER_VIEW_TYPE ->  {
                val layoutView = R.layout.main_friends_item_header
                val v = LayoutInflater.from(parent.context).inflate(layoutView, parent, false)
                MainFriendsRecyclerViewHolder.Header(model, v)
            }
            FRIEND_VIEW_TYPE ->  {
                val layoutView = R.layout.main_friends_item_friend
                val v = LayoutInflater.from(parent.context).inflate(layoutView, parent, false)
                MainFriendsRecyclerViewHolder.Friend(model, v)
            }
            BLOCKED_VIEW_TYPE ->  {
                val layoutView = R.layout.main_friends_item_blocked
                val v = LayoutInflater.from(parent.context).inflate(layoutView, parent, false)
                MainFriendsRecyclerViewHolder.Blocked(model, v)
            }
            REQUEST_SENT_VIEW_TYPE ->  {
                val layoutView = R.layout.main_friends_item_request_sent
                val v = LayoutInflater.from(parent.context).inflate(layoutView, parent, false)
                MainFriendsRecyclerViewHolder.RequestSent(model, v)
            }
            REQUEST_RECEIVED_VIEW_TYPE ->  {
                val layoutView = R.layout.main_friends_item_request_received
                val v = LayoutInflater.from(parent.context).inflate(layoutView, parent, false)
                MainFriendsRecyclerViewHolder.RequestReceived(model, v)
            }
            else -> throw BugException("view type Int value unknown: $viewType")
        }

    }

    override fun onBindViewHolder(viewHolder: MainFriendsRecyclerViewHolder, position: Int) {
        val contact = contacts[position]
        viewHolder.bind(contact)
    }

    override fun getItemCount() = contacts.size

    override fun getItemViewType(position: Int) = when(contacts[position]){
        is MainFriendsViewModel.ContactToDisplay.CategoryHeader -> HEADER_VIEW_TYPE
        is MainFriendsViewModel.ContactToDisplay.Friend -> FRIEND_VIEW_TYPE
        is MainFriendsViewModel.ContactToDisplay.Blocked -> BLOCKED_VIEW_TYPE
        is MainFriendsViewModel.ContactToDisplay.SentRequest -> REQUEST_SENT_VIEW_TYPE
        is MainFriendsViewModel.ContactToDisplay.ReceivedRequest -> REQUEST_RECEIVED_VIEW_TYPE
    }

}

